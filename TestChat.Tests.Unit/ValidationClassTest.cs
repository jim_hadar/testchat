using TestChat.Business.Helpers;
using Xunit;

namespace TestChat.Tests.Unit
{
    public class ValidationClassTest
    {
        [Fact]
        public void ContainsOnlyLatinTest()
        {
            Assert.True(Validation.ContainsOnlyLatin("TestOnlyLatin123"));
            Assert.False(Validation.ContainsOnlyLatin("414&6*"));
        }
    }
}