﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Helpers.Converters {
    public static partial class ConverterHelper {
        public static long ToUnixTimestamp(this DateTime dateTime) {
            DateTime dateTimeWithoutMs = new DateTime(dateTime.Year, 
                                                      dateTime.Month, 
                                                      dateTime.Day, 
                                                      dateTime.Hour, 
                                                      dateTime.Minute, 
                                                      dateTime.Second);
            TimeSpan span = (dateTimeWithoutMs - new DateTime(1970, 1, 1, 0, 0, 0, 0));
            return (long)span.TotalSeconds;
        }
    }
}
