﻿using System;

#nullable enable
namespace Framework.Exceptions {
    public class EntityNotFoundException : FrameworkException
    {
        public EntityNotFoundException(string? message)
            : base(message) { }

        public EntityNotFoundException(string? message, Exception innerException)
            :base(message, innerException) { }
    }
}
