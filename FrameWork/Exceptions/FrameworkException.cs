﻿using System;
using System.Collections.Generic;
using System.Text;
#nullable enable
namespace Framework.Exceptions
{
    public class FrameworkException : Exception
    {

        public FrameworkException()
            : base() { }

        public FrameworkException(string? message)
            : base(message) { }

        public FrameworkException(string? message, Exception innerException)
            :base(message, innerException) { }
    }
}
