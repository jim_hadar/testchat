﻿using System;

#nullable enable
namespace Framework.Exceptions.Db {
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="System.Exception" />
    public class ObjectCreateException : FrameworkException
    {
        public ObjectCreateException()
            : base("Ошибка при создании записи") {

        }

        public ObjectCreateException(string? message)
            :base(message)
        {
            
        }
        
        public ObjectCreateException(string? message, Exception innerException)
            :base(message, innerException) { }
    }
}
