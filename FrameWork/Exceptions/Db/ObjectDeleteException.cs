﻿using System;

#nullable enable
namespace Framework.Exceptions.Db {
    public class ObjectDeleteException : FrameworkException
    {
        public ObjectDeleteException(string? message)
            :base(message)
        {
            
        }
        
        public ObjectDeleteException(string? message, Exception innerException)
            :base(message, innerException) { }
    }
}
