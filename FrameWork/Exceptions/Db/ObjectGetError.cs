﻿using System;

#nullable enable
namespace Framework.Exceptions.Db {
    public class ObjectGetError : FrameworkException
    {
        public ObjectGetError(string? message)
            :base(message)
        {
            
        }
        
        public ObjectGetError(string? message, Exception innerException)
            :base(message, innerException) { }
    }
}
