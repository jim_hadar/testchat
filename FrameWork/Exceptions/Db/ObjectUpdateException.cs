﻿using System;

#nullable enable
namespace Framework.Exceptions.Db {
    public class ObjectUpdateException : FrameworkException
    {
        public ObjectUpdateException(string? message)
            :base(message)
        {
            
        }
        
        public ObjectUpdateException(string? message, Exception innerException)
            :base(message, innerException) { }
    }
}
