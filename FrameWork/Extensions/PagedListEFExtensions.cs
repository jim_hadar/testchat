﻿using Framework.DAL.Models;
using Framework.Paging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.DAL.Paging;
using Framework.DAL.Paging.EF;

namespace Framework.Extensions {
    public static class PagedListEfExtensions {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="pagingParams"></param>
        /// <param name="mappingFunc"></param>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TFilter"></typeparam>
        /// <returns></returns>
        public static async Task<PagedEfList<TFilter, TModel>> ToPagedListAsync<TModel, TFilter>(
                                                    this IQueryable<TModel> source,
                                                    PagingParams<TFilter> pagingParams,
                                                    Func<IQueryable<TModel>, Task<IList>>? mappingFunc = null)
            where TFilter : BaseEFFilter{
            return await source.ToPagedListAsync(pagingParams.PageSize, pagingParams.PageNumber, pagingParams.Filter, mappingFunc);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        /// <param name="mappingFunc"></param>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TFilter"></typeparam>
        /// <returns></returns>
        public static PagedEfList<TFilter, TModel> ToPagedList<TModel, TFilter>(
                                                    this IQueryable<TModel> source, 
                                                    int pageSize, 
                                                    int pageNumber,
                                                    TFilter filter,
                                                    Func<IQueryable<TModel>, IList>? mappingFunc = null)
            where TFilter : BaseEFFilter{
            var pageList = new PagedEfList<TFilter, TModel>(source, pageNumber, pageSize, filter);
            pageList.SetData(mappingFunc);
            return pageList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageNumber"></param>
        /// <param name="filter"></param>
        /// <param name="mappingFunc"></param>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TFilter"></typeparam>
        /// <returns></returns>
        public static async Task<PagedEfList<TFilter, TModel>> ToPagedListAsync<TModel, TFilter>(
                                                    this IQueryable<TModel> source,
                                                    int pageSize,
                                                    int pageNumber,
                                                    TFilter filter,
                                                    Func<IQueryable<TModel>, Task<IList>>? mappingFunc = null)
            where TFilter : BaseEFFilter{
            var pageList = new PagedEfList<TFilter, TModel>(source, pageSize, pageNumber, filter);
            await pageList.SetDataAsync(mappingFunc);
            return pageList;
        }

    }
}
