﻿using System;
using System.ComponentModel;
using System.Reflection;

#nullable enable
namespace Framework.Extensions {
    public static class NameExtensions {
        #region Get DisplayNameAttibute
        public static string? GetDisplayName(this object obj, string propertyName) {
            return GetDisplayName(obj.GetType(), propertyName);
        }
        
        public static string? GetDisplayName(this Type type, string propertyName) {
            var property = type.GetProperty(propertyName);
            return property == null ? string.Empty : GetAttributeDisplayName(property);
        }
        
        public static string? GetDisplayName(this object obj) {
            string res = string.Empty;
            if (obj is null)
                return res;
            res = obj.GetType().Name;
            try {
                var attr = obj.GetType().GetCustomAttribute<DisplayNameAttribute>(true);
                return attr?.DisplayName;
            }
            catch (Exception)
            {
                // ignored
            }

            return res;
        }

        public static string? GetAttributeDisplayName(PropertyInfo property) {
            try {
                var atts = property.GetCustomAttributes(
                    typeof(DisplayNameAttribute), true);
                if (atts.Length == 0)
                    return property.Name;
                
                return (atts[0] as DisplayNameAttribute)?.DisplayName;
            }
            catch (Exception)
            {
                // ignored
            }

            return property.Name;
        }
        #endregion

        #region Get Name of object
        
        public static string GetName(this object obj) {
            if (obj is null)
                throw new ArgumentNullException(nameof(obj));
            return obj.GetType().Name;
        }
        public static string GetName(this Type type) {
            if (type is null)
                throw new ArgumentNullException(nameof(type));
            return type.Name ?? string.Empty;
        }
        #endregion
    }
}
