﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Services
{
    public class BaseService : IService
    {
        protected ILogService _logService;

        public BaseService(ILogService logService)
        {
            this._logService = logService;
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (!_disposed)
            {
                _disposed = true;
            }
        }
    }
}