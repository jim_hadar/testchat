﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Services {
    public interface IService<T> : IService {

    }
    public interface IService : IDisposable {

    }
}
