﻿using System;
using System.Collections.Generic;
using System.Text;
using Framework.Constants;

namespace Framework.Services {
    public sealed class NLogLoggerService : ILogService {
        private string LoggerName { get; set; } = CommonConstants.AppLoggerName;
        private NLog.ILogger Logger => NLog.LogManager.GetLogger(LoggerName);
        public NLogLoggerService() {
            
        }
        public NLogLoggerService(string loggerName) {
            LoggerName = loggerName;
        }

        public void LogTrace(string message) {
            Logger.Trace(message);
        }
        public void LogTrace(Exception ex, string message) {
            Logger.Trace(ex, message);
        }

        public void LogDebug(string message) {
            Logger.Debug(message);
        }

        public void LogDebug(Exception ex, string message) {
            Logger.Debug(ex, message);
        }

        public void LogInfo(string message) {
            Logger.Info(message);
        }
        public void LogInfo(Exception ex, string message) {
            Logger.Info(ex, message);
        }

        public void LogError(string message) {
            Logger.Error(message);
        }

        public void LogError(Exception e) {
            Logger.Error(e);
        }

        public void LogError(Exception e, string message) {
            Logger.Error(e, message);
        }
        public void LogFatal(string message) {
            Logger.Fatal(message);
        }
        public void LogFatal(Exception e) {
            Logger.Fatal(e);
        }
        public void LogFatal(Exception e, string message) {
            Logger.Fatal(e, message);
        }

    }
}
