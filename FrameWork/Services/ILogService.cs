﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using NLog.Targets;

namespace Framework.Services {
    public static class LoggerService {

        public static void Initialize(string connString)
        {
            GlobalDiagnosticsContext.Set("defaultConnection", connString);
            NLog.LogManager.LoadConfiguration("nlog.config");
            foreach (var target in NLog.LogManager.Configuration.ConfiguredNamedTargets)
            {
                if (target is DatabaseTarget dbTarget)
                {
                    dbTarget.ConnectionString = connString;
                }
            }
        }

        public static void Shutdown()
        {
            NLog.LogManager.Shutdown();
        }
        
        private static ConcurrentDictionary<string, ILogService> LogServices = new ConcurrentDictionary<string, ILogService>();
        public static ILogService GetLogger(string loggerName) =>
            LogServices.GetOrAdd(loggerName, new NLogLoggerService(loggerName));
    }

    public interface ILogService {
        void LogTrace(string message);
        void LogTrace(Exception ex, string message);
        void LogDebug(string message);
        void LogDebug(Exception ex, string message);
        void LogInfo(string message);
        void LogInfo(Exception ex, string message);
        void LogError(string message);
        void LogError(Exception e);
        void LogError(Exception e, string message);
        void LogFatal(string message);
        void LogFatal(Exception e);
        void LogFatal(Exception e, string message);
    }
}
