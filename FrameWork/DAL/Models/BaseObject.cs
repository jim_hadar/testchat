﻿using System.ComponentModel.DataAnnotations;
namespace Framework.DAL.Models {
    public class BaseObject : IBaseObject {
        /// <summary>
        /// Key Field
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Logical delete
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
