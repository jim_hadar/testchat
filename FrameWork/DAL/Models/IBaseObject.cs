﻿namespace Framework.DAL.Models {

    public interface IBaseObject : IBase {
        long Id { get; set; }
        bool IsDeleted { get; set; }
    }
}
