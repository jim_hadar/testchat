﻿using System;
using Framework.Paging;
using Microsoft.AspNetCore.Mvc;

#nullable disable
namespace Framework.DAL.Paging {
    /// <summary>
    /// расширенная пагинация
    /// </summary>
    /// <typeparam name="TFilter">тип фильтра</typeparam>
    [ModelBinder(typeof(PagingModelBinder))]
    public class PagingParams<TFilter> : PagingParams
        where TFilter : IBaseFilter {
        /// <summary>
        /// Фильтр данных на странице
        /// </summary>
        public TFilter Filter { get; set; }       

        /// <inheritdoc />
        public PagingParams() {
            Filter = (TFilter)Activator.CreateInstance(typeof(TFilter));
        }
    }

    public abstract class PagingParams {
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Размер страницы
        /// </summary>
        public int PageSize { get; set; }
    }
}
