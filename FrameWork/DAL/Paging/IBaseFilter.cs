﻿namespace Framework.Paging
{
    public interface IBaseFilter
    {
        /// <summary>
        /// Value in search field
        /// </summary>
        string Search { get; set; }

        /// <summary>
        /// Name of column by sort
        /// </summary>
        string Sort { get; set; }

        /// <summary>
        /// Order by asc / desc
        /// </summary>
        string Order { get; set; }
    }
}