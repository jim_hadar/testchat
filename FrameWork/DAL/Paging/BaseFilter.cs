﻿using Framework.Paging;

namespace Framework.DAL.Paging
{
    public abstract class BaseFilter : IBaseFilter
    {
        #region constants

        /// <summary>
        /// Order by descending const
        /// </summary>
        protected const string DescSort = "desc";

        /// <summary>
        /// order by descending const
        /// </summary>
        protected const string DescendingSort = "descending";

        /// <summary>
        /// order by ascending const
        /// </summary>
        protected const string AscSort = "asc";

        /// <summary>
        /// order by ascending const 
        /// </summary>
        protected const string AscendingSort = "ascending";

        #endregion

        #region private variables

        private string _search = string.Empty;
        private string _sort = "Id";
        private string _order = AscendingSort;

        #endregion

        #region protected methods

        /// <summary>
        /// Sort field by default
        /// </summary>
        protected abstract string DefaultSortField { get; }

        /// <summary>
        /// For DynamicLinqCore
        /// </summary>
        protected string DefaultDescendingSortExpression => $"{Sort} {DescendingSort}";

        #endregion

        #region public variables

        /// <inheritdoc />
        public virtual string Search
        {
            get => !string.IsNullOrEmpty(_search) ? _search.ToLower() : string.Empty;
            set => _search = value;
        }

        /// <inheritdoc />
        public virtual string Sort
        {
            get => !string.IsNullOrEmpty(_sort) ? _sort : DefaultSortField;
            set => _sort = value;
        }

        /// <inheritdoc />
        public string Order
        {
            get => string.IsNullOrEmpty(_order) ? DescendingSort : _order;
            set => _order = string.IsNullOrEmpty(value) ? DescendingSort : value;
        }

        #endregion
    }
}