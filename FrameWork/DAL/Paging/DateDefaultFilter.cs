﻿using System;
using Framework.Paging;

namespace Framework.DAL.Paging
{
    public class DateDefaultFilter : DefaultFilter
    {
        /// <summary>
        /// Дата начала фильтрации
        /// </summary>
        public DateTime? DateFrom { get; set; }

        private DateTime? _dateTo;

        /// <summary> 
        /// Дата окончания фильтрации
        /// </summary>
        public DateTime? DateTo
        {
            get => _dateTo;
            set => _dateTo = value?.Date.AddDays(1).AddSeconds(-1) ?? value;
        }
    }
}