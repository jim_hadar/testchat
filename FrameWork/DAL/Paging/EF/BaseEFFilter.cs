﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

// ReSharper disable PossibleUnintendedQueryableAsEnumerable

namespace Framework.DAL.Paging.EF {
    public class BaseEFFilter : DefaultFilter, IEfFilter {

        /// <inheritdoc />
        public virtual IQueryable Filtrate(IQueryable data) {            
            return DefaultSorting(data);
        }
        /// <inheritdoc />
        public virtual IList Filtrate(IList data) {
            return (IList) Filtrate(data.AsQueryable());
        }

        private IQueryable DefaultSorting(IQueryable data)
        {
            data = Order.ToLower() switch
            {
                AscSort => data.OrderBy(Sort),
                AscendingSort => data.OrderBy(Sort),
                _ => data.OrderBy(DefaultDescendingSortExpression)
            };
            return data;
        }

        protected IQueryable<T> DefaultSorting<T>(IQueryable<T> data)
        {
            data = Order.ToLower() switch
            {
                AscSort => data.OrderBy(Sort),
                AscendingSort => data.OrderBy(Sort),
                _ => data.OrderBy(DefaultDescendingSortExpression)
            };
            return data;
        }

        protected virtual IQueryable<TSource> DefaultSorting<TSource, TKey>(IQueryable<TSource> data, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            data = Order.ToLower() switch
            {
                "asc" => data.OrderBy(keySelector, comparer).AsQueryable(),
                "ascending" => data.OrderBy(keySelector, comparer).AsQueryable(),
                _ => data.OrderByDescending(keySelector, comparer).AsQueryable()
            };
            return data;
        }
    }
}
