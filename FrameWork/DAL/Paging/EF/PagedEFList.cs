﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Framework.DAL.Paging.EF {
    public class PagedEfList<TFilter, TModel> : PagedList 
        where TFilter : BaseEFFilter{

        private IQueryable<TModel> _sources;

        private readonly TFilter _filter;

        public PagedEfList(IQueryable<TModel> source,
                            int pageSize,
                            int pageNumber,
                            TFilter filter)
            : base(pageSize, pageNumber) {
            this._sources = source;
            _filter = filter;
            Init();
        }

        public void SetData(Func<IQueryable<TModel>, IList>? mappingFunc = null)
        {
            Data = mappingFunc is null ? _sources.ToList() : mappingFunc(_sources);
        }

        public async Task SetDataAsync(Func<IQueryable<TModel>, Task<IList>>? mappingFunc = null) {            
            if (mappingFunc is null) {
                Data = await _sources.ToListAsync();
            }
            else {
                Data = await mappingFunc(_sources);
            }
        }

        private void Init() {
            _sources = (IQueryable<TModel>)_filter.Filtrate(_sources);
            TotalItemCount = _sources.Count();
            if(PageNumber == 0 && PageSize == 0)
            {
                return;
            }
            _sources = PageNumber == 1
                ? _sources.Skip(0).Take(PageSize)
                : _sources.Skip((PageNumber - 1) * PageSize).Take(PageSize);
        }

    }
}
