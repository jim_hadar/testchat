﻿using System.Collections;
using System.Linq;
using Framework.Paging;

namespace Framework.DAL.Paging.EF {
    /// <summary>
    /// Entity framework filter
    /// </summary>
    public interface IEfFilter : IBaseFilter {
        /// <summary>
        /// Rules for filtration
        /// </summary>
        /// <param name="data">data for sorting</param>
        /// <returns></returns>
        IQueryable Filtrate(IQueryable data);
        /// <summary>
        /// Rules for filtration
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        IList Filtrate(IList data);
    }
}
