﻿using System;

namespace Framework.DAL.Paging.EF {
    public class DateEfDefaultFilter : DateDefaultFilter {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateToCompare"></param>
        /// <returns></returns>
        protected bool DateComparerBetween(DateTime? dateToCompare) {
            return DateFrom.HasValue && !DateTo.HasValue && dateToCompare >= DateFrom
                   ||
                   !DateFrom.HasValue && DateTo.HasValue && dateToCompare <= DateTo
                   ||
                   DateFrom.HasValue && DateTo.HasValue && dateToCompare >= DateFrom && dateToCompare <= DateTo
                   ||
                   !DateFrom.HasValue && !DateTo.HasValue;
        }
    }
}
