﻿namespace Framework.DAL.Paging
{
    /// <summary>
    /// Filter by default
    /// </summary>
    public class DefaultFilter : BaseFilter
    {
        ///<inheritdoc/>
        protected override string DefaultSortField => "id";
    }
}