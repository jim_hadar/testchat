﻿using System;
using System.Collections;
using Newtonsoft.Json;

namespace Framework.DAL.Paging
{
    public class PagedList
    {
        private bool PageNumberIsGood => PageCount > 0 && PageNumber <= PageCount;
        private int NumberOfFirstItemOnPage => (PageNumber - 1) * PageSize + 1;
        private int NumberOfLastItemOnPage => NumberOfFirstItemOnPage + PageSize - 1;

        /// <summary>
        /// Total number of subsets within the superset.
        /// </summary>
        public int PageCount =>
            TotalItemCount > 0
                ? (int) Math.Ceiling(TotalItemCount / (double) PageSize)
                : 0;

        /// <summary>
        /// Total number of objects contained within the superset.
        /// </summary>
        public int TotalItemCount { get; protected set; }

        /// <summary>
        /// One-based index of this subset within the superset, zero if the superset is empty.
        /// </summary>
        public int PageNumber { get; }

        /// <summary>
        /// Maximum size any individual subset.
        /// </summary>
        public int PageSize { get; }

        /// <summary>
        /// Returns true if the superset is not empty and PageNumber is less than or equal to PageCount and this is NOT the first subset within the superset.
        /// </summary>
        public bool HasPreviousPage =>
            PageNumberIsGood && PageNumber > 1;

        /// <summary>
        /// Returns true if the superset is not empty and PageNumber is less than or equal to PageCount and this is NOT the last subset within the superset.
        /// </summary>
        public bool HasNextPage =>
            PageNumberIsGood && PageNumber < PageCount;

        /// <summary>
        /// Returns true if the superset is not empty and PageNumber is less than or equal to PageCount and this is the first subset within the superset.
        /// </summary>
        public bool IsFirstPage =>
            PageNumberIsGood && PageNumber == 1;

        /// <summary>
        /// Returns true if the superset is not empty and PageNumber is less than or equal to PageCount and this is the last subset within the superset.
        /// </summary>
        public bool IsLastPage =>
            PageNumberIsGood && PageNumber == PageCount;

        /// <summary>
        /// One-based index of the first item in the paged subset, zero if the superset is empty or PageNumber is greater than PageCount.
        /// </summary>
        /// <value>
        /// One-based index of the first item in the paged subset, zero if the superset is empty or PageNumber is greater than PageCount.
        /// </value>
        int FirstItemOnPage =>
            PageNumberIsGood ? NumberOfFirstItemOnPage : 0;

        /// <summary>
        /// One-based index of the last item in the paged subset, zero if the superset is empty or PageNumber is greater than PageCount.
        /// </summary>
        /// <value>
        /// One-based index of the last item in the paged subset, zero if the superset is empty or PageNumber is greater than PageCount.
        /// </value>
        int LastItemOnPage =>
            PageNumberIsGood
                ? (NumberOfLastItemOnPage > TotalItemCount
                    ? TotalItemCount
                    : NumberOfLastItemOnPage)
                : 0;

        /// <summary>
        /// Gets the number of elements contained on this page
        /// </summary>
        public int Count =>
            PageNumber < PageCount
                ? PageSize
                : TotalItemCount - (PageCount * PageSize - PageSize);

        /// <summary>
        /// result List of items,
        /// non-generic because may be use DTOs
        /// </summary>
        public IList? Data { get; protected set; }

        public PagedList(int pageSize, int pageNumber)
        {
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
        }
        
        [JsonConstructor]
        public PagedList(int pageSize, int pageNumber, int totalItemCount, IList data)
        {
            this.PageSize = pageSize;
            this.PageNumber = pageNumber;
            TotalItemCount = totalItemCount;
            Data = data;
        }
    }
}