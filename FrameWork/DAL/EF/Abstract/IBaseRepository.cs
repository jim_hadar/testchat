﻿using Framework.DAL.Models;
using Framework.Paging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Framework.DAL.Paging;
using Framework.DAL.Paging.EF;
using Framework.Exceptions;

namespace Framework.DAL.EF.Abstract {

    /// <summary>
    /// базовый репозиторий
    /// </summary>
    public interface IBaseRepository
    {
        
    }
    
    /// <summary>
    /// Репозиторий для работы с данными
    /// </summary>
    /// <typeparam name="TEntity">Тип сущности</typeparam>
    public interface IBaseRepository<TEntity> : IBaseRepository
        where TEntity : class {

        /// <summary>
        /// Набор данных
        /// </summary>
        IQueryable<TEntity> Query { get; }
        
        /// <summary>
        /// создание сущности 
        /// </summary>
        /// <param name="entity">Сущность для создания</param>
        /// <param name="withSave">Необходимо ли вызывать сразу Insert запрос к БД</param>
        /// <returns>Созданная сущность</returns>
        TEntity Create(TEntity entity, bool withSave = false);
        
        /// <summary>
        /// Асинхронное создание сущности
        /// </summary>
        /// <param name="entity">Сущность для создания</param>
        /// <returns>Созданная сущность</returns>
        Task<TEntity> CreateAsync(TEntity entity);
        
        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="entity">сущность для обновления</param>
        /// <param name="withSave">неоюходимо ли вызывать update запрос к БД</param>
        /// <returns>Обновленная сущность</returns>
        TEntity Update(TEntity entity, bool withSave = false);
        
        /// <summary>
        /// Обновление записи
        /// </summary>
        /// <param name="entity">Сущность для обновления</param>
        /// <returns>Обновленная сущность</returns>
        Task<TEntity> UpdateAsync(TEntity entity);
        
        /// <summary>
        /// Поиск сущности по ее идентификатору
        /// </summary>
        /// <param name="id">id сущность, которую необходимо получить</param>
        /// <exception cref="EntityNotFoundException">Если сущность с указанными параметрами не найдена</exception>
        /// <returns>Сущность из БД по ее идентификатору</returns>
        TEntity Get(long id, bool? isDeleted = false);
        
        /// <summary>
        /// Получение сущности по ее идентификатору
        /// </summary>
        /// <param name="id">id сущности, которую необходимо получить</param>
        /// <exception cref="EntityNotFoundException">Если сущность с указанными парааметрами не найдена</exception>
        /// <returns>Сущность из БД по ее идентификатору</returns>
        Task<TEntity> GetAsync(long id, bool? isDeleted = false);
        
        /// <summary>
        /// Получение всех сущностей, удовлетворяющих заданным условиям <paramref name="predicate"/> 
        /// </summary>
        /// <param name="predicate">Условие для выборки сущностей</param>
        /// <returns>Сущности из БД</returns>
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);
        
        /// <summary>
        /// Получение всех сущностей, удовлетворяющих заданным условиям <paramref name="predicate"/> 
        /// </summary>
        /// <param name="predicate">Условие для выборки сущностей</param>
        /// <returns>Сущности из БД</returns>
        Task<List<TEntity>> GetAsync(Func<TEntity, bool> predicate);
        
        /// <summary>
        /// Получение первой найденной сущности, удовлетворяющей заданным условиям <paramref name="predicate"/> 
        /// </summary>
        /// <param name="predicate">Условие для выборки сущностей</param>
        /// <returns>Сущность из БД</returns>
        TEntity FirstOrDefault(Func<TEntity, bool> predicate);
        
        /// <summary>
        /// Получение первой найденной сущности, удовлетворяющей заданным условиям <paramref name="predicate"/> 
        /// </summary>
        /// <param name="predicate">Условие для выборки сущностей</param>
        /// <returns>Сущность из БД</returns>
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Получение всех сущностей из БД, помеченных / не помеченных, как удаленные <paramref name="isDeleted"/> 
        /// </summary>
        /// <param name="isDeleted"></param>
        /// <returns>Список сущностей</returns>
        IQueryable<TEntity> GetAll(bool? isDeleted = false);

        /// <summary>
        /// Получение сущностей из БД, удовлетворяющих некоторым условияям, с постраничной разбивкой
        /// </summary>
        /// <param name="pagingParams">Параметры фильтрации и пагинации</param>
        /// <param name="mappingFunc">Функций мапирования сущностей БД</param>
        /// <param name="isDeleted">Нужно ли учитывать сущности, которые помечены в БД, как удаленные</param>
        /// <typeparam name="TFilter">Тип фильтра</typeparam>
        /// <returns>Отфильтрованные сущности с постраничной разюивкой</returns>
        Task<PagedEfList<TFilter, TEntity>> GetPagedList<TFilter>(PagingParams<TFilter> pagingParams,
                                                                  Func<IQueryable<TEntity>, Task<IList>> mappingFunc,
                                                                  bool? isDeleted = false)
            where TFilter : BaseEFFilter;

        /// <summary>
        /// Удаление сущности 
        /// </summary>
        /// <param name="entity">Сущность для удаления</param>
        /// <param name="withSave">Необходимо ли вызывать SaveChanges</param>
        /// <returns>количество удаленных сущностей</returns>
        void Delete(TEntity entity, bool withSave = false);
        
        /// <summary>
        /// delete entity with calling SaveChanges
        /// </summary>
        /// <param name="entity">Сущность для удаления</param>
        /// <returns>Количество удаленных сущностей</returns>
        Task<int> DeleteAsync(TEntity entity);

        /// <summary>
        /// delete entities by expression without calling SaveChanges
        /// </summary>
        /// <param name="predicate">Условия выборки для удаления сущностей</param>
        /// <param name="withSave">Необходимо ли вызывать SaveChanges</param>
        /// <returns>количество удаленных сущностей</returns>
        void Delete(Func<TEntity, bool> predicate, bool withSave = false);

        /// <summary>
        /// delete entities by expression with calling SaveChangesAsync
        /// </summary>
        /// <param name="predicate">условие выборки данных для удаления</param>
        /// <returns>Количество удаленных сущностей</returns>
        Task<int> DeleteAsync(Func<TEntity, bool> predicate);

        /// <summary>
        /// Удаление сущности по ее идентификатору без вызова SaveChanges 
        /// </summary>
        /// <param name="id">Идентификатор сущности для удаления</param>
        /// <param name="withSave">Необходимо ли вызывать SaveChanges</param>
        void Delete(long id, bool withSave = false);

        /// <summary>
        /// удаление сущности по ее идентификатору с вызовом SaveChanges
        /// </summary>
        /// <param name="id">идентификатор сущности для удаления</param>
        /// <returns>Асинхронная операция</returns>
        Task DeleteAsync(long id);

        /// <summary>
        /// Attach
        /// </summary>
        /// <param name="t">object attach.</param>
        void Attach(TEntity t);

        /// <summary>
        /// Detach
        /// </summary>
        /// <param name="t">object attach.</param>
        void Detach(TEntity t);

        int Count { get; }

        Task<int> CountAsync();
    }
}
