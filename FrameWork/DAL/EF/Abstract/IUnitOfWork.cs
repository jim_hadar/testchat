﻿using Framework.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Framework.DAL.EF.Abstract {
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();

        void CommitTransaction();

        void RollBackTransaction();

        IBaseRepository<TEntity> Repository<TEntity>()
            where TEntity : class;

        Task Commit();

        void RollBack();
    }
}
