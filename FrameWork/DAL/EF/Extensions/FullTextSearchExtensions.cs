﻿using Framework.DAL.Attributes;
using Framework.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
// ReSharper disable PossibleNullReferenceException

#nullable disable
namespace Framework.DAL.EF.Extensions {
    public static class FullTextSearchExtensions {
        private const string KeyFullTextSearch = "{932DB32F-30C2-425E-B0C6-13697FE3C1E9}";

        private static readonly object СacheLock = new object();

        public static string RemoveSpecialCharacters(this string str) {
            return str.Replace("'", @"\'").Replace("\"", @"\'");
        }

        private static Dictionary<string, PropertyInfo> WrapQuery(Dictionary<string, PropertyInfo> props) {
            Dictionary<string, PropertyInfo> strings = new Dictionary<string, PropertyInfo>();

            Regex regex = new Regex(Regex.Escape("[]"));

            foreach (KeyValuePair<string, PropertyInfo> prop in props.Where(x => x.Value.PropertyType == typeof(string))) {
                string query = null;

                if (prop.Key.Contains("[]")) {
                    string propPath = prop.Key.Replace("[].", "[]");

                    string propName = propPath;

                    string[] arr = propName.Split("[]".ToCharArray());

                    if (arr.Length > 0) {
                        propName = arr[^1];
                    }

                    query = propPath + " != null && " + propName + ".ToUpper().Contains(\"{0}\".ToUpper())";

                    while (query.Contains("[]")) {
                        query = regex.Replace(query, ".Where(", 1);
                        query += ").Any()";
                    }
                }
                else {
                    query = prop.Key + " != null && " + prop.Key + ".ToUpper().Contains(\"{0}\".ToUpper())";
                }

                strings.Add(query, prop.Value);
            }

            return strings;
        }

        public static Dictionary<string, PropertyInfo> GetAttributedProperties(Type type, Dictionary<string, PropertyInfo> props = null, PropertyInfo currentProp = null, bool isCollection = false, int depth = 1, string path = "") {
            if (props == null) {
                props = new Dictionary<string, PropertyInfo>();
            }

            if (depth >= 0) {
                foreach (var prop in type.GetProperties(BindingFlags.Public | BindingFlags.Instance)) {
                    FullTextSearchPropertyAttribute attr = prop.GetCustomAttribute<FullTextSearchPropertyAttribute>();

                    string newPath = path;

                    if (attr != null) {
                        Type collectionType = prop.PropertyType.GetInterface("IEnumerable`1");

                        if (currentProp != null) {
                            if (collectionType != null && prop.PropertyType != typeof(String)) {
                                newPath = newPath + "." + prop.Name + "[]";
                            }
                            else {
                                newPath = newPath + "." + prop.Name;
                            }
                        }
                        else {
                            if (collectionType != null && prop.PropertyType != typeof(String)) {
                                newPath = prop.Name + "[]";
                            }
                            else {
                                newPath = prop.Name;
                            }
                        }

                        if (!props.ContainsKey(newPath)) {
                            props.Add(newPath, prop);

                            if (prop.PropertyType != typeof(String) && collectionType != null) {
                                Type collectionEntryType = collectionType.GetGenericArguments()[0];

                                GetAttributedProperties(collectionEntryType, props, prop, true, depth: attr.Depth - 1, path: newPath);
                            }
                            else {
                                GetAttributedProperties(prop.PropertyType, props, prop, depth: attr.Depth - 1, path: newPath);
                            }
                        }
                    }
                }
            }

            return props;
        }

        public static IQueryable<T> FullTextSearch<T>(this IQueryable<T> query, string searchStr, ICacheWrapper cache) {
            return (IQueryable<T>)FullTextSearch((IQueryable)query, searchStr, cache);
        }
        /// <summary>
        /// Поиск по заданным полям, при этом поля должны быть помечены атрибутом
        /// FullTextSearchPropertyAttribute
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="searchStr">The search string.</param>
        /// <param name="cache">The cache.</param>
        /// <returns></returns>
        public static IQueryable FullTextSearch(this IQueryable query, string searchStr, ICacheWrapper cache) {
            if (String.IsNullOrEmpty(searchStr) || String.IsNullOrWhiteSpace(searchStr)) {
                return query;
            }
            var type = query.GetType().GetGenericArguments()[0];

            Dictionary<string, PropertyInfo> props;

            lock (СacheLock) {
                if (cache[KeyFullTextSearch] == null) {
                    cache[KeyFullTextSearch] = new Dictionary<Type, Dictionary<string, PropertyInfo>>();
                }

                var dic = cache[KeyFullTextSearch] as Dictionary<Type, Dictionary<string, PropertyInfo>>;

                if (!dic.ContainsKey(type)) {
                    dic.Add(type, WrapQuery(GetAttributedProperties(type)));
                }

                props = dic[type];
            }

            if (props.Count != 0) {
                string criteria = string.Format(string.Join(" or ", props.Select(_ => _.Key)), searchStr);

                return query.Where(criteria);
            }

            return query;
        }
    }
#nullable restore
}
