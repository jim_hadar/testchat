﻿using Framework.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.DAL.EF.Settings {
    public class Setting : BaseObject {
        public string Name { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        public string? Value { get; set; }
    }
}
