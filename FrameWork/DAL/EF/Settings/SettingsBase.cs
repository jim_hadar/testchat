﻿using Framework.DAL.EF.Abstract;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Framework.DAL.EF.Settings
{
    public abstract class SettingsBase
    {
        private readonly string _name;
        private readonly PropertyInfo[] _properties;

        protected SettingsBase()
        {
            var type = GetType();
            _name = type.Name;
            _properties = type.GetProperties();
        }

        public virtual void Load(IUnitOfWork ufw)
        {
            var settings = ufw.Repository<Setting>().Get(s => s.Type == _name).ToList();
            foreach (var prop in _properties)
            {
                var setting = settings.SingleOrDefault(s => s.Name == prop.Name);
                if (setting != null)
                {
                    prop.SetValue(this, Convert.ChangeType(setting.Value, prop.PropertyType));
                }
            }
        }

        public virtual void Save(IUnitOfWork unitOfWork)
        {
            var repo = unitOfWork.Repository<Setting>();
            var settings = repo.Get(w => w.Type == _name).ToList();
            foreach (var propInfo in _properties)
            {
                object? propValue = propInfo.GetValue(this, null);
                if (propValue == null)
                {
                    continue;
                }
                string? value = propValue?.ToString();

                var setting = settings.SingleOrDefault(_ => _.Name == propInfo.Name);
                if (setting != null)
                {
                    setting.Value = value;
                }
                else
                {
                    var newSetting = new Setting
                    {
                        Name = propInfo.Name,
                        Type = _name,
                        Value = value
                    };
                    repo.Create(newSetting);
                }
            }
        }
    }
}