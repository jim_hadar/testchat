﻿using Microsoft.EntityFrameworkCore;

namespace Framework.DAL.EF.Settings {
    public static class SettingsDbModelBuilderExtenions {
        public static void BuildSetting(this ModelBuilder modelBuilder) {
            modelBuilder.Entity<Setting>(b => {
                b.HasKey(x => new { x.Name, x.Type });
                b.Property(x => x.Value)
                  .IsRequired(false);
                b.ToTable(nameof(Setting), "settings");
            });
        }
    }
}
