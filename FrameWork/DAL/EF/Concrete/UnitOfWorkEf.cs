﻿using Framework.DAL.EF.Abstract;
using Framework.DAL.Models;
using Framework.Wrappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Framework.DAL.EF.Concrete {
    /// <inheritdoc/>
    public class UnitOfWorkEf : IUnitOfWork {
        private readonly Dictionary<Type, IBaseRepository> _repositories;
        private readonly DbContext _context;
        private readonly ICacheWrapper _cache;
        
        public UnitOfWorkEf(DbContext context, ICacheWrapper cache) {
            _context = context;
            _repositories = new Dictionary<Type, IBaseRepository>();
            _cache = cache;
        }

        /// <inheritdoc/>
        public IBaseRepository<TEntity> Repository<TEntity>()
            where TEntity : class
        {
            if (!_repositories.ContainsKey(typeof(TEntity)))
            {
                _repositories.Add(typeof(TEntity), new BaseRepository<TEntity>(_context, _cache));
            }

            return (IBaseRepository<TEntity>) _repositories[typeof(TEntity)];
        }

        /// <inheritdoc/>
        public void CommitTransaction()
        {
            _context.Database.CommitTransaction();
        }

        /// <inheritdoc/>
        public void BeginTransaction()
        {
            _context.Database.BeginTransaction();
        }

        /// <inheritdoc/>
        public void RollBackTransaction()
        {
            _context.Database.RollbackTransaction();
        }

        /// <inheritdoc/>
        public async Task Commit() {
            await _context.SaveChangesAsync();
        }
        
        /// <inheritdoc/>
        public void Dispose()
        {
            _context?.Dispose();
        }

        /// <inheritdoc/>
        public void RollBack() {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }
    }
}
