﻿using Framework.DAL.EF.Abstract;
using Framework.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Framework.Wrappers;
using Framework.Exceptions;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using Framework.DAL.EF.Extensions;
using Framework.DAL.Paging;
using Framework.DAL.Paging.EF;
using Framework.Extensions;
using Framework.Paging;

namespace Framework.DAL.EF.Concrete
{
    /// <inheritdoc />
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class
    {
        private readonly DbContext _context;
        private DbSet<TEntity> Set => _context.Set<TEntity>();

        private readonly ICacheWrapper _cache;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="cache">The cache.</param>
        public BaseRepository(DbContext context, ICacheWrapper cache)
        {
            _context = context;
            _cache = cache;
        }

        /// <inheritdoc />
        public IQueryable<TEntity> Query => Set;

        /// <inheritdoc />
        public int Count => Set.Count();

        /// <inheritdoc />
        public Task<int> CountAsync()
            => Set.CountAsync();

        /// <inheritdoc />
        public TEntity Create(TEntity entity, bool withSave = false)
        {
            Set.Attach(entity);
            _context.Entry(entity).State = EntityState.Added;
            if (withSave)
            {
                _context.SaveChanges();
            }

            return entity;
        }

        /// <inheritdoc />
        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            Create(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        /// <inheritdoc />
        public TEntity Update(TEntity entity, bool withSave = false)
        {
            Set.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            if (withSave)
            {
                _context.SaveChanges();
            }

            return entity;
        }

        /// <inheritdoc />
        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            Update(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        /// <inheritdoc />
        public void Delete(Func<TEntity, bool> predicate, bool withSave = false)
        {
            var toDel = Set.Where(predicate).ToList();

            if (toDel.Any())
            {
                if (toDel.First() is IBaseObject)
                {
                    toDel.ForEach(e => { ((IBaseObject) e).IsDeleted = true; });
                    _context.UpdateRange(toDel);
                }
                else
                {
                    _context.RemoveRange(toDel);
                }
            }

            if (withSave)
            {
                _context.SaveChanges();
            }
        }

        /// <inheritdoc />
        public async Task<int> DeleteAsync(Func<TEntity, bool> predicate)
        {
            Delete(predicate);
            return await _context.SaveChangesAsync();
        }

        /// <inheritdoc />
        public void Delete(TEntity entity, bool withSave = false)
        {
            if (entity is IBaseObject baseObject)
            {
                baseObject.IsDeleted = true;
                _context.Update(entity);
            }
            else
            {
                Set.Remove(entity);
            }

            if (withSave)
            {
                _context.SaveChanges();
            }
        }

        /// <inheritdoc />
        public async Task<int> DeleteAsync(TEntity entity)
        {
            Delete(entity);
            return await _context.SaveChangesAsync();
        }

        /// <inheritdoc />
        public void Delete(long id, bool withSave = false)
        {
            ThrowIfNotIBaseObjectImpl();
            var entity = Query.FirstOrDefault(_ => ((IBaseObject) _).Id == id);
            Delete(entity, withSave);
        }

        /// <inheritdoc />
        public async Task DeleteAsync(long id)
        {
            ThrowIfNotIBaseObjectImpl();
            var entity = await Query.FirstOrDefaultAsync(_ => ((IBaseObject) _).Id == id);
            if (entity != null)
            {
                await DeleteAsync(entity);
            }
        }

        /// <inheritdoc />
        public void Attach(TEntity entity)
        {
            Set.Attach(entity);
        }

        /// <inheritdoc />
        public void Detach(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Detached;
        }

        /// <inheritdoc />
        public TEntity Get(long id, bool? isDeleted)
        {
            ThrowIfNotIBaseObjectImpl();
            var entity = isDeleted.HasValue
                ? Set.FirstOrDefault(_ => ((IBaseObject) _).Id == id && ((IBaseObject) _).IsDeleted == isDeleted.Value)
                : Set.FirstOrDefault(_ => ((IBaseObject) _).Id == id);

            if (entity == null)
            {
                throw new EntityNotFoundException($"Не найдено записи с id = {id}");
            }

            return entity;
        }

        /// <inheritdoc />
        public async Task<TEntity> GetAsync(long id, bool? isDeleted = false)
        {
            ThrowIfNotIBaseObjectImpl();
            var entity = isDeleted.HasValue
                ? await Set.FirstOrDefaultAsync(_ =>
                    ((IBaseObject) _).Id == id && ((IBaseObject) _).IsDeleted == isDeleted.Value)
                : await Set.FirstOrDefaultAsync(_ => ((IBaseObject) _).Id == id);
            if (entity == null)
            {
                throw new EntityNotFoundException($"Не найдено записи с id = {id}");
            }

            return entity;
        }

        /// <inheritdoc />
        public TEntity FirstOrDefault(Func<TEntity, bool> predicate)
            => Set.FirstOrDefault(predicate);

        /// <inheritdoc />
        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
            => await Set.FirstOrDefaultAsync(predicate, CancellationToken.None);


        /// <inheritdoc />
        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return Set.Where(predicate);
        }

        /// <inheritdoc />
        public async Task<List<TEntity>> GetAsync(Func<TEntity, bool> predicate)
            => await Task.Run(() => Set.Where(predicate).ToList());

        /// <inheritdoc />
        public IQueryable<TEntity> GetAll(bool? isDeleted)
        {
            ThrowIfNotIBaseObjectImpl();
            return isDeleted.HasValue ? Query.Where(_ => ((IBaseObject) _).IsDeleted == isDeleted.Value) : Query;
        }

        /// <inheritdoc />
        public async Task<PagedEfList<TFilter, TEntity>> GetPagedList<TFilter>(
            PagingParams<TFilter> pagingParams,
            Func<IQueryable<TEntity>, Task<IList>> mappingFunc,
            bool? isDeleted = false)
            where TFilter : BaseEFFilter
        {
            if (typeof(TEntity).GetInterface(nameof(IBaseObject)) != null && isDeleted.HasValue)
            {
                return await Set.Where(_ => ((IBaseObject) _).IsDeleted == isDeleted.Value)
                        .FullTextSearch(pagingParams.Filter.Search, _cache).ToPagedListAsync(pagingParams, mappingFunc);
                
            }
            return await Set.FullTextSearch(pagingParams.Filter.Search, _cache).ToPagedListAsync(pagingParams, mappingFunc);
        }

        private void ThrowIfNotIBaseObjectImpl()
        {
            if (typeof(TEntity).GetInterface(nameof(IBaseObject)) == null)
            {
                throw new NotSupportedException(
                    $"Данная операция не поддерживается, так как сущность не реализует интерфейс {nameof(IBaseObject)}");
            }
        }
    }
}