﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.DAL.Attributes {
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class EnableFullTextSearchAttribute : Attribute {
        public EnableFullTextSearchAttribute(bool enableFullTextSearch) {
            Enabled = enableFullTextSearch;
        }

        public EnableFullTextSearchAttribute() {
            Enabled = true;
        }

        public bool Enabled { get; }
    }
}
