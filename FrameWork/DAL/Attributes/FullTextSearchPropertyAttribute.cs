﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.DAL.Attributes {
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public sealed class FullTextSearchPropertyAttribute : Attribute {
        public FullTextSearchPropertyAttribute(int depth) {
            Depth = depth;
        }

        public FullTextSearchPropertyAttribute() {
            Depth = 1;
        }

        public int Depth { get; }
    }
}
