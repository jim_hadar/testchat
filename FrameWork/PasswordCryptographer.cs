﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Framework {
    public class PasswordCryptographer {
        const int SaltLength = 6;
        const string Delim = "*";

        private string SaltPassword(string password, string salt) {
            SHA512 hashAlgorithm = SHA512.Create();
            return Convert.ToBase64String(hashAlgorithm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(salt + password)));
        }
        public virtual string GenerateSaltedPassword(string password) {
            if (string.IsNullOrEmpty(password))
                return password;
            byte[] randomSalt = new byte[SaltLength];
            new RNGCryptoServiceProvider().GetBytes(randomSalt);

            string salt = Convert.ToBase64String(randomSalt);
            return salt + Delim + SaltPassword(password, salt);
        }

        public virtual bool AreEqual(string saltedPassword, string password) {
            if (string.IsNullOrEmpty(saltedPassword))
                return string.IsNullOrEmpty(password);
            if (string.IsNullOrEmpty(password)) {
                return false;
            }
            int delimPos = saltedPassword.IndexOf(Delim, StringComparison.Ordinal);
            if (delimPos <= 0) {
                return false;
            }
            else {
                string calculatedSaltedPassword = SaltPassword(password, saltedPassword.Substring(0, delimPos));
                string expectedSaltedPassword = saltedPassword.Substring(delimPos + Delim.Length);
                if (expectedSaltedPassword.Equals(calculatedSaltedPassword)) {
                    return true;
                }
                return expectedSaltedPassword.Equals(SaltPassword(password, "System.Byte[]"));
            }
        }
    }
}
