﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Wrappers {
    public interface ICacheWrapper {        

        bool IsInitialized { get; }

        int Count { get; }

        object this[string key] { get; set; }

        object Get(string key);

        bool TryGetValue(string key, out object value);

        bool TryGetValue<T>(string key, out T value);

        void Remove(string key);

        void Set(string key, object value);

        void Set<T>(string key, T value);
    }
}
