﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Caching.Memory;

namespace Framework.Wrappers {
    public class CacheWrapper : ICacheWrapper {
        readonly IMemoryCache _memoryCache;
        public CacheWrapper(IMemoryCache memoryCache) {
            _memoryCache = memoryCache;
        }
        public object this[string key] { 
            get => Get(key); 
            set => Set(key, value); 
        }

        public bool IsInitialized => _memoryCache != null;

        public int Count => ((MemoryCache)_memoryCache).Count;

        public object Get(string key) => 
            _memoryCache.Get(key);

        public void Remove(string key) => 
            _memoryCache.Remove(key);

        public void Set(string key, object value) => 
            _memoryCache.Set(key, value);

        public void Set<T>(string key, T value) => 
            _memoryCache.Set<T>(key, value);

        public bool TryGetValue(string key, out object value) =>
            _memoryCache.TryGetValue(key, out value);

        public bool TryGetValue<T>(string key, out T value) =>
            _memoryCache.TryGetValue<T>(key, out value);
    }
}
