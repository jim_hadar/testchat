using System.Net.Http;
using System.Text;
using System.Text.Json;

namespace TestChat.IntegrationTests.Extensions
{
    public static class JsonExtensions
    {
        public static HttpContent AsJson(this object obj)
        {
            string stringContent = JsonSerializer.Serialize(obj, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            });
            return new StringContent(stringContent, Encoding.UTF8, "application/json");
        }

        public static TResult DeserializeWithCamelCase<TResult>(this string obj)
        {
            return JsonSerializer.Deserialize<TResult>(obj, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            });
        }
    }
}