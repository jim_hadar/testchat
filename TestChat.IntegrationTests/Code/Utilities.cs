﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Framework;
using Framework.DAL.EF.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestChat.Data.Common;
using TestChat.Data.Models;
using TestChat.IntegrationTests.Helpers.Data;

namespace TestChat.IntegrationTests.Code
{
    public static class Utilities
    {
        public static async Task InitializeTestDb(this IServiceProvider scopedSp)
        {
            var ufw = scopedSp.GetRequiredService<IUnitOfWork>();
            var roleRepo = ufw.Repository<UserRole>();
            var userRepo = ufw.Repository<User>();
            var chatRepo = ufw.Repository<Chat>();
            var messageRepo = ufw.Repository<ChatMessage>();
            
            #region [ Заполнение ролей пользователей ]
            
            var adminRole = new UserRole
            {
                DisplayName = "Администратор",
                Name = UserType.Administrator,
            };
            await roleRepo.CreateAsync(adminRole);
            var userRole = new UserRole
            {
                DisplayName = "Пользователь",
                Name = UserType.User
            };
            await roleRepo.CreateAsync(userRole);

            DataHelper.UserRoles = await roleRepo.GetAll().ToListAsync();

            #endregion

            #region [ Заполнение пользователей ]

            // администратор по умолчанию
            var passwordHasher = scopedSp.GetRequiredService<PasswordCryptographer>();
            adminRole = await roleRepo.FirstOrDefaultAsync(_ => _.Name == UserType.Administrator);
            userRole = await roleRepo.FirstOrDefaultAsync(_ => _.Name == UserType.User);
            var defaultUser = new User
            {
                Name = UserType.Administrator,
                Login = UserType.Administrator,
                Password = passwordHasher.GenerateSaltedPassword("Qwerty7"),
                RoleId = adminRole.Id,
                Role = adminRole,
            };
            await userRepo.CreateAsync(defaultUser);
            // создаем остальных юзеров
            foreach (var user in DataHelper.Users)
            {
                user.Password = passwordHasher.GenerateSaltedPassword(user.Password);
                user.RoleId = user.Login.Contains("admin") ? adminRole.Id : userRole.Id;
                await userRepo.CreateAsync(user);
            }

            DataHelper.Users = await userRepo.GetAll().ToListAsync();

            #endregion

            #region [ Заполнение чатов пользователей ]
            // создание по 4 чата для каждого пользователя
            for (int i = 0; i < DataHelper.Users.Count; i++)
            {
                var chat = new Chat
                {
                    Name = $"UserChat{DataHelper.Users[i]}1",
                    CreateUserId = DataHelper.Users[i].Id,
                    DateCreate = DateTime.Now,
                };
                chatRepo.Create(chat);
                chat = new Chat
                {
                    Name = $"UserChat{DataHelper.Users[i]}2",
                    CreateUserId = DataHelper.Users[i].Id,
                    DateCreate = DateTime.Now,
                };
                chatRepo.Create(chat);
                chat = new Chat
                {
                    Name = $"UserChat{DataHelper.Users[i]}3",
                    CreateUserId = DataHelper.Users[i].Id,
                    DateCreate = DateTime.Now,
                };
                chatRepo.Create(chat);
                chat = new Chat
                {
                    Name = $"UserChat{DataHelper.Users[i]}4",
                    CreateUserId = DataHelper.Users[i].Id,
                    DateCreate = DateTime.Now,
                };
                chatRepo.Create(chat);
            }
            await ufw.Commit();
            
            DataHelper.Chats = await chatRepo.GetAll().ToListAsync();
            
            //добавление в каждый чат пользователей
            for (int i = 0; i < DataHelper.Chats.Count; i++)
            {
                var tmpChat = DataHelper.Chats[i];
                var chat = await chatRepo.GetAsync(tmpChat.Id);
                chat.Users.Add(new ChatUser
                {
                    UserId = chat.CreateUserId,
                    ChatId = chat.Id,
                });
                var randomUser = DataHelper.Users[new Random().Next(0, 49)];
                if (chat.Users.All(_ => _.UserId != randomUser.Id))
                {
                    chat.Users.Add(new ChatUser
                    {
                        UserId = randomUser.Id,
                        ChatId = chat.Id
                    });
                }
                chatRepo.Update(chat);
            }

            await ufw.Commit();
            
            DataHelper.Chats = await chatRepo.GetAll().ToListAsync();

            #endregion

            #region [ Заполнение сообщений чатов ]

            foreach (var chat in DataHelper.Chats)
            {
                foreach (var user in chat.Users)
                {
                    var message = new ChatMessage
                    {
                        ChatId = chat.Id,
                        Chat = chat,
                        DateCreate = DateTime.Now,
                        Content = $"test message {user.User.Name}. Test chat {chat.Name}",
                        UserId = user.UserId,
                        User = user.User
                    };
                    messageRepo.Create(message);
                }
            }

            await ufw.Commit();

            DataHelper.ChatMessages = await messageRepo.GetAll().ToListAsync();

            #endregion
        }

        private static async Task InitializeDefaultUser(this IServiceProvider serviceProvider)
        {
            try
            {
                using var scope = serviceProvider.CreateScope();
                var scopedServiceProvider = scope.ServiceProvider;
                var ufw = scopedServiceProvider.GetRequiredService<IUnitOfWork>();
                var userRepo = ufw.Repository<User>();
                var passwordHasher = scopedServiceProvider.GetRequiredService<PasswordCryptographer>();
                if (!userRepo.Query.Any())
                {
                    var adminRole = await ufw.Repository<UserRole>().FirstOrDefaultAsync(_ => _.Name == UserType.Administrator);
                    var defaultUser = new User
                    {
                        Name = UserType.Administrator,
                        Login = UserType.Administrator,
                        Password = passwordHasher.GenerateSaltedPassword("Qwerty7"),
                        RoleId = adminRole.Id,
                        Role = adminRole,
                    };
                    await userRepo.CreateAsync(defaultUser);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
    }
}