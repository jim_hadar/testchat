﻿using System;
using System.Linq;
using Framework.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.DependencyInjection;
using TestChat.Data.Contexts;
using TestChat.WebApi;

namespace TestChat.IntegrationTests.Code
{
    public class DbInMemoryAppFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            // base;
            builder.ConfigureServices(services =>
            {
                var descriptor = services.FirstOrDefault(_ => _.ServiceType == typeof(DbContextOptions<ChatContext>));
                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                services.AddDbContext<ChatContext>(opt =>
                {
                    opt.UseInMemoryDatabase("InMemoryDbForTesting");
                    opt.UseLazyLoadingProxies();
                });

                var sp = services.BuildServiceProvider();
                using var scope = sp.CreateScope();
                var scopedSp = scope.ServiceProvider;
                var db = scopedSp.GetRequiredService<ChatContext>();
                if (!db.Database.EnsureCreated())
                {
                    return;
                }
                try
                {
                    scopedSp.InitializeTestDb().Wait();
                }
                catch (Exception ex)
                {
                   Console.WriteLine(ex.Message);
                   throw;
                }
            });
        }
    }
}