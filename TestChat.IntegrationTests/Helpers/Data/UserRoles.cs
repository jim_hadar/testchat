using System.Collections.Generic;
using TestChat.Data.Models;

namespace TestChat.IntegrationTests.Helpers.Data
{
    public partial class DataHelper
    { 
        public static List<UserRole> UserRoles { get; set; }
    }
}