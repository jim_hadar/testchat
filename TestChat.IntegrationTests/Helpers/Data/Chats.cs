using System.Collections.Generic;
using TestChat.Data.Models;

namespace TestChat.IntegrationTests.Helpers.Data
{
    public partial class DataHelper
    {
        /// <summary>
        /// Чаты
        /// </summary>
        public static List<Chat> Chats { get; set; }
        
    }
}