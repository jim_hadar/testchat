﻿using System.Collections.Generic;
using TestChat.Data.Models;

namespace TestChat.IntegrationTests.Helpers.Data
{
    public partial class DataHelper
    {
        private static List<User> _users;

        public static List<User> Users
        {
            get => _users ??= CreateUsers();
            set
            {
                if (value != null)
                {
                    _users = value;
                }
            }
        }

        private static List<User> CreateUsers()
        {
            var result = new List<User>();
            
            for (int i = 0; i < 50; i++)
            {
                var user = new User
                {
                    Name = $"User{i}",
                    Login = i % 2 == 0 ? $"adminsitrator{i}" : $"user{i}",
                    Password = $"User157",
                };
                result.Add(user);
            }

            _users = result;

            return result;
        }
    }
}