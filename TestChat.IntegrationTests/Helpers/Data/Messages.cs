using System.Collections.Generic;
using TestChat.Data.Models;

namespace TestChat.IntegrationTests.Helpers.Data
{
    public partial class DataHelper 
    {
        /// <summary>
        /// Сообщения
        /// </summary>
        public static List<ChatMessage> ChatMessages { get; set; }
    }
}