using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Framework.DAL.Paging;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TestChat.Business.Models;
using TestChat.Data.Common;
using TestChat.IntegrationTests.Code;
using TestChat.IntegrationTests.Extensions;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    public abstract class BaseTestClass : IClassFixture<DbInMemoryAppFactory>
    {
        protected virtual string BaseUrl => "/api/v1/";
        private readonly DbInMemoryAppFactory _appFactory;

        protected BaseTestClass(DbInMemoryAppFactory appFactory)
        {
            _appFactory = appFactory;
        }

        protected async Task<TResult> GetSuccessObjectTest<TResult>(string url, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.GetAsync(url);
            var resultString = await response.Content.ReadAsStringAsync();
            if (response.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine(resultString);
            }
            response.EnsureSuccessStatusCode();
            var result = resultString.DeserializeWithCamelCase<TResult>();
            return result;
        }
        
        protected async Task GetFailedObjectTest(string url, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.GetAsync(url);
            var resultString = await response.Content.ReadAsStringAsync();
            Assert.Equal( HttpStatusCode.BadRequest, response.StatusCode);
        }

        protected async Task GetObjectTestWithStatusCode(string url, HttpStatusCode statusCode, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.GetAsync(url);
            var resultString = await response.Content.ReadAsStringAsync();
            Assert.Equal( statusCode, response.StatusCode);
        }

        protected async Task<TResult> PostSuccessObjectTest<TResult>(string url, object obj, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.PostAsync(url, obj.AsJson());
            var resultString = await response.Content.ReadAsStringAsync();
            response.EnsureSuccessStatusCode();
            var result = resultString.DeserializeWithCamelCase<TResult>();
            return result;
        }
        
        protected async Task PostFailedObjectTest(string url, object obj, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.PostAsync(url, obj.AsJson());
            var resultString = await response.Content.ReadAsStringAsync();
            Assert.Equal( HttpStatusCode.BadRequest, response.StatusCode);
        }

        protected async Task<TResult> PutSuccessObjectTest<TResult>(string url, object obj, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.PutAsync(url, obj.AsJson());
            var resultString = await response.Content.ReadAsStringAsync();
            response.EnsureSuccessStatusCode();
            var result = resultString.DeserializeWithCamelCase<TResult>();
            return result;
        }
        
        protected async Task PutFailedObjectTest(string url, object obj, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.PutAsync(url, obj.AsJson());
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            var resultString = await response.Content.ReadAsStringAsync();
        }

        protected async Task DeleteSuccessObjectTest(string url, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.DeleteAsync(url);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        
        protected async Task DeleteFailedObjectTest(string url, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.DeleteAsync(url);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        protected async Task<PagedList> GetPagedListTest(string url, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.GetAsync(url);
            var resultString = await response.Content.ReadAsStringAsync();
            if (response.StatusCode != HttpStatusCode.OK)
            {
                
            }
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<PagedList>(resultString, new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                }
            });
            return result;
        }

        protected async Task<List<TResult>> GetPagedListTest<TResult>(string url, Credentials credentials = null)
        {
            var paged = await GetPagedListTest(url, credentials);
            var result = new List<TResult>();
            foreach (var item in paged.Data)
            {
                result.Add(JsonConvert.DeserializeObject<TResult>(item.ToString()));
            }

            return result;
        }

        protected async Task TestSortingPagedList<TResult>(
            string url,
            string[] excludedFromSortProps = null,
            Credentials credentials = null)
        {
            var type = typeof(TResult);
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (excludedFromSortProps != null && excludedFromSortProps.Any() && excludedFromSortProps.Contains(prop.Name))
                    continue;
                string tempUrl = url + $"&sort={prop.Name}&order=asc";
                var res = await GetPagedListTest<TResult>(tempUrl, credentials);
                tempUrl = url + $"&sort={prop.Name}&order=desc";
                var pagedList = await GetPagedListTest(tempUrl, credentials);
            }
        }

        protected async Task DeleteForbiddenTest(string url, Credentials credentials = null)
        {
            var client = await GetAuthClient(credentials);
            var response = await client.DeleteAsync(url);
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
        }

        protected HttpClient GetClient()
        {
            var client = _appFactory.CreateClient();
            
            return client;
        }

        protected async Task<HttpClient> GetAuthClient(Credentials credentials = null)
        {
            var client = GetClient();
            var token = await GetJwtToken(credentials);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            return client;
        }
            
        protected async Task<string> GetJwtToken(Credentials credentials)
        {
            if (credentials == null)
            {
                credentials = new Credentials
                {
                    UserName = UserType.Administrator,
                    Password = "Qwerty7",
                };
            }

            var client = GetClient();
            var response = await client.PostAsync($"api/v1/auth/login", credentials.AsJson());
            response.EnsureSuccessStatusCode();
            var resultString = await response.Content.ReadAsStringAsync();
            var result = resultString.DeserializeWithCamelCase<Auth>();
            return result.AuthToken;
        }
    }

    class Auth
    {
        public string AuthToken { get; set; }
    }
}