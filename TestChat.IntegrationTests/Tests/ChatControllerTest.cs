using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TestChat.Business.Models;
using TestChat.Business.Models.Chats;
using TestChat.Business.Models.Users;
using TestChat.Data.Common;
using TestChat.Data.Models;
using TestChat.IntegrationTests.Code;
using TestChat.IntegrationTests.Helpers.Data;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    /// <summary>
    /// Тесты для контроллера чата
    /// </summary>
    public class ChatControllerTest : BaseTestClass
    {
        protected override string BaseUrl => "api/v1/chat/";

        public ChatControllerTest(DbInMemoryAppFactory appFactory) 
            : base(appFactory)
        {
            
        }

        [Fact]
        public async Task GetChatByIdTest()
        {
            //проверка простого получения чата
            await GetAuthClient();
            var expChat = DataHelper.Chats[30];
            var actChat = await GetSuccessObjectTest<ChatModel>($"{BaseUrl}{expChat.Id}");
            Assert.Equal(expChat.Id, actChat.Id);
            Assert.Equal(expChat.Name, actChat.Name);
            Assert.Equal(expChat.CreateUserId, actChat.CreateUserId);
            Assert.Equal(expChat.DateCreate, actChat.Date);
            Assert.Equal(expChat.Users.Count, actChat.Members.Count);
            // проверка чата, на который пользователь не имеет прав
            var easyUser = GetEasyUser();
            var accessDeniedChat = DataHelper.Chats.First(_ =>
                _.CreateUserId != easyUser.Id && !_.Users.Any(_ => _.UserId == easyUser.Id));
            await GetFailedObjectTest($"{BaseUrl}{accessDeniedChat.Id}", new Credentials
            {
                Password = "User157",
                UserName = easyUser.Login,
            });
            // проверка чата, на который пользователь имеет права
            expChat = DataHelper.Chats.First(_ =>
                _.CreateUserId == easyUser.Id || _.Users.Any(_ => _.UserId == easyUser.Id));
            actChat = await GetSuccessObjectTest<ChatModel>($"{BaseUrl}{expChat.Id}", new Credentials
            {
                Password = "User157",
                UserName = easyUser.Login,
            });
            Assert.Equal(expChat.Id, actChat.Id);
            Assert.Equal(expChat.Name, actChat.Name);
            Assert.Equal(expChat.CreateUserId, actChat.CreateUserId);
            Assert.Equal(expChat.DateCreate, actChat.Date);
            Assert.Equal(expChat.Users.Count, actChat.Members.Count);
        }

        [Fact]
        public async Task GetPagedEasyUserChatsTest()
        {
            await GetAuthClient();
            string url = $"{BaseUrl}?pageSize=10&pageNumber=1";
            var easyUser = GetEasyUser();
            // тесты на сортировку по всем полям
            await TestSortingPagedList<ChatModel>(url, credentials: new Credentials
            {
                Password = "User157",
                UserName = easyUser.Login,
            });

            var easyUserChatsOtherMembers = easyUser.Chats.SelectMany(_ => _.Chat.Users.Where(u => u.UserId != easyUser.Id)).ToList();

            var result = await GetPagedListTest<ChatModel>($"{url}&ChatMembers={easyUserChatsOtherMembers.First().UserId}", credentials: new Credentials
            {
                Password = "User157",
                UserName = easyUser.Login,
            });

        }

        [Fact]
        public async Task GetPagedAdminChatsTest()
        {
            await GetAuthClient();
            string url = $"{BaseUrl}admin?pageSize=10&pageNumber=1";
            var easyUser = GetEasyUser();
            // проверка запрета доступа к методу обычному пользователю 
            await GetObjectTestWithStatusCode(url, HttpStatusCode.Forbidden, new Credentials
            {
                Password = "User157",
                UserName = easyUser.Login,
            });
            // тесты на сортировку по всем полям
            await TestSortingPagedList<ChatModel>(url);
        }

        [Fact]
        public async Task CreateChatTest()
        {
            await GetAuthClient();
            string url = $"{BaseUrl}";
            var model = new CreateChatModel
            {
                Name = "test_chat",
                Members = new List<long>
                {
                    DataHelper.Users[10].Id
                }
            };
            var actChat = await PostSuccessObjectTest<ChatModel>(url, model);
            Assert.Equal(model.Name, actChat.Name);
            Assert.Equal(2, actChat.Members.Count);
        }

        [Fact]
        public async Task UpdateChatTest()
        {
            await GetAuthClient();
            string url = $"{BaseUrl}";
            var model = new UpdateChatModel
            {
                ChatId = DataHelper.Chats[25].Id,
                Name = "test_chat_update",
                Members = new List<long>
                {
                    DataHelper.Users[10].Id
                }
            };
            var actChat = await PutSuccessObjectTest<ChatModel>(url, model);
            Assert.Equal(model.ChatId, actChat.Id);
            Assert.Equal(model.Name, actChat.Name);
            Assert.NotEmpty(actChat.Members);
        }


        [Fact]
        public async Task DeleteChatTest()
        {
            await GetAuthClient();
            var toDel = DataHelper.Chats.Last();
            string url = $"{BaseUrl}{toDel.Id}";
            await DeleteSuccessObjectTest(url);
            DataHelper.Chats.Remove(toDel);
        }

        #region [ Help methods ]

        private User GetEasyUser()
        {
            return DataHelper.Users.Last(_ => _.RoleId == GetEasyUserRoleId());
        }

        private long GetEasyUserRoleId()
        {
            var role = DataHelper.UserRoles.First(_ => _.Name == UserType.User);
            return role.Id;
        }

        #endregion
    }
}