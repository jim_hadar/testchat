using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestChat.Business.Models.Users;
using TestChat.IntegrationTests.Code;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    public class UserRoleControllerTest : BaseTestClass
    {
        protected override string BaseUrl => "api/v1/admin/userrole";

        public UserRoleControllerTest(DbInMemoryAppFactory appFactory) 
            : base(appFactory)
        {
        }

        [Fact]
        public async Task GetUserRolesTest()
        {
            var client = await GetAuthClient();
            var result = await GetSuccessObjectTest<List<UserRoleModel>>(BaseUrl);
            Assert.Contains(result, _ => _.Name.ToLower() == "администратор");
            Assert.Contains(result, _ => _.Name.ToLower() == "пользователь");
        }
        
    }
}