using System.Linq;
using System.Threading.Tasks;
using TestChat.Business.Models;
using TestChat.Business.Models.Users;
using TestChat.Data.Common;
using TestChat.Data.Models;
using TestChat.IntegrationTests.Code;
using TestChat.IntegrationTests.Helpers.Data;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    public class UserControllerTest : BaseTestClass
    {
        protected override string BaseUrl => "api/v1/user";

        public UserControllerTest(DbInMemoryAppFactory appFactory) : base(appFactory)
        {
        }

        [Fact]
        public async Task GetByIdTest()
        {
            await GetAuthClient();
            var user = GetEasyUser();
            var actualUser = await GetSuccessObjectTest<UserModel>($"{BaseUrl}/{user.Id}", new Credentials
            {
                Password = "User157",
                UserName = user.Login,
            });
            Assert.Equal(user.Id, actualUser.Id);
            Assert.Equal(user.Login, actualUser.Login);
            Assert.Equal(user.Name, actualUser.Name);
        }

        [Fact]
        public async Task PagedTest()
        {
            await GetAuthClient();
            string url = $"{BaseUrl}?pageSize=10&pageNumber=1";
            // тесты на сортировку по всем полям
            await TestSortingPagedList<UserModel>(url, new[]
            {
                nameof(UserModel.Password)
            });
        }

        [Fact]
        public async Task UpdateUserTest()
        {
            await GetAuthClient();
            var user = GetEasyUser();
            var expUser = await GetSuccessObjectTest<UserModel>($"{BaseUrl}/{user.Id}");
            expUser.Login = "testupdateeasyuser";
            var actualUser = await PutSuccessObjectTest<UserModel>(BaseUrl, expUser, new Credentials
            {
                UserName = user.Login,
                Password = "User157" 
            });
            Assert.Equal(expUser.Id, actualUser.Id);
            Assert.Equal(expUser.Login, actualUser.Login);
            Assert.Equal(expUser.Name, actualUser.Name);
            user.Login = "testupdateeasyuser";
        }

        private User GetEasyUser()
        {
            return DataHelper.Users.First(_ => _.RoleId == GetEasyUserRoleId());
        }

        private long GetEasyUserRoleId()
        {
            var role = DataHelper.UserRoles.First(_ => _.Name == UserType.User);
            return role.Id;
        }
    }
}