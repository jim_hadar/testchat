using System.Linq;
using System.Threading.Tasks;
using TestChat.Business.Models;
using TestChat.Business.Models.Messages;
using TestChat.Data.Common;
using TestChat.Data.Models;
using TestChat.IntegrationTests.Code;
using TestChat.IntegrationTests.Helpers.Data;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    /// <summary>
    /// Тесты на контроллер сообщщений
    /// </summary>
    public class MessageControllerTest : BaseTestClass
    {
        protected override string BaseUrl => "api/v1/message";

        public MessageControllerTest(DbInMemoryAppFactory appFactory) 
            : base(appFactory)
        {
        }

        [Fact]
        public async Task GetByIdMessageTest()
        {
            await GetAuthClient();
            var easyUser = GetEasyUser();
            var messageExp = easyUser.Messages.First();
            //проверка прав на получение сообщения администратором
            var messageAct = await GetSuccessObjectTest<MessageModel>($"{BaseUrl}/{messageExp.Id}");
            Assert.Equal(messageExp.UserId, messageAct.UserId);
            Assert.Equal(messageExp.Content, messageAct.Content);
            Assert.Equal(messageExp.DateCreate, messageAct.DateCreate);
            //Проверка успешности получения сообщения обычным пользователем, который отправил сообщение
            await GetSuccessObjectTest<MessageModel>($"{BaseUrl}/{messageExp.Id}", new Credentials
            {
                UserName = easyUser.Login,
                Password = "User157",
            });
            // Проверка ошибки доступа на сообщение пользователем, который не имеет доступа к нему
            var denyUser = DataHelper.Users.Last(_ =>
                _.RoleId == GetEasyUserRoleId() && _.Messages.All(m => m.UserId != easyUser.Id));
            await GetFailedObjectTest($"{BaseUrl}/{messageExp.Id}", new Credentials
            {
                UserName = denyUser.Login,
                Password = "User157",
            });
        }

        [Fact]
        public async Task GetPagedMessagesTest()
        {
            await GetAuthClient();
            var easyUser = GetEasyUser();
            string url = $"{BaseUrl}?pageSize=1&pageNumber=1&ChatId={easyUser.Chats.First().ChatId}";
            var result = await GetPagedListTest<MessageModel>(url);
            Assert.Single(result);
            result = await GetPagedListTest<MessageModel>(url, new Credentials
            {
                UserName = easyUser.Login,
                Password = "User157"
            });
            Assert.Single(result);
            var denyUser = DataHelper.Users.Last(_ =>
                _.RoleId == GetEasyUserRoleId() && _.Messages.All(m => m.UserId != easyUser.Id));
            await GetFailedObjectTest(url, new Credentials
            {
                UserName = denyUser.Login,
                Password = "User157"
            });
        }

        [Fact]
        public async Task CreateMessageTest()
        {
            await GetAuthClient();
            var easyUser = GetEasyUser();
            var chat = easyUser.Chats.First();
            var expMessage = new MessageCreateModel
            {
                ChatId = chat.ChatId,
                Content = "test create message",
            };
            // проверка создания сообщения для пользователя с соотвествующими правами
            var actMessage = await PostSuccessObjectTest<MessageModel>($"{BaseUrl}", expMessage, new Credentials
            {
                UserName = easyUser.Login,
                Password = "User157"
            });
            Assert.Equal(expMessage.Content, actMessage.Content);
            Assert.Equal(easyUser.Id, actMessage.UserId);
            Assert.False(actMessage.IsRead);
            //проверка запрета на создание сообщения
            var denyUser = DataHelper.Users.Last(_ =>
                _.RoleId == GetEasyUserRoleId() && _.Messages.All(m => m.UserId != easyUser.Id));
            await PostFailedObjectTest(BaseUrl, actMessage, new Credentials
            {
                UserName = denyUser.Login,
                Password = "User157"
            });
        }

        [Fact]
        public async Task DeleteMessageTest()
        {
            await GetAuthClient();
            var easyUser = GetEasyUser();
            //проверка запрета на удаление сообщения
            var denyUser = DataHelper.Users.Last(_ =>
                _.RoleId == GetEasyUserRoleId() && _.Messages.All(m => m.UserId != easyUser.Id));
            await DeleteFailedObjectTest($"{BaseUrl}/{easyUser.Messages.Last().Id}", new Credentials
            {
                UserName = denyUser.Login,
                Password = "User157"
            });
            //проверка корректного удаления сообщения
            await DeleteSuccessObjectTest($"{BaseUrl}/{easyUser.Messages.Last().Id}", new Credentials
            {
                UserName = easyUser.Login,
                Password = "User157"
            });
        }
        
        private User GetEasyUser()
        {
            return DataHelper.Users.Last(_ => _.RoleId == GetEasyUserRoleId() && _.Messages.Any());
        }

        private long GetEasyUserRoleId()
        {
            var role = DataHelper.UserRoles.First(_ => _.Name == UserType.User);
            return role.Id;
        }
    }
}