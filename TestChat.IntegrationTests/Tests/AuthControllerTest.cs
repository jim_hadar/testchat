using System.Net;
using System.Threading.Tasks;
using TestChat.Business.Models;
using TestChat.Data.Common;
using TestChat.IntegrationTests.Code;
using TestChat.IntegrationTests.Extensions;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    public class AuthControllerTest : BaseTestClass
    {
        protected override string BaseUrl => "api/v1/auth";
        public AuthControllerTest(DbInMemoryAppFactory appFactory) 
            : base(appFactory)
        {
            
        }

        [Fact]
        public async Task LoginSuccessTest()
        {
            var credentials = new Credentials
            {
                UserName = UserType.Administrator,
                Password = "Qwerty7",
            };

            var client = GetClient();
            var response = await client.PostAsync($"{BaseUrl}/login", credentials.AsJson());
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task LoginFailedTest()
        {
            var credentials = new Credentials
            {
                UserName = UserType.Administrator,
                Password = "Qwerty8",
            };

            var client = GetClient();
            var response = await client.PostAsync($"{BaseUrl}/login", credentials.AsJson());
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

    }
}