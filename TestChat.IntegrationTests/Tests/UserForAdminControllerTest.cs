using System.Linq;
using System.Threading.Tasks;
using TestChat.Business.Models;
using TestChat.Business.Models.Users;
using TestChat.Data.Common;
using TestChat.Data.Models;
using TestChat.IntegrationTests.Code;
using TestChat.IntegrationTests.Helpers.Data;
using Xunit;

namespace TestChat.IntegrationTests.Tests
{
    public class UserForAdminControllerTest : BaseTestClass
    {
        protected override string BaseUrl => "api/v1/admin/user";

        public UserForAdminControllerTest(DbInMemoryAppFactory appFactory) : base(appFactory)
        {
            
        }

        [Fact]
        public async Task GetByIdSuccessTest()
        {
            await GetAuthClient();
            var expUser = DataHelper.Users[30];
            var result = await GetById(expUser.Id);
            Assert.Equal(expUser.Id, result.Id);
            Assert.Equal(expUser.Name, result.Name);
            Assert.Equal(expUser.Login, result.Login);
            Assert.Equal(expUser.RoleId, result.RoleId);
        }
        
        [Fact]
        public async Task CreateUserSuccessTest()
        {
            await GetAuthClient();
            int countUsers = DataHelper.Users.Count;
            string userName = "TestCreatedUser";
            var expUser = new UserForAdminModel
            {
                Name = userName,
                Login = userName.ToLower(),
                Password = $"User{countUsers}",
                RoleId = countUsers % 2 == 0 
                    ? DataHelper.Users[0].RoleId
                    : DataHelper.Users[1].RoleId
            };

            var actualUser = await PostSuccessObjectTest<UserForAdminModel>(BaseUrl, expUser);
            Assert.Equal(expUser.Name, actualUser.Name);
            Assert.Equal(expUser.Login, actualUser.Login);
            Assert.Equal(expUser.RoleId, actualUser.RoleId);
            DataHelper.Users.Add(new User
            {
                Id = actualUser.Id,
                Name = actualUser.Name,
                Login = actualUser.Login,
                RoleId = actualUser.RoleId
            });
            //пытаемся залогиниться под логином
            await GetAuthClient(new Credentials
            {
                UserName = actualUser.Login,
                Password = expUser.Password
            });
        }

        [Fact]
        public async Task CreateUserFailedTest()
        {
            await GetAuthClient();
            int countUsers = DataHelper.Users.Count;
            string userName = "TestCreatedUser";
            var expUser = new UserForAdminModel
            {
                Name = userName,
                Login = userName.ToLower(),
                Password = $"User{countUsers}",
                // задаем неверный id роли
                RoleId = 0
            };
            await PostFailedObjectTest(BaseUrl, expUser);
            expUser.RoleId = DataHelper.Users[1].RoleId;
            expUser.Login = DataHelper.Users[2].Login;
            await PostFailedObjectTest(BaseUrl, expUser);
            expUser.Login = "dasdadadsdasdasdas";
            expUser.Password = "";
            await PostFailedObjectTest(BaseUrl, expUser);
        }

        [Fact]
        public async Task UpdateUserSuccessTest()
        {
            await GetAuthClient();
            var user = await GetById(DataHelper.Users[30].Id);

            user.Login = "testupdateuser";
            user.Name = "dasdasda";
            user.Password = "Qetuo146";

            var actualUser = await PutSuccessObjectTest<UserForAdminModel>(BaseUrl, user);
            
            Assert.Equal(user.Login, actualUser.Login);
            Assert.Equal(user.Id, actualUser.Id);
            Assert.Equal(user.Name, actualUser.Name);
            // пытаемся залогиниться под обновленным пользователем
            await GetAuthClient(new Credentials
            {
                UserName = actualUser.Login,
                Password = user.Password
            });
        }

        [Fact]
        public async Task DeleteSuccessUserTest()
        {
            await GetAuthClient();
            var user = await GetById(DataHelper.Users[40].Id);

            await DeleteSuccessObjectTest($"{BaseUrl}/{user.Id}");
            DataHelper.Users.RemoveAt(40);
        }

        [Fact]
        public async Task DeleteFailedUserTestWithEasyUserAuth()
        {
            await GetAuthClient();
            var userRoleId = DataHelper.UserRoles.First(_ => _.Name == UserType.User).Id;
            var easyUser = DataHelper.Users.First(_ => _.RoleId == userRoleId);
            var userForDel = DataHelper.Users[20];
            await DeleteForbiddenTest($"{BaseUrl}/{userForDel.Id}", new Credentials
            {
                UserName = easyUser.Login,
                Password = "User157",
            });
        }

        #region [ Help methods ]

        private async Task<UserForAdminModel> GetById(long id)
        {
            await GetAuthClient();
            var result = await GetSuccessObjectTest<UserForAdminModel>($"{BaseUrl}/{id}");
            return result;
        }

        #endregion
    }
}