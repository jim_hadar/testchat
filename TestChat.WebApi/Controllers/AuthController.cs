﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestChat.Business.Models;
using TestChat.Business.Services.Abstract;

namespace TestChat.WebApi.Controllers
{
    /// <summary>
    /// Контроллер авторизации
    /// </summary>
    /// <seealso cref="TestChat.WebApi.Controllers.BaseController" />
    [AllowAnonymous]
    [Route("api/v1/auth")]
    public class AuthController : BaseController
    {
        private readonly IAuthService _authService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mediator"></param>
        /// <param name="authService"></param>
        public AuthController(IMediator mediator, IAuthService authService)
            :base(mediator)
        {
            _authService = authService;
        }

        /// <summary>
        /// Авторизация пользователя
        /// </summary>
        /// <param name="credentials">Данные для аутентификации пользователя</param>
        /// <returns>Результат аутентификации</returns>
        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] Credentials credentials)
        {
            return Ok(await _authService.Login(credentials).ConfigureAwait(false));
        }

    }
}