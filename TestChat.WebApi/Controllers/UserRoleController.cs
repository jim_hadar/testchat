using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestChat.Business.Models.Users;
using TestChat.Business.Queries.Users;
using TestChat.Data.Common;

namespace TestChat.WebApi.Controllers
{
    /// <summary>
    /// Контроллер работы с ролями пользователя
    /// </summary>
    [Authorize(Roles = UserType.Administrator)]
    [Route("api/v1/admin/[controller]")]
    public class UserRoleController : BaseController
    {
        public UserRoleController(IMediator mediator) 
            : base(mediator)
        {
            
        }

        /// <summary>
        /// Получение списка ролей по системе
        /// </summary>
        /// <param name="token">the token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<UserRoleModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllRoles(CancellationToken token)
            => Ok(await _mediator.Send(GetRoles.Query.Instance, token).ConfigureAwait(false));
    }
}