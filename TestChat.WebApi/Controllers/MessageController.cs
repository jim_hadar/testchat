using System;
using System.Threading;
using System.Threading.Tasks;
using Framework.DAL.Paging;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using TestChat.Business.Commands.Messages;
using TestChat.Business.Models.Messages;
using TestChat.Business.Queries.Messages;

namespace TestChat.WebApi.Controllers
{
    /// <summary>
    /// Контроллер для работы с сообщениями
    /// </summary>
    [Route("api/v1/[controller]")]
    [Authorize]
    public class MessageController : BaseController
    {
        
        public MessageController(IMediator mediator) : base(mediator)
        {
            
        }

        /// <summary>
        /// Создание сообщения для текущего пользователя
        /// </summary>
        /// <param name="model">Сообщение для создания</param>
        /// <param name="token">The token</param>
        /// <returns>Созданное сообщение</returns>
        [HttpPost]
        [ProducesResponseType(typeof(MessageModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateMessage([FromBody] MessageCreateModel model, CancellationToken token)
            => Ok(await _mediator.Send(new CreateMessageCommand.Command(model), token).ConfigureAwait(false));

        /// <summary>
        /// Удаление сообщения
        /// </summary>
        /// <param name="id">ИД сообщения</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [HttpDelete("{id:long}")]
        public async Task<IActionResult> DeleteMessage(long id, CancellationToken token)
        {
            await _mediator.Send(new DeleteMessageCommand.Command(id), token).ConfigureAwait(false);
            return NoContent();
        }

        /// <summary>
        /// Получение сообщения по идентификатору
        /// </summary>
        /// <param name="id">ИД сообщения</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(MessageModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetMessageById(long id, CancellationToken token)
            => Ok(await _mediator.Send(new GetMessageByIdQuery.Query(id), token).ConfigureAwait(false));

        /// <summary>
        /// Получение сообщения чата с постраничной разбивкой
        /// </summary>
        /// <param name="filter">Фильтр</param>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagedList), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPagedMessages([FromQuery] GetPagedUserMessagesQuery.Query filter,
            CancellationToken token)
            => Ok(await _mediator.Send(filter, token).ConfigureAwait(false));

    }
}