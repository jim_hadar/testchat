using Framework.Constants;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TestChat.WebApi.Code;

namespace TestChat.WebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]    
    public class BaseController : ControllerBase
    {
        protected ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);

        /// <summary>
        /// IMediator
        /// </summary>
        protected readonly IMediator _mediator;

        public BaseController(IMediator mediator)
        {
            _mediator = mediator;
        }
    }
}