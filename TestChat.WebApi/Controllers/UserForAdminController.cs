using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestChat.Business.Commands.Users;
using TestChat.Business.Models.Users;
using TestChat.Business.Queries.Users;
using TestChat.Data.Common;

namespace TestChat.WebApi.Controllers
{
    /// <summary>
    /// Контроллер для управления пользователями администратором
    /// </summary>
    [Route("api/v1/admin/user")]
    [Authorize(Roles = UserType.Administrator)]
    [ApiController]
    public class UserForAdminController : BaseController
    {
        public UserForAdminController(IMediator mediator)
            : base(mediator)
        {
        }

        /// <summary>
        /// Получение пользователя по его идентификатору
        /// </summary>
        /// <param name="id">ИД пользователя</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(UserForAdminModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserByIdForAdministration(long id, CancellationToken token)
            => Ok(await _mediator.Send(new GetUserByIdForUseAdminQuery.Query(id), token).ConfigureAwait(false));

        /// <summary>
        /// Создание пользователя
        /// </summary>
        /// <param name="model">Модель пользователя для создания</param>
        /// <param name="token">The token</param>
        /// <returns>Созданный пользователь</returns>
        [HttpPost]
        [ProducesResponseType(typeof(UserForAdminModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateUser([FromBody] UserForAdminModel model, CancellationToken token)
            => Ok(await _mediator.Send(new CreateUserCommand.Command(model), token).ConfigureAwait(false));

        /// <summary>
        /// Обновление пользователя
        /// </summary>
        /// <param name="model">Модель пользователя для обновления</param>
        /// <param name="token">The token</param>
        /// <returns>Обновленный пользователь</returns>
        [HttpPut]
        [ProducesResponseType(typeof(UserForAdminModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateUser([FromBody] UserForAdminModel model, CancellationToken token)
            => Ok(await _mediator.Send(new UpdateUserForAdminCommand.Command(model), token).ConfigureAwait(false));

        /// <summary>
        /// Удаление пользователя по его идентификатору
        /// </summary>
        /// <param name="id">ИД пользователя для удаления</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id:long}")]
        public async Task<IActionResult> DeleteUser(long id, CancellationToken token)
        {
            await _mediator.Send(new DeleteUserCommand.Command(id), token).ConfigureAwait(false);
            return NoContent();
        }
    }
}