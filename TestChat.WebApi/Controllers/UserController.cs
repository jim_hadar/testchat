using System.Threading;
using System.Threading.Tasks;
using Framework.DAL.Paging.EF;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestChat.Business.Commands.Users;
using TestChat.Business.Models.Users;
using TestChat.Business.Queries.Users;

namespace TestChat.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize]
    public class UserController : BaseController
    {
        public UserController(IMediator mediator)
            : base(mediator)
        {
        }

        /// <summary>
        /// Получение пользователя по его идентификатору
        /// </summary>
        /// <param name="id">ИД пользователя</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserById(long id, CancellationToken token)
        {
            return Ok(await _mediator.Send(new GetUserByIdQuery.Query(id), token).ConfigureAwait(false));
        }

        /// <summary>
        /// Получение пользователей с постраничной разбивкой
        /// </summary>
        /// <param name="filter">Параметры фильтрации</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagedEfList<BaseEFFilter, UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPagedUsers([FromQuery] GetPagedUsers.Query filter, CancellationToken token)
            => Ok(await _mediator.Send(filter, token).ConfigureAwait(false));

        /// <summary>
        /// Обновление пользователя
        /// </summary>
        /// <param name="model">Модель пользователя для обновления</param>
        /// <param name="token">The token</param>
        /// <returns>Обновленный пользователь</returns>
        [HttpPut]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateUser([FromBody] UserModel model, CancellationToken token)
            => Ok(await _mediator.Send(new UpdateUserCommand.Command(model), token).ConfigureAwait(false));
    }
}