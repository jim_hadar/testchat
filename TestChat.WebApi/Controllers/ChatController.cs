using System.Threading;
using System.Threading.Tasks;
using Framework.DAL.Paging;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestChat.Business.Commands.Chats;
using TestChat.Business.Models.Chats;
using TestChat.Business.Queries.Chats;
using TestChat.Data.Common;

namespace TestChat.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize]
    public class ChatController : BaseController
    {
        public ChatController(IMediator mediator) 
            : base(mediator)
        {
        }

        /// <summary>
        /// Получение чата по его идентификатора
        /// </summary>
        /// <param name="id">Идентификатор чата</param>
        /// <param name="token">the token</param>
        /// <returns>Чат</returns>
        [HttpGet("{id:long}")]
        [ProducesResponseType(typeof(ChatModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetChatAsync(long id, CancellationToken token)
        {
            return Ok(await _mediator.Send(new GetChatByIdQuery.Query(id), token).ConfigureAwait(false));
        }

        /// <summary>
        /// Получение списка чатов с разбивкой для текущего пользователя
        /// </summary>
        /// <param name="filter">Параметры фильтра</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagedList), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPagedChatAsync([FromQuery] GetCurUserChats.Query filter,
            CancellationToken token)
        {
            return Ok(await _mediator.Send(filter, token).ConfigureAwait(false));
        }

        /// <summary>
        /// Получение списка чатов в соответствии с условиями фильтрации (доступно только администраторам) 
        /// </summary>
        /// <param name="filter">Параметры фильтрации</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [Authorize(Roles = UserType.Administrator)]
        [HttpGet("admin")]
        [ProducesResponseType(typeof(PagedList), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPagedChatAsyncForAdmin([FromQuery] GetPagedChats.Query filter,
            CancellationToken token)
        {
            return Ok(await _mediator.Send(filter, token).ConfigureAwait(false));
        }
        
        /// <summary>
        /// Создание чата
        /// </summary>
        /// <param name="model">Модель чата для создания</param>
        /// <param name="token">The token</param>
        /// <returns>Созданный чат</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ChatModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateChat([FromBody] CreateChatModel model, CancellationToken token)
        {
            return Ok(await _mediator.Send(new CreateChatCommand.Command(model), token).ConfigureAwait(false));
        }

        /// <summary>
        /// Обновление чата
        /// </summary>
        /// <param name="model">Модель чата для обновления</param>
        /// <param name="token">The token</param>
        /// <returns>Обновленный чат</returns>
        [HttpPut]
        [ProducesResponseType(typeof(ChatModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateChat([FromBody] UpdateChatModel model, CancellationToken token)
        {
            return Ok(await _mediator.Send(new UpdateChatCommand.Command(model), token).ConfigureAwait(false));
        }

        /// <summary>
        /// Удаление чата
        /// </summary>
        /// <param name="id">Идентификатор чата для удаления</param>
        /// <param name="token">The token</param>
        /// <returns></returns>
        [HttpDelete("{id:long}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteChat(long id, CancellationToken token)
        {
            await _mediator.Send(new DeleteChatCommand.Command(id), token).ConfigureAwait(false);
            return NoContent();
        }
    }
}