using Framework.Constants;
using Framework.Exceptions;
using Framework.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

#nullable disable
namespace TestChat.WebApi.Code
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                LoggerService.GetLogger(CommonConstants.AppLoggerName).LogError(context.Exception,
                    $"Ошибка приложения: {context.HttpContext.User.Identity.Name}: {context.Exception.Message}");
            }
            else
            {
                LoggerService.GetLogger(CommonConstants.AppLoggerName).LogError(context.Exception,
                    $"Ошибка приложения: {context.Exception.Message}");
            }

            switch (context.Exception)
            {
                case FrameworkException frameworkException:
                    context.Result = new BadRequestObjectResult(frameworkException.Message);
                    break;
                default:
                    context.Result = new BadRequestObjectResult("Произошла неизвестная ошибка.");
                    break;
            }
        }
    }
}