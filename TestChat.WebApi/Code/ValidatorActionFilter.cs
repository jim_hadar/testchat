using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TestChat.WebApi.Code
{
    public class ValidatorActionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null || filterContext.ModelState.IsValid) return;
            var errors = filterContext.ModelState.Values.SelectMany(_ => _.Errors.Select(e => e.ErrorMessage)).ToList();
            filterContext.Result = new BadRequestObjectResult(string.Join(',', errors));
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
    }
}