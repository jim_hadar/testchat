using System.Linq;
using Framework;
using Microsoft.Extensions.DependencyInjection;
using TestChat.Business.Services.Abstract;
using TestChat.Business.Services.Concrete;
using TestChat.Business.SignalrHubs;
using TestChat.Data.Extensions;

namespace TestChat.Business.Extensions
{
    public static class BusinessServicesInitializer
    {
        /// <summary>
        /// Инициализация провайдеров
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        public static void InitializeContracts(this IServiceCollection services)
        {
            services.InitializeDataServices();
            services.AddScoped<IAuthService, AuthService>();
            services.AddSingleton<PasswordCryptographer>();
            services.AddSingleton<IConnectionManager, ConnectionManager>();
            services.AddSingleton<IChatMessageHelper, ChatMessageHelper>();
        }
        
    }
}