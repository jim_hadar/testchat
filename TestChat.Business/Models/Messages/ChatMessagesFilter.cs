using System.Linq;
using System.Linq.Dynamic.Core;
using Framework.DAL.Paging.EF;
using Framework.Exceptions;
using TestChat.Data.Models;

namespace TestChat.Business.Models.Messages
{
    /// <summary>
    /// Фильтр для чата
    /// </summary>
    public class ChatMessagesFilter : BaseEFFilter
    {
        /// <summary>
        /// Идентификатор чата, для которого необходимо найти сообщения
        /// </summary>
        public long ChatId { get; set; }

        public override IQueryable Filtrate(IQueryable data)
        {
            // полностью игнорируем сортировку по другим столбцам
            Sort = nameof(ChatMessage.DateCreate);
            Order = DescendingSort;
            var messages = data as IQueryable<ChatMessage>;

            if (messages == null)
            {
                throw new FrameworkException("Не удалось получить список сообщений");
            }
            
            // фильтруем по ИД чата
            messages = messages.Where(_ => !_.IsDeleted && _.ChatId == ChatId);

            return DefaultSorting(messages);
        }
    }
}