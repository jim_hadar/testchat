using System;
using TestChat.Business.Models.Users;

namespace TestChat.Business.Models.Messages
{
    /// <summary>
    /// Модель сообщения
    /// </summary>
    public class MessageModel
    {
        public long Id { get; set; }
        /// <summary>
        /// ИД пользователя, отправившего сообщение
        /// </summary>
        public long UserId { get; set; }
        
        public long ChatId { get; set; }
        
        /// <summary>
        /// User of message
        /// </summary>
        public UserModel User { get; set; }
        
        /// <summary>
        /// Дата создания сообщения 
        /// </summary>
        public DateTime DateCreate { get; set; }
        
        /// <summary>
        /// Тело сообщения
        /// </summary>
        public string Content { get; set; }
        
        /// <summary>
        /// Было ли прочитано сообщение
        /// </summary>
        public bool IsRead { get; set; }
    }
}