namespace TestChat.Business.Models.Messages
{
    public class MessageCreateModel
    {
        public long ChatId { get; set; }
        public string Content { get; set; }
    }
}