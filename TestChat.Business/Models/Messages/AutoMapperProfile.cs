using AutoMapper;
using TestChat.Data.Models;

namespace TestChat.Business.Models.Messages
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ChatMessage, MessageModel>()
                .ForMember(_ => _.User, opt => opt.Ignore())
                .ForAllOtherMembers(_ => _.UseDestinationValue());
        }
    }
}