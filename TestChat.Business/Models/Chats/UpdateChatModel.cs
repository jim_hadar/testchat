namespace TestChat.Business.Models.Chats
{
    /// <summary>
    /// Модель для обновления информации о чате
    /// </summary>
    public class UpdateChatModel : CreateChatModel
    {
        /// <summary>
        /// Идентификатор чата
        /// </summary>
        public long ChatId { get; set; }
    }
}