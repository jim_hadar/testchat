using System.Collections.Generic;

namespace TestChat.Business.Models.Chats
{
    /// <summary>
    /// Модель для создания чата
    /// </summary>
    public class CreateChatModel
    {
        /// <summary>
        /// Название чата
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Участники чата
        /// </summary>
        public List<long> Members { get; set; }
    }
}