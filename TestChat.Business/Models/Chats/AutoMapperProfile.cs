using System.Linq;
using AutoMapper;
using TestChat.Data.Models;

namespace TestChat.Business.Models.Chats
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Chat, ChatModel>()
                .ForMember(_ => _.Date, opt => opt.MapFrom(_ => _.DateCreate))
                .ForMember(_ => _.CreateUser, opt => opt.MapFrom(_ => _.CreateUser))
                .ForMember(_ => _.Members, opt => opt.MapFrom(_ => _.Users.Select(u => u.User)))
                .ForAllOtherMembers(_ => _.UseDestinationValue());
        }
    }
}