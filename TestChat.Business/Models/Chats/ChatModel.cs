using System;
using System.Collections.Generic;
using TestChat.Business.Models.Users;

namespace TestChat.Business.Models.Chats
{
    /// <summary>
    /// Модель получения информации о чате
    /// </summary>
    public sealed class ChatModel
    {
        /// <summary>
        /// Идентификатор чата
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Название чата 
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Идентификатор пользователя, создавшего чат
        /// </summary>
        public long CreateUserId { get; set; }
        
        /// <summary>
        /// Дата создания чата
        /// </summary>
        public DateTime Date { get; set; }
        
        /// <summary>
        /// Информация о пользователе, создавшем чат
        /// </summary>
        public UserModel CreateUser { get; set; }
        
        /// <summary>
        /// Кто состоит в чате
        /// </summary>
        public List<UserModel> Members { get; set; }
    }
}