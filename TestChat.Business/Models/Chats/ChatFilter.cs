using System.Collections.Generic;
using System.Linq;
using Framework.DAL.Paging.EF;
using Microsoft.EntityFrameworkCore.Internal;
using TestChat.Data.Models;

namespace TestChat.Business.Models.Chats
{
    /// <summary>
    /// Фильтр для чатов
    /// </summary>
    public sealed class ChatFilter : BaseEFFilter
    {
        private string _sort;
        public override string Sort
        {
            get => _sort ??= DefaultSortField;
            set
            {
                if (value.ToLower() == nameof(ChatModel.Date).ToLower())
                {
                    _sort = nameof(Chat.DateCreate);
                }
                else if (value.ToLower() == nameof(ChatModel.Members).ToLower())
                {
                    _sort = $"{nameof(Chat.Users)}.Count";
                }
                else if (value.ToLower() == nameof(ChatModel.CreateUser).ToLower())
                {
                    _sort = nameof(Chat.CreateUserId);
                }
                else
                {
                    _sort = value;
                }
            }
        }
        protected override string DefaultSortField => nameof(Chat.DateCreate);
        
        /// <summary>
        /// Кем создан чат 
        /// </summary>
        public long? CreatedBy { get; set; }
        
        /// <summary>
        /// Участники чата
        /// </summary>
        public List<long> ChatMembers { get; set; }

        
        public override IQueryable Filtrate(IQueryable data)
        {
            var chats = (IQueryable<Chat>) data;
            chats = chats.Where(_ => !_.IsDeleted);
            var tmp = chats.ToList();
            var users = tmp.First().Users.ToList();
            chats = FiltrateCreatedBy(chats);
            tmp = chats.ToList();
            chats = FiltrateChatMembers(chats);
            tmp = chats.ToList();
            chats = DefaultSorting(chats);
            return chats;
        }

        private IQueryable<Chat> FiltrateCreatedBy(IQueryable<Chat> chats)
            => CreatedBy.HasValue
                ? chats.Where(_ => _.CreateUserId == CreatedBy.Value || _.Users.Any(u => u.UserId == CreatedBy.Value))
                : chats;

        private IQueryable<Chat> FiltrateChatMembers(IQueryable<Chat> chats)
        {
            if (ChatMembers == null || !ChatMembers.Any())
                return chats;
            return chats.Where(_ => _.Users.Any(l => ChatMembers.Contains(l.UserId)));
        }
    }
}