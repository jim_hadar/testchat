namespace TestChat.Business.Models.Users
{
    /// <summary>
    /// Модель роли пользователя
    /// </summary>
    public sealed class UserRoleModel
    {
        /// <summary>
        /// Идентификатор роли
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Название роли
        /// </summary>
        public string Name { get; set; }
    }
}