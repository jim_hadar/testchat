using AutoMapper;
using TestChat.Data.Models;

namespace TestChat.Business.Models.Users
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserForAdminModel>()
                .ForMember(e => e.RoleName, o => o.MapFrom(_ => _.Role.DisplayName))
                .ForMember(_ => _.Password, opt => opt.Ignore())
                .ForAllOtherMembers(_ => _.UseDestinationValue());
            
            CreateMap<UserForAdminModel, User>()
                .ForMember(_ => _.Chats, opt => opt.Ignore())
                .ForMember(_ => _.Messages, opt => opt.Ignore())
                .ForMember(_ => _.Role, opt => opt.Ignore())
                .ForMember(_ => _.IsDeleted, opt => opt.Ignore())
                .ForMember(_ => _.Password, opt => opt.Ignore())
                .ForAllOtherMembers(_ => _.UseDestinationValue());

            CreateMap<User, UserModel>()
                .ForMember(_ => _.Password, opt => opt.Ignore());
            
            CreateMap<UserModel, User>()
                .ForMember(_ => _.Chats, opt => opt.Ignore())
                .ForMember(_ => _.Messages, opt => opt.Ignore())
                .ForMember(_ => _.Role, opt => opt.Ignore())
                .ForMember(_ => _.IsDeleted, opt => opt.Ignore())
                .ForMember(_ => _.Password, opt => opt.Ignore())
                .ForMember(_ => _.RoleId, opt => opt.Ignore())
                .ForAllOtherMembers(_ => _.UseDestinationValue());
            
            CreateMap<UserRole, UserRoleModel>()
                .ForMember(e => e.Name, o => o.MapFrom(_ => _.DisplayName))
                .ForAllOtherMembers(_ => _.UseDestinationValue());
        }
    }
}