namespace TestChat.Business.Models.Users
{
    /// <summary>
    /// Модель пользователя для административной части.
    /// </summary>
    public class UserForAdminModel : UserModel
    {
        /// <summary>
        /// Идентификатор роли пользователя
        /// </summary>
        public long RoleId { get; set; }
        
        /// <summary>
        /// Название роли пользователя
        /// </summary>
        public string RoleName { get; set; }    
    }
}