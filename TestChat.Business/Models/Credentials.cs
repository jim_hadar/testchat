namespace TestChat.Business.Models
{
    /// <summary>
    /// Данные авторизации
    /// </summary>
    public class Credentials
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string UserName { get; set; }
        
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
    }
}
