using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using MediatR;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Users
{
    /// <summary>
    /// Обертка запроса получения пользователя по его идентификатору для административной части
    /// </summary>
    public static class GetUserByIdForUseAdminQuery
    {
        /// <summary>
        /// Запрос получения пользователя по его идентификатору
        /// </summary>
        public sealed class Query : IRequest<UserForAdminModel>
        {
            public Query(long id)
            {
                Id = id;
            }
            
            /// <summary>
            /// Идентификатор пользователя
            /// </summary>
            public long Id { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, UserForAdminModel>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            
            public Handler(IMapper mapper, IUnitOfWork unitOfWork)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
            }

            public async Task<UserForAdminModel> Handle(Query request, CancellationToken cancellationToken)
            {
                try
                {
                    var info = await UserRepo.GetAsync(request.Id).ConfigureAwait(false);
                    return _mapper.Map<UserForAdminModel>(info);
                }
                catch (EntityNotFoundException ex)
                {
                    throw new EntityNotFoundException($"Пользователь не найден", ex);
                }
            }
        }
    }
}