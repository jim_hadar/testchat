using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Framework.DAL.EF.Abstract;
using Framework.DAL.Paging;
using Framework.DAL.Paging.EF;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Users
{
    /// <summary>
    /// Обертка запроса получения пользователей с постраничной разбивкой
    /// </summary>
    public static class GetPagedUsers
    {
        /// <summary>
        /// Запрос получения списка пользователей с постраничной разбивкой
        /// </summary>
        public sealed class Query : PagingParams<BaseEFFilter>, IRequest<PagedEfList<BaseEFFilter, User>>
        {
            
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, PagedEfList<BaseEFFilter, User>>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
            }

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<PagedEfList<BaseEFFilter, User>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await UserRepo.GetPagedList(request, async users => 
                        (IList) (await users.ProjectTo<UserModel>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken)))
                    .ConfigureAwait(false);
            }
        }
    }
}