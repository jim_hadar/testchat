using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.DAL.EF.Abstract;
using Framework.DAL.Paging.EF;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Users
{
    /// <summary>
    /// Обертка запроса получения списка ролей системы 
    /// </summary>
    public static class GetRoles
    {
        /// <summary>
        /// Запрос получения списка ролей системы
        /// </summary>
        public sealed class Query : IRequest<IEnumerable<UserRoleModel>>
        {
            public static Query Instance => new Query();
        }

        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, IEnumerable<UserRoleModel>>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _ufw;
        
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork)
            {
                _mapper = mapper;
                _ufw = unitOfWork;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<IEnumerable<UserRoleModel>> Handle(Query request, CancellationToken cancellationToken)
            {
                var repo = _ufw.Repository<UserRole>();
                var roles = await repo.Query.ToListAsync(cancellationToken).ConfigureAwait(false);
                return _mapper.Map<IEnumerable<UserRoleModel>>(roles);
            }
        }
    }
}