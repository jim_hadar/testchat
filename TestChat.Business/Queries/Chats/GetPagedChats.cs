using System.Collections;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Framework.DAL.EF.Abstract;
using Framework.DAL.Paging;
using Framework.DAL.Paging.EF;
using Framework.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using TestChat.Business.Models.Chats;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Chats
{
    /// <summary>
    /// Команда получения списка всех чатов с постраничной разбивкой и фильтрами
    /// </summary>
    public static class GetPagedChats
    {
        /// <summary>
        /// запрос получения списка чатов с постраничной разбивкой.
        /// </summary>
        public sealed class Query : PagingParams<ChatFilter>, IRequest<PagedEfList<ChatFilter, Chat>>
        {
            
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, PagedEfList<ChatFilter, Chat>>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IHttpContextAccessor _accessor;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();
            
            private string User => _accessor.HttpContext.User.Identity.Name;
            
            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == User.ToLower()&& !_.IsDeleted).First();

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor accessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _accessor = accessor;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<PagedEfList<ChatFilter, Chat>> Handle(Query request, CancellationToken cancellationToken)
            {
                if (!CurUser.IsAdmin)
                {
                    throw new FrameworkException($"Нет прав на просмотр чатов");
                }
                return await ChatRepo.GetPagedList(request, async users => 
                        (IList) (await users.ProjectTo<ChatModel>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken)))
                    .ConfigureAwait(false);
            }
        }
    }
}