using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Chats;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Chats
{
    /// <summary>
    /// Обертка запроса получения информации о чате по его идентификатору
    /// </summary>
    public static class GetChatByIdQuery
    {
        /// <summary>
        /// Команда получения информации о чате по его идентификатору
        /// </summary>
        public sealed class Query : IRequest<ChatModel>
        {
            public Query(long id)
            {
                Id = id;
            }
            
            /// <summary>
            /// Идентификатор чата
            /// </summary>
            public long Id { get;  }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, ChatModel>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();

            private readonly IHttpContextAccessor _context;

            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == _context.HttpContext.User.Identity.Name.ToLower() && !_.IsDeleted).First();
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _context = contextAccessor;
            }

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<ChatModel> Handle(Query request, CancellationToken cancellationToken)
            {
                try
                {
                    var info = await ChatRepo.GetAsync(request.Id);
                    long userId = CurUser.Id;
                    if (!(CurUser.IsAdmin || info.CreateUserId == userId ||
                          info.Users.Select(_ => _.UserId).Contains(userId)))
                    {
                        throw new FrameworkException($"Нет прав на просмотр чата");
                    }
                    return _mapper.Map<ChatModel>(info);
                }
                catch (EntityNotFoundException ex)
                {
                    throw new EntityNotFoundException($"Чат не найден", ex);
                }
            }
            
        }
    }
}