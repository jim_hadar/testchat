using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using NLog;
using TestChat.Business.Models.Messages;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Messages
{
    /// <summary>
    /// Обертка запроса получения сообщения по его идентификатору
    /// </summary>
    public static class GetMessageByIdQuery
    {
        /// <summary>
        /// Запрос получения списка сообщения чата с постраничной разбивкой.
        /// </summary>
        public sealed class Query : IRequest<MessageModel>
        {
            public Query(long id)
            {
                Id = id;
            }

            /// <summary>
            /// ИД сообщения
            /// </summary>
            public long Id { get; }
        }

        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, MessageModel>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IHttpContextAccessor _accessor;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();

            private IBaseRepository<ChatMessage> MessageRepo => _unitOfWork.Repository<ChatMessage>();

            private string User => _accessor.HttpContext.User.Identity.Name;

            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == User.ToLower()&& !_.IsDeleted).First();

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor accessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _accessor = accessor;
            }

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<MessageModel> Handle(Query request, CancellationToken cancellationToken)
            {
                try
                {
                    if (!MessageRepo.Query.Any(_ => !_.IsDeleted && _.Id == request.Id))
                    {
                        throw new FrameworkException($"Сообщения не найдено");
                    }

                    var message = await MessageRepo.GetAsync(request.Id).ConfigureAwait(false);
                    if(!(CurUser.IsAdmin || message.UserId == CurUser.Id || message.Chat.Users.Any(_ => _.UserId == CurUser.Id)))
                    {
                        throw new FrameworkException($"Нет прав на просмотр сообщения");
                    }

                    message.IsRead = true;
                    await MessageRepo.UpdateAsync(message).ConfigureAwait(false);

                    var messageDto = _mapper.Map<MessageModel>(message);
                    messageDto.User = _mapper.Map<UserModel>(message.User);
                    return messageDto;
                }
                catch (Exception ex)
                {
                    throw new FrameworkException($"Пользователь {User}: произошла ошибка при получении сообщения", ex);
                }
            }
        }
    }
}