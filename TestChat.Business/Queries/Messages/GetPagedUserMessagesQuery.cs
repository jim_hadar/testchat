using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Framework.DAL.EF.Abstract;
using Framework.DAL.Paging;
using Framework.DAL.Paging.EF;
using Framework.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using TestChat.Business.Models.Messages;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Queries.Messages
{
    /// <summary>
    /// Обертка запроса получения списка сообщений чата с постраничной разбивкой
    /// </summary>
    public static class GetPagedUserMessagesQuery 
    {
        /// <summary>
        /// Запрос получения списка сообщения чата с постраничной разбивкой.
        /// </summary>
        public sealed class Query: PagingParams<ChatMessagesFilter>, IRequest<PagedEfList<ChatMessagesFilter, ChatMessage>>
        {
            
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Query, PagedEfList<ChatMessagesFilter, ChatMessage>>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IHttpContextAccessor _accessor;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();

            private IBaseRepository<ChatMessage> MessageRepo => _unitOfWork.Repository<ChatMessage>();

            private string User => _accessor.HttpContext.User.Identity.Name;

            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == User.ToLower()&& !_.IsDeleted).First();

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor accessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _accessor = accessor;
            }

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<PagedEfList<ChatMessagesFilter, ChatMessage>> Handle(Query request, CancellationToken cancellationToken)
            {
                try
                {
                    if (!ChatRepo.Query.Any(_ => _.Id == request.Filter.ChatId && !_.IsDeleted))
                    {
                        throw new EntityNotFoundException($"Чат с ИД = {request.Filter.ChatId} не найден");
                    }

                    var chat = await ChatRepo.GetAsync(request.Filter.ChatId);

                    long curUserId = CurUser.Id;

                    if (!(CurUser.IsAdmin || chat.CreateUserId == curUserId || chat.Users.Any(_ => _.UserId == curUserId)))
                    {
                        throw new FrameworkException($"Нет прав на просмотр сообщений чата");
                    }
                    
                    return await MessageRepo.GetPagedList(request, MappingFunc).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    throw new FrameworkException($"Пользователь {User}: произошла ошибка при получении списка сообщения чата", ex);
                }
            }

            private async Task<IList> MappingFunc(IQueryable<ChatMessage> chatMessages)
            {
                chatMessages = chatMessages.AsNoTracking();
                var messages = _mapper.Map<List<MessageModel>>(chatMessages);
                var usersQ = UserRepo.Query.AsNoTracking();
                var userIds = messages.Select(_ => _.UserId).Distinct();
                var users = await usersQ.Where(_ => userIds.Contains(_.Id)).ProjectTo<UserModel>(_mapper.ConfigurationProvider).ToListAsync();
                foreach (var message in messages)
                {
                    message.User = users.Single(_ => _.Id == message.UserId);
                }

                return messages;
            }
        }
    }
}