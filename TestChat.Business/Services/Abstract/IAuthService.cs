using System.Threading.Tasks;
using Framework.Services;
using TestChat.Business.Models;

namespace TestChat.Business.Services.Abstract
{
    /// <summary>
    /// Сервис авторизации
    /// </summary>
    public interface IAuthService : IService
    {
        /// <summary>
        /// Проверка учетных данных пользователя, формирование для него token доступа
        /// </summary>
        /// <param name="credentials">Учетные данные пользователя</param>
        /// <returns>Token авторизации</returns>
        Task<object> Login(Credentials credentials);
    }
}