using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Framework;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Extensions;
using Framework.Services;
using Microsoft.Extensions.Options;
using TestChat.Business.Exceptions;
using TestChat.Business.Models;
using TestChat.Business.Services.Abstract;
using TestChat.Data.Models;

namespace TestChat.Business.Services.Concrete
{
    /// <inheritdoc cref="IAuthService"/>
    public class AuthService : BaseService, IAuthService
    {
        private readonly JwtIssuerOptions _jwtOptions;

        private readonly IUnitOfWork _ufw;

        private IBaseRepository<User> UserRepo => _ufw.Repository<User>();

        private readonly PasswordCryptographer _passwordCryptographer;

        /// <inheritdoc cref="BaseService"/>
        public AuthService(
            IOptions<JwtIssuerOptions> jwtOptions,
            IUnitOfWork unitOfWork,
            PasswordCryptographer passwordCryptographer)
            : base(LoggerService.GetLogger(CommonConstants.AppLoggerName))
        {
            _jwtOptions = jwtOptions.Value;
            _ufw = unitOfWork;
            _passwordCryptographer = passwordCryptographer;
        }

        /// <inheritdoc cref="IAuthService"/>
        public async Task<object> Login(Credentials credentials)
        {
            var user = await GetUserForVerify(credentials);

            if(!_passwordCryptographer.AreEqual(user.Password, credentials.Password))
            {
                throw new UnauthorizedException($"Неверный логин или пароль");
            }

            _logService.LogInfo($"Пользователь {credentials.UserName} успешно авторизован");

            var claimsIdentity = GetClaimsIdentity(user);

            return await GetFullJwtToken(user.Login, claimsIdentity);
        }

        #region help methods
        /// <summary>
        /// Получение администратора для проверки его пароля
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        private async Task<User> GetUserForVerify(Credentials credentials)
        {
            string userName = credentials.UserName;
            string password = credentials.Password;
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                throw new UnauthorizedException($"Логин или пароль не могут быть пустыми");

            var user = await _ufw.Repository<User>().FirstOrDefaultAsync(_ =>
                                _.Login.ToLower() == userName.ToLower() &&
                                !_.IsDeleted);

            if(user == null)
            {
                throw new UnauthorizedException($"Не найдено пользователя с логином {credentials.UserName}");
            }

            return user;
        }

        /// <summary>
        /// Получение claim'ов от роли пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private ClaimsIdentity GetClaimsIdentity(User user)
        {
            ClaimsIdentity identity = new ClaimsIdentity(new GenericIdentity(user.Login, "Token"));

            identity.AddClaim(new Claim(ClaimTypes.Name, user.Login));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
            return identity;
        }

        /// <summary>
        /// Generates JWT token.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="identity">The identity.</param>
        /// <returns></returns>
        private async Task<object> GetFullJwtToken(string userName, ClaimsIdentity identity)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, _jwtOptions.IssuedAt.DatetimeToUnixTimeStamp().ToString(), ClaimValueTypes.Integer64),
                identity.FindFirst(ClaimTypes.Role),
                identity.FindFirst(ClaimTypes.Name),
            };

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
            issuer: _jwtOptions.Issuer,
            audience: _jwtOptions.Audience,
            claims: claims,
            notBefore: _jwtOptions.NotBefore,
            expires: _jwtOptions.Expiration,
            signingCredentials: _jwtOptions.SigningCredentials);

            var authToken = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                id = identity.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value,
                authToken,
                expires_in = (int)_jwtOptions.ValidFor.TotalSeconds,
                username = userName
            };

            return response;
        }
        #endregion

    }
}