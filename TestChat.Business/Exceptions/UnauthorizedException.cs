using System;
using Framework.Exceptions;

namespace TestChat.Business.Exceptions
{
    /// <inheritdoc />
    public class UnauthorizedException : FrameworkException
    {
        public UnauthorizedException(string message)
            : base(message) { }

        public UnauthorizedException(string message, Exception innerException)
            :base(message, innerException) { }
    }
}