using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace TestChat.Business.SignalrHubs
{
    public class ChatHub : Hub
    {
        public const string CreateChatMessage = "CreateChatMessage";
        public const string DeleteChatMessage = "DeleteChatMessage";
        private readonly IConnectionManager _connectionManager;
        public ChatHub(IConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }

        public override Task OnConnectedAsync()
        {
            if (Context.User.Identity.IsAuthenticated && !string.IsNullOrWhiteSpace(Context.User?.Identity?.Name))
            {
                _connectionManager.AddConnection(Context.User.Identity.Name, Context.ConnectionId);
            }

            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _connectionManager.RemoveConnection(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }
    }
}