using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestChat.Business.SignalrHubs
{
    /// <summary>
    /// Helper
    /// </summary>
    public interface IChatMessageHelper
    {
        /// <summary>
        /// Список пользователей, которые присоединены к Signalr
        /// </summary>
        IEnumerable<string> OnlineUsers { get; }
        
        /// <summary>
        /// Отсылка объекта клиенту userName
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="obj"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        ValueTask SendNotification(string userName, string method, object obj);

        /// <summary>
        /// Отсылка объекта указанному списку клиентов <see cref="users"/>
        /// </summary>
        /// <param name="users"></param>
        /// <param name="method"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        ValueTask SendNotification(string[] users, string method, object obj);
    }
}