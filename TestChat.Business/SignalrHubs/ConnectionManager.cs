using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

#nullable enable
namespace TestChat.Business.SignalrHubs
{
    /// <inheritdoc />
    public class ConnectionManager : IConnectionManager
    {
        private static readonly ConcurrentDictionary<string, HashSet<string>> _userMap = new ConcurrentDictionary<string, HashSet<string>>();
        
        /// <inheritdoc />
        public void AddConnection(string userName, string connectionId)
        {
            if (!_userMap.ContainsKey(userName))
            {
                _userMap.TryAdd(userName, new HashSet<string>());
            }

            _userMap[userName].Add(connectionId);
        }

        /// <inheritdoc />
        public void RemoveConnection(string connectionId)
        {
            foreach (var userName in _userMap.Keys)
            {
                if (!_userMap.ContainsKey(userName) || !_userMap[userName].Contains(connectionId)) continue;
                _userMap[userName].Remove(connectionId);
                if (!_userMap[userName].Any())
                {
                    _userMap.Remove(userName, out _);
                }
                break;
            }
        }

        /// <inheritdoc />
        public HashSet<string> GetConnections(string userName) =>
            _userMap.ContainsKey(userName) ? _userMap[userName] : new HashSet<string>();

        /// <inheritdoc />
        public IEnumerable<string> OnlineUsers => _userMap.Keys;
    }
}