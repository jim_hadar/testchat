using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace TestChat.Business.SignalrHubs
{
    /// <inheritdoc />
    public class ChatMessageHelper : IChatMessageHelper
    {
        private readonly IConnectionManager _connectionManager;
        private readonly IHubContext<ChatHub> _chatContext;

        /// <inheritdoc />
        public ChatMessageHelper(IConnectionManager connectionManager, IHubContext<ChatHub> chatContext)
        {
            _connectionManager = connectionManager;
            _chatContext = chatContext;
        }

        /// <inheritdoc />
        public IEnumerable<string> OnlineUsers => _connectionManager.OnlineUsers;
        
        /// <inheritdoc />
        public async ValueTask SendNotification(string userName, string method, object obj)
        {
            try
            {
                var connections = _connectionManager.GetConnections(userName).ToList();
                if (connections.Any())
                {
                    foreach (var connection in connections)
                    {
                        try
                        {
                            await _chatContext.Clients.Clients(connection).SendAsync(method, obj);
                        }
                        catch (Exception)
                        {
                            //
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //
            }
        }

        public async ValueTask SendNotification(string[] users, string method, object obj)
        {
            try
            {
                await _chatContext.Clients.Users(users).SendAsync(method, obj);
            }
            catch (Exception ex)
            {
                //
            }
        }
    }
}