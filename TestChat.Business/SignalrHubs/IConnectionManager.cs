using System.Collections.Generic;

#nullable enable
namespace TestChat.Business.SignalrHubs
{
    /// <summary>
    /// Управление подключениями Signalr
    /// </summary>
    public interface IConnectionManager
    {
        /// <summary>
        /// Доблавление информации о соединении
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="connectionId">ИД соединения</param>
        void AddConnection(string userName, string connectionId);
        
        /// <summary>
        /// Удаление соединения
        /// </summary>
        /// <param name="connectionId"></param>
        void RemoveConnection(string connectionId);
        
        /// <summary>
        /// Список соединений пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        HashSet<string> GetConnections(string userName);
        
        /// <summary>
        /// Список пользователей онлайн
        /// </summary>
        IEnumerable<string> OnlineUsers { get; }
    }
}