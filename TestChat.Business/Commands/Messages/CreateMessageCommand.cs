using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using TestChat.Business.Models.Messages;
using TestChat.Business.Models.Users;
using TestChat.Business.SignalrHubs;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Messages
{
    /// <summary>
    /// Обертка команды создания сообщения
    /// </summary>
    public static class CreateMessageCommand
    {
        /// <summary>
        /// Команда создания сообщения
        /// </summary>
        public sealed class Command : IRequest<MessageModel>
        {
            public Command(MessageCreateModel model)
            {
                Model = model;
            }

            /// <summary>
            /// Сообщение
            /// </summary>
            public MessageCreateModel Model { get; set; }
        }

        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public class Handler : IRequestHandler<Command, MessageModel>
        {
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IHttpContextAccessor _accessor;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();
            private IBaseRepository<ChatMessage> MessageRepo => _unitOfWork.Repository<ChatMessage>();
            private string User => _accessor.HttpContext.User.Identity.Name;
            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == User.ToLower() && !_.IsDeleted).First();

            private readonly IChatMessageHelper _chatMessageHelper;

            private readonly IHubContext<ChatHub> _chatContext;

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor accessor, IChatMessageHelper chatMessageHelper,IHubContext<ChatHub> chatContext)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _accessor = accessor;
                _chatMessageHelper = chatMessageHelper;
                _chatContext = chatContext;
            }

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<MessageModel> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    var chat = await ChatRepo.GetAsync(request.Model.ChatId);
                    if (chat.Users.All(_ => _.UserId != CurUser.Id))
                    {
                        throw new EntityNotFoundException($"Пользователь не найден в чате");
                    }

                    var message = new ChatMessage
                    {
                        Content = request.Model.Content,
                        ChatId = request.Model.ChatId,
                        DateCreate = DateTime.Now,
                        UserId = CurUser.Id,
                        IsRead = false
                    };

                    message = await MessageRepo.CreateAsync(message);
                    var messageDto = _mapper.Map<MessageModel>(message);
                    messageDto.User = _mapper.Map<UserModel>(CurUser);

                    var chatMembers = chat.Users.Select(_ => _.User.Login).ToArray();

                    await _chatMessageHelper.SendNotification(chatMembers, ChatHub.CreateChatMessage, messageDto);
                    
                    return messageDto;
                }
                catch (Exception ex)
                {
                    _unitOfWork.RollBack();
                    throw new FrameworkException($"Пользователь {User}: ошибка создания сообзения - {ex.Message}", ex);
                }
            }
        }
    }
}
