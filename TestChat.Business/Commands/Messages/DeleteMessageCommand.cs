using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Messages
{
    /// <summary>
    /// Обертка команды удаления сообщения
    /// </summary>
    public static class DeleteMessageCommand
    {
        /// <summary>
        /// Команда удаления сообщения
        /// </summary>
        public sealed class Command : IRequest<Unit>
        {
            public Command(long id)
            {
                Id = id;
            }
            
            /// <summary>
            /// ИД сообщения для удаления
            /// </summary>
            public long Id { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Command, Unit>
        {
            private readonly IUnitOfWork _unitOfWork;
            private readonly IHttpContextAccessor _accessor;
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();
            private IBaseRepository<ChatMessage> MessageRepo => _unitOfWork.Repository<ChatMessage>();
            private string User => _accessor.HttpContext.User.Identity.Name;
            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == User.ToLower() && !_.IsDeleted).First();

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public Handler(IUnitOfWork unitOfWork, IHttpContextAccessor accessor)
            {
                _unitOfWork = unitOfWork;
                _accessor = accessor;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                try
                {
                    var message = await MessageRepo.GetAsync(request.Id);
                    if (!(CurUser.IsAdmin || message.UserId == CurUser.Id))
                    {
                        throw new FrameworkException($"Нет прав на удаление сообщения");
                    }

                    await MessageRepo.DeleteAsync(message);
                }
                catch (Exception ex)
                {
                    _unitOfWork.RollBack();
                    throw new FrameworkException($"Пользователь {User} - ошибка удаления сообщения - {ex.Message}");
                }

                return Unit.Value;
            }
        }
    }
}