using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Chats;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Chats
{
    /// <summary>
    /// Обертка команды создания чата
    /// </summary>
    public static class CreateChatCommand
    {
        /// <summary>
        /// Команда создания чата
        /// </summary>
        public sealed class Command : IRequest<ChatModel>
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="model"></param>
            public Command(CreateChatModel model)
            {
                Model = model;
            }
            
            /// <summary>
            /// Модель для создания чата
            /// </summary>
            public CreateChatModel Model { get; }
                
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Command, ChatModel>
        {
            /// <summary>
            /// AutoMapper
            /// </summary>
            private readonly IMapper _mapper;

            /// <summary>
            /// IUnitOfWork
            /// </summary>
            private readonly IUnitOfWork _unitOfWork;

            /// <summary>
            /// User Repo
            /// </summary>
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();

            /// <summary>
            /// Chat Repo
            /// </summary>
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();

            /// <summary>
            /// 
            /// </summary>
            private readonly IHttpContextAccessor _accessor;
            
            private ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);
            
            private string User => _accessor.HttpContext.User.Identity.Name;
            
            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == _accessor.HttpContext.User.Identity.Name.ToLower()&& !_.IsDeleted).First();

            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _accessor = contextAccessor;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<ChatModel> Handle(Command request, CancellationToken cancellationToken)
            {
                var model = request.Model;
                var chat = new Chat
                {
                    Name = model.Name,
                    DateCreate = DateTime.Now,
                };
                try
                {
                    chat.CreateUserId = CurUser.Id;
                    chat.CreateUser = CurUser;
                    
                    // _unitOfWork.BeginTransaction();

                    chat = await ChatRepo.CreateAsync(chat);
                    chat.Users.Add(new ChatUser
                    {
                        UserId = CurUser.Id,
                        ChatId = chat.Id
                    });
                    foreach (var memberId in model.Members.Where(_ => _ != chat.CreateUserId).Distinct())
                    {
                        if (UserRepo.Query.Any(_ => !_.IsDeleted && _.Id == memberId))
                        {
                            chat.Users.Add(new ChatUser
                            {
                                UserId = memberId,
                                ChatId = chat.Id,
                                User = UserRepo.Get(memberId)
                            });           
                        }
                    }

                    chat = await ChatRepo.UpdateAsync(chat);
                    
                    // _unitOfWork.CommitTransaction();
                    
                    Logger.LogInfo($"{User}: успешно создан чат {chat.Name}");

                    return _mapper.Map<ChatModel>(chat);
                }
                catch (Exception ex)
                {
                    _unitOfWork.RollBack();
                    // _unitOfWork.RollBackTransaction();
                    throw;
                }
            }
        }
    }
}