using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Chats;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Chats
{
    /// <summary>
    /// Обертка команды обновления информации о чате
    /// </summary>
    public static class UpdateChatCommand
    {
        /// <summary>
        /// Команда обновления информации о чате
        /// </summary>
        public sealed class Command : IRequest<ChatModel>
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="model"></param>
            public Command(UpdateChatModel model)
            {
                Model = model;
            }
            
            /// <summary>
            /// Модель чата для обновления 
            /// </summary>
            public UpdateChatModel Model { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>    
        public sealed class Handler : IRequestHandler<Command, ChatModel>
        {
            /// <summary>
            /// AutoMapper
            /// </summary>
            private readonly IMapper _mapper;

            /// <summary>
            /// IUnitOfWork
            /// </summary>
            private readonly IUnitOfWork _unitOfWork;

            /// <summary>
            /// User Repo
            /// </summary>
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();

            /// <summary>
            /// Chat Repo
            /// </summary>
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();

            /// <summary>
            /// 
            /// </summary>
            private readonly IHttpContextAccessor _accessor;
            
            private ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);
            
            private string User => _accessor.HttpContext.User.Identity.Name;
            
            private User CurUser => UserRepo.Get(_ => _.Login.ToLower() == _accessor.HttpContext.User.Identity.Name.ToLower() && !_.IsDeleted).First();

            public Handler(IMapper mapper, IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _accessor = contextAccessor;
            }
            public async Task<ChatModel> Handle(Command request, CancellationToken cancellationToken)
            {
                var model = request.Model;
                try
                {
                    var chat = await ChatRepo.FirstOrDefaultAsync(_ => !_.IsDeleted && _.Id == model.ChatId);
                    if (!(chat.CreateUserId == CurUser.Id || CurUser.IsAdmin))
                    {
                        throw new FrameworkException($"Не достаточно прав");
                    }
                    chat.Name = model.Name;
                    chat.Users.Clear();
                    
                    chat.Users.Add(new ChatUser
                    {
                        UserId = chat.CreateUserId,
                        ChatId = chat.Id
                    });
                    
                    foreach (var memberId in model.Members.Where(_ => _ != chat.CreateUserId).Distinct())
                    {
                        if (UserRepo.Query.Any(_ => !_.IsDeleted && _.Id == memberId))
                        {
                            chat.Users.Add(new ChatUser
                            {
                                ChatId = chat.Id,
                                UserId = memberId,
                                User = UserRepo.Get(memberId)
                            });
                        }
                    }
                    
                    await ChatRepo.UpdateAsync(chat);
                    
                    // _unitOfWork.CommitTransaction();
                    
                    Logger.LogInfo($"{User}: успешно обновлен чат {chat.Name}");

                    return _mapper.Map<ChatModel>(chat);
                }
                catch (Exception ex)
                {
                    _unitOfWork.RollBack();
                    // _unitOfWork.RollBackTransaction();
                    throw;
                }
            }
        }
    }
}