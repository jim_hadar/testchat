using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions;
using Framework.Exceptions.Db;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Chats
{
    /// <summary>
    /// Обертка команды удаления чата
    /// </summary>
    public static class DeleteChatCommand
    {
        /// <summary>
        /// Команда удаления чата
        /// </summary>
        public sealed class Command : IRequest<Unit>
        {
            public Command(long id)
            {
                Id = id;
            }
            
            public long Id { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>    
        public sealed class Handler : IRequestHandler<Command, Unit>
        {
            /// <summary>
            /// IUnitOfWork
            /// </summary>
            private readonly IUnitOfWork _unitOfWork;

            /// <summary>
            /// User Repo
            /// </summary>
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();

            private User CurUser => UserRepo.Get(_ => _.Login == User && !_.IsDeleted).First();

            /// <summary>
            /// Chat Repo
            /// </summary>
            private IBaseRepository<Chat> ChatRepo => _unitOfWork.Repository<Chat>();

            /// <summary>
            /// 
            /// </summary>
            private readonly IHttpContextAccessor _accessor;
            
            private ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);
            
            private string User => _accessor.HttpContext.User.Identity.Name;
            
            public Handler(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor)
            {
                _unitOfWork = unitOfWork;
                _accessor = contextAccessor;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>    
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var chat = await ChatRepo.GetAsync(request.Id);
                if (!(chat.CreateUserId == CurUser.Id || CurUser.IsAdmin))
                {
                    throw new FrameworkException("Нет прав на удаление чата");
                }
                try
                {
                    string chatName = chat.Name;

                    await ChatRepo.DeleteAsync(chat);
                    
                    Logger.LogInfo($"{User}: успешно удален чат {chatName}");
                }
                catch (Exception ex)
                {
                    throw new ObjectDeleteException($"Произошла ошибка при удалении чата - {ex.Message}", ex);
                }

                return Unit.Value;
            }
        }
    }
}