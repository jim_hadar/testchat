using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions.Db;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Users
{
    /// <summary>
    /// Команда обновления пользователя для административной части 
    /// </summary>
    public static class UpdateUserForAdminCommand
    {
        /// <summary>
        /// Команда обновления пользователя 
        /// </summary>
        public sealed class Command : IRequest<UserForAdminModel>
        {
            /// <summary>
            /// Initialize instance of <see cref="Command"/>
            /// </summary>
            /// <param name="model">Модель пользователя для создания</param>
            public Command(UserForAdminModel model)
            {
                Model = model;
            }
            
            /// <summary>
            /// Модель пользователя для создания
            /// </summary>
            public UserForAdminModel Model { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Command, UserForAdminModel>
        {
            /// <summary>
            /// AutoMapper
            /// </summary>
            private readonly IMapper _mapper;

            /// <summary>
            /// IUnitOfWork
            /// </summary>
            private readonly IUnitOfWork _unitOfWork;

            /// <summary>
            /// User Repo
            /// </summary>
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();

            private readonly PasswordCryptographer _passwordCryptographer;

            private readonly IHttpContextAccessor _accessor;
            private ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);

            private string User => _accessor.HttpContext.User.Identity.Name;

            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>    
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, PasswordCryptographer passwordCryptographer, IHttpContextAccessor accessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _passwordCryptographer = passwordCryptographer;
                _accessor = accessor;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<UserForAdminModel> Handle(Command request, CancellationToken cancellationToken)
            {
                var model = request.Model;

                try
                {
                    if (!string.IsNullOrWhiteSpace(request.Model.Password) && 
                        !PasswordValidator.Check(request.Model.Password, out var message))
                    {
                        throw new Exception(message);
                    }
                    var entity = await UserRepo.GetAsync(model.Id).ConfigureAwait(false);
                    entity = _mapper.Map(model, entity);
                    if (!string.IsNullOrWhiteSpace(model.Password))
                    {
                        entity.Password = _passwordCryptographer.GenerateSaltedPassword(model.Password);
                    }
                    entity = await UserRepo.UpdateAsync(entity);
                    Logger.LogInfo($"{User}: обновлен пользователь {entity.Name}");
                    return _mapper.Map<UserForAdminModel>(entity);
                }
                catch (Exception ex)
                {
                    throw new ObjectUpdateException($"Произошла ошибка ошибка при обновлении пользователя", ex);
                }
            }
        }
    }
}