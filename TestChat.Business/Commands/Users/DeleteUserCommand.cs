using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions.Db;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Users
{
    /// <summary>
    /// Обертка команды удаления пользователя
    /// </summary>
    public static class DeleteUserCommand
    {
        /// <summary>
        /// Команда удаления пользователя
        /// </summary>
        public sealed class Command : IRequest<Unit>
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="id">ИД пользователя для удаления</param>
            public Command(long id)
            {
                Id = id;
            }
            
            /// <summary>
            /// ИД пользователя для удаления
            /// </summary>
            public long Id { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Command, Unit>
        {
            /// <summary>
            /// AutoMapper
            /// </summary>
            private readonly IMapper _mapper;

            /// <summary>
            /// IUnitOfWork
            /// </summary>
            private readonly IUnitOfWork _unitOfWork;

            /// <summary>
            /// User Repo
            /// </summary>
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();

            private readonly PasswordCryptographer _passwordCryptographer;

            private readonly IHttpContextAccessor _accessor;
            private ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);

            private string User => _accessor.HttpContext.User.Identity.Name;
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>    
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, PasswordCryptographer passwordCryptographer, IHttpContextAccessor accessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _passwordCryptographer = passwordCryptographer;
                _accessor = accessor;
            }
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var user = await UserRepo.GetAsync(request.Id).ConfigureAwait(false);
                if (user.Login == User)
                {
                    throw new ObjectDeleteException($"Удаление пользователя текущего сеанса запрещено");
                }

                try
                {
                    string userName = user.Name;
                    await UserRepo.DeleteAsync(user);
                    Logger.LogInfo($"{User}: удален пользователь: {userName}");
                    return Unit.Value;
                }
                catch (Exception ex)
                {
                    throw new ObjectDeleteException($"Произошла ошибка при удалении пользователя {user.Login}", ex);
                }
            }
        }
    }
}
