using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Framework;
using Framework.Constants;
using Framework.DAL.EF.Abstract;
using Framework.Exceptions.Db;
using Framework.Services;
using MediatR;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Commands.Users
{
    /// <summary>
    /// Обертка команды обновления пользователя (без его роли)
    /// </summary>
    public static class UpdateUserCommand
    {
        /// <summary>
        /// Команда обновления пользователя (для обновления пользователем собственных данных)
        /// </summary>
        public sealed class Command : IRequest<UserModel>
        {
            public Command(UserModel model)
            {
                Model = model;
            }
            
            /// <summary>
            /// Модель пользователя для обновления
            /// </summary>
            public UserModel Model { get; }
        }
        
        /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
        public sealed class Handler : IRequestHandler<Command, UserModel>
        {
            /// <summary>
            /// AutoMapper
            /// </summary>
            private readonly IMapper _mapper;

            /// <summary>
            /// IUnitOfWork
            /// </summary>
            private readonly IUnitOfWork _unitOfWork;

            /// <summary>
            /// User Repo
            /// </summary>
            private IBaseRepository<User> UserRepo => _unitOfWork.Repository<User>();

            private readonly PasswordCryptographer _passwordCryptographer;

            private readonly IHttpContextAccessor _accessor;
            private ILogService Logger => LoggerService.GetLogger(CommonConstants.AppLoggerName);

            private string User => _accessor.HttpContext.User.Identity.Name;
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>    
            public Handler(IMapper mapper, IUnitOfWork unitOfWork, PasswordCryptographer passwordCryptographer, IHttpContextAccessor accessor)
            {
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _passwordCryptographer = passwordCryptographer;
                _accessor = accessor;
            }
            
            /// <inheritdoc cref="IRequestHandler{TRequest,TResponse}"/>
            public async Task<UserModel> Handle(Command request, CancellationToken cancellationToken)
            {
                var model = request.Model;
                var user = await UserRepo.GetAsync(model.Id);
                
                if (user.Login != _accessor.HttpContext.User.Identity.Name.ToLower())
                {
                    throw new ObjectUpdateException($"Невозможно обновить данные другого пользователя");
                }

                try
                {
                    if (!string.IsNullOrWhiteSpace(request.Model.Password) && 
                        !PasswordValidator.Check(request.Model.Password, out var message))
                    {
                        throw new Exception(message);
                    }
                    user = _mapper.Map(model, user);
                    if (!string.IsNullOrWhiteSpace(model.Password))
                    {
                        user.Password = _passwordCryptographer.GenerateSaltedPassword(model.Password);
                    }
                    user = await UserRepo.UpdateAsync(user).ConfigureAwait(false);
                    Logger.LogInfo($"{User}: успешно обновлен пользователь {user.Name}");
                    return _mapper.Map<UserModel>(user);
                }
                catch (Exception ex)
                {
                    throw new ObjectUpdateException($"Ошибка обновления пользователя {user.Login}", ex);
                }
            }
        }
    }
}