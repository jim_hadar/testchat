using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Framework;
using Framework.DAL.EF.Abstract;
using TestChat.Business.Helpers;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Validators.Users
{
    public class UserModelValidator : AbstractValidator<UserModel>
    {
        private readonly IUnitOfWork _unitOfWork;
        private const string LoginField = "Логин";

        public UserModelValidator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            RuleFor(_ => _.Login)
                .NotEmpty()
                .WithMessage(string.Format(Validation.NotEmptyFieldTemplate, LoginField))
                .MustAsync((user, login, token) => UniqueLogin(user))
                .WithMessage(string.Format(Validation.UniqueNameFieldTemplate, LoginField))
                .Length(0, 50)
                .WithMessage(string.Format(Validation.MaxLength50FieldTemplate, LoginField));
            
            RuleFor(_ => _.Name)
                .NotEmpty()
                .Length(0, 50)
                .WithMessage(string.Format(Validation.MaxLength50FieldTemplate, LoginField));

            // RuleFor(_ => _.Password)
            //     .Must(_ => PasswordValidator.Check(_, out var message))
            //     .WithMessage($"Пароль не соответсвует парольной политике")
            //     .When(_ => !string.IsNullOrWhiteSpace(_.Password) ||
            //                !UserExists(_));
        }

        private async Task<bool> UniqueLogin(UserModel user)
        {
            string lowerLogin = user.Login.ToLower();
            var users = await _unitOfWork.Repository<User>().GetAsync(_ => !_.IsDeleted
                                                          &&
                                                          (_.Login.ToLower() == lowerLogin && _.Id != user.Id));
            return !users.Any();
        }

        private bool UserExists(UserModel user)
            => _unitOfWork.Repository<User>().Query.Any(_ => _.Id == user.Id && !_.IsDeleted);
    }
}