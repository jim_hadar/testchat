using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using Framework;
using Framework.DAL.EF.Abstract;
using TestChat.Business.Helpers;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Validators.Users
{
    public class UserForAdminModelValidator : AbstractValidator<UserForAdminModel>
    {

        private readonly IUnitOfWork _unitOfWork;

        private const string LoginField = "Логин";

        public UserForAdminModelValidator(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            RuleFor(_ => _.Login)
                .NotEmpty()
                .WithMessage(string.Format(Validation.NotEmptyFieldTemplate, LoginField))
                .MustAsync((user, login, token) => UniqueLogin(user))
                .WithMessage(string.Format(Validation.UniqueNameFieldTemplate, LoginField))
                .Length(0, 50)
                .WithMessage(string.Format(Validation.MaxLength50FieldTemplate, LoginField));

            RuleFor(_ => _.RoleId)
                .Must(_ => RoleExists(_))
                .WithMessage($"Не найдено указанной роли");

            // RuleFor(_ => _.Password)
            //     .Must(_ => PasswordValidator.Check(_, out var message))
            //     .WithMessage($"Пароль не соответсвует парольной политике")
            //     .When(_ => !string.IsNullOrWhiteSpace(_.Password) ||
            //                !UserExists(_));
        }

        /// <summary>
        /// Проверка логина пользователя на уникальность 
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns>Результат проверки</returns>
        private async Task<bool> UniqueLogin(UserForAdminModel user)
        {
            string lowerLogin = user.Login.ToLower();
            var users = await _unitOfWork.Repository<User>().GetAsync(_ => !_.IsDeleted
                                                          &&
                                                          (_.Login.ToLower() == lowerLogin && _.Id != user.Id)).ConfigureAwait(true);
            return !users.Any();
        }

        /// <summary>
        /// Проверка существования пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool UserExists(UserForAdminModel user)
            => _unitOfWork.Repository<User>().Query.Any(_ => _.Id == user.Id && !_.IsDeleted);

        /// <summary>
        /// проверка роли
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        private bool RoleExists(long roleId)
        {
            return _unitOfWork.Repository<UserRole>().Query.Any(r => r.Id == roleId);
        }
    }
}