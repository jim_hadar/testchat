using System.Linq;
using FluentValidation;
using Framework.DAL.EF.Abstract;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Chats;
using TestChat.Business.Models.Users;
using TestChat.Data.Models;

namespace TestChat.Business.Validators.Chats
{
    /// <summary>
    /// Валидатор создания чата
    /// </summary>
    public class CreateChatValidator : AbstractValidator<CreateChatModel>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _accessor;

        private string User => _accessor.HttpContext.User.Identity.Name;

        private User CurUser => _unitOfWork.Repository<User>().Get(_ => _.Login == User && !_.IsDeleted).First();
        
        public CreateChatValidator(IUnitOfWork unitOfWork, IHttpContextAccessor accessor)
        {
            _unitOfWork = unitOfWork;
            _accessor = accessor;
            RuleFor(_ => _.Name)
                .NotEmpty()
                .WithMessage($"Название чата не может быть пустым")
                .Must(UniqueName)
                .WithMessage($"Название чата должно быть уникальным");
        }

        private bool UniqueName(string name)
        {
            return !_unitOfWork.Repository<Chat>().Query.Any(_ => _.Name == name && _.CreateUserId == CurUser.Id && !_.IsDeleted);
        }
    }
}