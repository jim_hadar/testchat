using System.Linq;
using FluentValidation;
using Framework.DAL.EF.Abstract;
using Microsoft.AspNetCore.Http;
using TestChat.Business.Models.Chats;
using TestChat.Data.Models;

namespace TestChat.Business.Validators.Chats
{
    /// <summary>
    /// Валидатор для модели обновления чата
    /// </summary>
    public class UpdateChatValidator : AbstractValidator<UpdateChatModel>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _accessor;
        private string User => _accessor.HttpContext.User.Identity.Name;
        private User CurUser => _unitOfWork.Repository<User>().Get(_ => _.Login == User && !_.IsDeleted).First();
        
        public UpdateChatValidator(IUnitOfWork unitOfWork, IHttpContextAccessor accessor)
        {
            _unitOfWork = unitOfWork;
            _accessor = accessor;
            
            RuleFor(_ => _.Name)
                .NotEmpty()
                .WithMessage($"Название чата не может быть пустым")
                .Must((x, y) => UniqueName(x))
                .WithMessage($"Название чата должно быть уникальным");
        }
        
        private bool UniqueName(UpdateChatModel model)
        {
            return !_unitOfWork.Repository<Chat>().Query.Any(_ => _.Name == model.Name && _.CreateUserId == CurUser.Id && _.Id != model.ChatId && !_.IsDeleted);
        }
    }
}