using System.Reflection;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace TestChat.Business.Helpers
{
    public static class MapperInitializer
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var assembly = Assembly.GetAssembly(typeof(MapperInitializer));
            services.AddAutoMapper(assembly);
        }
    }
}