using System.Text.RegularExpressions;

namespace TestChat.Business.Helpers
{
    public sealed class Validation
    {
        #region [ Messages ]

        /// <summary>
        /// Шаблон сообщения об обязательности заполнения поля
        /// </summary>
        public const string NotEmptyFieldTemplate = "Поле {0} не может быть пустым";
        
        /// <summary>
        /// Шаблон сообщения об уникальности поля 
        /// </summary>
        public const string UniqueNameFieldTemplate = "Поле {0} должно быть уникальным";

        /// <summary>
        /// Шаблон сообщения о максимальной длине поля в 50 символов
        /// </summary>
        public const string MaxLength50FieldTemplate = "Длина поля {0} не должна превышать 50 символов";

        #endregion

        #region [ Regex patterns ]

        private const string OnlyLatinPattern = "^([A-Za-z0-9]*)$";

        #endregion
        
        /// <summary>
        /// Проверка на то, что название может содержать только латинские буквы и цифры 
        /// </summary>
        /// <param name="objectName"></param>
        /// <returns></returns>
        public static bool ContainsOnlyLatin(string objectName)
            => !string.IsNullOrWhiteSpace(objectName) &&
               Regex.IsMatch(objectName, OnlyLatinPattern);
    }
}