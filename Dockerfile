# устанавливаем базовый образ
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
# устанавливаем базовую директорию
WORKDIR /app
# копирование всех файлов .csproj для восстановления nuget 
#COPY * ./aspnetapp/
# восстановление пакетов nuget
#RUN dotnet restore

# копирование всех остальных файлов для создания билда
COPY . ./aspnetapp/
# установка рабочей директории внутри контейнера  
WORKDIR /app/aspnetapp
#RUN ls -al
# запуск паблиша
RUN dotnet publish ./TestChat.WebApi/TestChat.WebApi.csproj -c Deploy -r linux-x64 /p:PublishSingleFile=true 
RUN sed -i 's/Host=localhost/Host=postgres/g' /app/aspnetapp/TestChat.WebApi/bin/Deploy/netcoreapp3.1/linux-x64/publish/appsettings.json
#WORKDIR /app/aspnetapp/bin/Deploy/netcoreapp3.1/publish
#RUN ls -al

# Install ASP.NET Core
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim
EXPOSE 5000
WORKDIR /app
COPY --from=build /app/aspnetapp/TestChat.WebApi/bin/Deploy/netcoreapp3.1/linux-x64/publish /app
# See: https://github.com/dotnet/announcements/issues/20
# Uncomment to enable globalization APIs (or delete)
#ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT false
#RUN apk add --no-cache icu-libs
#ENV LC_ALL en_US.UTF-8
#ENV LANG en_US.UTF-8
CMD sleep 5 && ./TestChat.WebApi
#ENTRYPOINT ["./TestChat.WebApi"]