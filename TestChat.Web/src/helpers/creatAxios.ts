import axios from 'axios';
import appLocalStorage from "./appLocalStorage";
import { stringify } from 'qs';
import {RootDispatch} from "types/root";
import {dispatchHandleError, getBaseUrl} from "./general";

const createAxios = (dispatch: RootDispatch) => {
    /**
     * Get an authorization header.
     * @returns An authorization header, if a user has an auth token or an empty string if it's not.
     */
    const getAuthHeader = () => {
        const user = appLocalStorage.getUser(); 
        
        if (user && user.authToken) {
            return `Bearer ${user.authToken}`;
        }

        return '';
    };
    
    const baseUrl = getBaseUrl();
    
    const ax = axios.create({
        baseURL: baseUrl,
    });
    
    ax.interceptors.request.use((config) => {
       return {
           ...config,
           headers: {
               ...config.headers,
               /** Add an authorization header. */
               Authorization: getAuthHeader(),
               /** Prevent caching. */
               'Cache-Control': 'no-cache',
               Pragma: 'no-cache',
               Expires: -1,
           },
           paramsSerializer(params) {
               return stringify(params, { allowDots: true });
           },
       } 
    });
    
    ax.interceptors.response.use(function(response) {
            return response;
        },
        function(error) {
            if(error.response) {
                const {response} = error;
                if(response.config.method === 'delete')
                    return Promise.resolve(response);
            }
            dispatchHandleError(error, dispatch);
            return Promise.reject(error);
        }
    );
    
    return ax;
};

export default createAxios;