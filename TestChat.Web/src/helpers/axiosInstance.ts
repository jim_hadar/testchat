import createAxios from "./creatAxios";
import store from "redux/store";

const axios = createAxios(store.dispatch);

export default axios;