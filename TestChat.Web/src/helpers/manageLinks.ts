import Path from "types/constants/Path";
import Users from 'components/users/UserList';
import AddUser from "components/users/AddUser";
import Profile from "components/users/Profile";
import ChatList from "components/chats/ChatLIst";
import {LayoutProps} from "components/common/Layout";
import AddChat from "components/chats/AddChat";
import MessageChatList from "components/messages/MessageChatList";

/**
 * Список ссылок неавторизованного пользователя
 */
const getUnAuthLInks = (): LayoutProps => ({
    mainLink: {
        ref: '/auth',
        name: 'authorization',
        text: 'Авторизация'
    },
    links: [],
});

/**
 * Список ссылока авторизованного администратора
 */
const getAuthAdminLinks = (): LayoutProps => ({
    mainLink: {
        ref: Path.Profile,
        name: '/',
        text: 'Профиль',
        component: Profile,
        exact: true
    },
    links: [
        {
            ref: Path.Users,
            name: 'users',
            text: 'Пользователи',
            component: Users,
            visibleInMenu: true,
            exact: true
        },
        {
            ref: Path.AddUser,
            name: 'addUser',
            text: 'Создание пользователя',
            component: AddUser,
            visibleInMenu: false,
            exact: true
        },
        {
            ref: `${Path.Users}/:id`,
            name: 'usersView',
            text: 'Просмотр пользователя',
            component: AddUser,
            visibleInMenu: false,
            exact: true
        },
        {
            ref: Path.Profile,
            name: 'profile',
            text: 'Мой профиль',
            component: Profile,
            visibleInMenu: false,
            exact: true
        },
        {
            ref: Path.Chats,
            name: 'chats',
            text: 'Чаты',
            component: ChatList,
            visibleInMenu: true,
            exact: true
        },
        {
            ref: `${Path.MessageChats}`,
            name: 'messagechats',
            text: 'Сообщения',
            component: MessageChatList,
            visibleInMenu: true,
        },
        {
            ref: Path.AddChat,
            name: 'add-chat',
            text: 'Создание чата',
            component: AddChat,
            visibleInMenu: false,
            exact: true
        },
        {
            ref: `${Path.Chats}/:id`,
            name: 'edit-chat',
            text: 'Редактирование чата',
            component: AddChat,
            visibleInMenu: false,
            exact: true
        }
    ],
    redirectLink: {
        ref: Path.Profile,
        name: 'profile',
        text: 'Мой профиль',
        component: Profile,
        visibleInMenu: true,
        exact: true
    }
});

/**
 * Список ссылока авторизованного пользователя
 */
const getAuthUserLinks = (): LayoutProps => ({
    mainLink: {
        ref: Path.Profile,
        name: '/',
        text: 'Профиль',
        component: Profile,
        exact: true
    },
    links: [
        {
            ref: Path.Users,
            name: 'users',
            text: 'Пользователи',
            component: Users,
            visibleInMenu: true,
            exact: true
        },
        {
            ref: Path.Profile,
            name: 'profile',
            text: 'Мой профиль',
            component: Profile,
            visibleInMenu: false,
            exact: true
        },
        {
            ref: Path.Chats,
            name: 'chats',
            text: 'Чаты',
            component: ChatList,
            visibleInMenu: true,
            exact: true
        },
        {
            ref: `${Path.MessageChats}`,
            name: 'messagechats',
            text: 'Сообщения',
            component: MessageChatList,
            visibleInMenu: true,
        },
        {
            ref: Path.AddChat,
            name: 'add-chat',
            text: 'Создание чата',
            component: AddChat,
            visibleInMenu: false,
            exact: true
        },
        {
            ref: `${Path.Chats}/:id`,
            name: 'edit-chat',
            text: 'Редактирование чата',
            component: AddChat,
            visibleInMenu: false,
            exact: true
        }
    ],
    redirectLink: {
        ref: Path.Profile,
        name: 'profile',
        text: 'Мой профиль',
        component: Profile,
        visibleInMenu: true,
        exact: true
    }
});

export {
    getUnAuthLInks,
    getAuthAdminLinks,
    getAuthUserLinks
}