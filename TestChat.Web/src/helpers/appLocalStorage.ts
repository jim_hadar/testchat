import {AuthSuccessResponse} from "../types/auth";

const appLocalStorage = (() => {
    
    const items = {
        user: 'user'
    };

    type Item = typeof items[keyof typeof items];
    
    function setLocalItem(item: Item, value: unknown){
        const val = JSON.stringify(value);
        localStorage.setItem(item, val);
    }
    
    function getLocalItem(item: Item){
        const value = localStorage.getItem(item);
        return value ? JSON.parse(value) : null;
    }
    
    return {
        setUser(value: AuthSuccessResponse | null){
            if(value === null) {
                localStorage.removeItem(items.user);
                return;
            }
            setLocalItem(items.user, value);
        },
        getUser(){
            return getLocalItem(items.user);
        }
    };
})();

export default appLocalStorage;