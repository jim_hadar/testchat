import {GridOptions, IServerSideGetRowsParams} from "ag-grid-community";
import defaultGridOptions from "types/constants/tables/gridOptions";
import {BaseFilter, PagingParams} from "../types/general";

/**
 * инициализация gridOptions для ag-grid по умолчанию для server-Side рендеринга
 * @param gridOptions - входные параметры ag-grid;
 */
function initServerGridOptions(gridOptions: GridOptions = defaultGridOptions) {
    gridOptions.api?.sizeColumnsToFit();
    gridOptions.paginationAutoPageSize = false;
    gridOptions.pagination = true;
    gridOptions.suppressPaginationPanel = true;
    gridOptions.paginationPageSize = 20;
    gridOptions.suppressPaginationPanel = false;
    gridOptions.cacheBlockSize = 20;
    gridOptions.cacheQuickFilter = false;
    gridOptions.rowModelType = 'serverSide';
    gridOptions.skipHeaderOnAutoSize = true;
    gridOptions.suppressCsvExport = true;
    gridOptions.suppressExcelExport = true;
    gridOptions.suppressCopyRowsToClipboard = true;
    gridOptions.suppressClipboardPaste = true;
    return function (addGridOpt?: any): GridOptions {
        if (addGridOpt)
            return {
                ...gridOptions,
                ...addGridOpt
            };
        return gridOptions;
    }
}

/**
 * Возвращает параметр getRows для serverSide сортировки ag-grid
 * Здесь устанавливаются значения для постраничной разбивки
 * @param load - promise метод, который отвечает за загрузку данных с сервера.
 * @param filter - параметры фильтра.
 */
function getCommonServerSideGetRows<T extends BaseFilter>(
    load: ((loadParams: PagingParams<T>) => Promise<any>),
    filter: T) {
    return {
        getRows(params: IServerSideGetRowsParams): void {
            const {
                endRow,
                startRow,
                sortModel
            } = params.request;
            const pageSize: number = endRow - startRow;
            const pageNumber: number = endRow / pageSize;
            const sort: string = sortModel.length > 0 ? sortModel[0].colId : 'id';
            filter.order = sortModel.length > 0 ? (sortModel[0].sort ? sortModel[0].sort : 'asc') : 'asc';
            filter.sort = sort;

            const loadParams: PagingParams<T> = {
                pageSize,
                pageNumber,
                filter
            }
            load(loadParams)
                .then((rowsData) => {
                    params.successCallback(rowsData?.data, rowsData?.totalItemCount)
                    params.api.sizeColumnsToFit();
                })
                .catch(e => {
                    params.failCallback();
                    params.api.sizeColumnsToFit();
                });
        }
    };
}

export {
    initServerGridOptions,
    getCommonServerSideGetRows
}