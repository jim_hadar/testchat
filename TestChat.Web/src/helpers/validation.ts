/**
 * Проверка на то, что поле не пустое
 * @param str - строка для проверки
 * @returns {boolean} - результат проверки
 */
const required = (str: any) => str ? undefined : 'Поле обязательно к заполнению';

/**
 * Проверка на то, что поле содержит только латинские буквы
 * @param str - строка для проверки
 * @returns {Boolean} - результат проверки
 */
const onlyLatin = (str : string) => str && /^[a-zA-Z]+$/gi.test(str) ? undefined : 'Поле может содержать только латинские цифры';

/**
 * Проверка пароля пользователя на валидность
 * @param str
 */
const profilePassword = (str: string) => (!str || /^[a-zA-Z]+$/gi.test(str)) ? undefined : 'Пароль задане некорректно'; 

export{
    required,
    onlyLatin,
    profilePassword
}