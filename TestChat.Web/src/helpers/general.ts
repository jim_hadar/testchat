import appLocalStorage from "./appLocalStorage";
import AccountRole from "types/constants/AccountRole";
import jwt_decode from "jwt-decode";
import {RootDispatch} from "types/root";
import {closeBar, openBar} from "redux/actions/snackbar/actions";
import {SnackbarMessage} from "types/snackbarMessage";
import {logout} from "redux/actions/auth";
import {HttpTransportType, HubConnectionBuilder} from "@microsoft/signalr";

type DecodedToken = {
    /** Name. */
    'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name': string;

    /** Role. */
    'http://schemas.microsoft.com/ws/2008/06/identity/claims/role': AccountRole;

    /** Token expiration time. */
    exp: number;
}

/**
 * Get user data from the local storage.
 * @returns User data or null.
 */
const getUserData = () => {
    return appLocalStorage.getUser();
};

/**
 * Get an auth token.
 * @returns An auth token or null.
 */
const getAuthToken = () => {
    const userData = getUserData();

    return userData ? userData.authToken : null;
};

/**
 * Decode an auth token from the local storage.
 * @returns Decoded token or null.
 */
const getDecodedToken = () => {
    const token = getAuthToken();

    return token ? jwt_decode<DecodedToken>(token) : null;
};

/**
 * Get a user role from a decoded token.
 * @returns A user role or null.
 */
const getUserRole = (): AccountRole | null => {
    const token = getDecodedToken();
    if (!token)
        return null;
    return token['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];
};

/**
 * Get a user identifier.
 * @returns A user id or null.
 */
const getUserId = () => getUserData() ? getUserData()['id'] : null;

/**
 * Get a user name
 * @returns {string} A user name or null
 */
const getUserName = () => getUserData() ? getUserData()['username'] : null;

/**
 * Обработка ошибок и выдачу snackbar
 * @param error - ошибка
 * @param dispatch - RootDispatch
 */
const dispatchHandleError = (error: any, dispatch: RootDispatch) => {
    try {
        let errorMsg: SnackbarMessage = {
            message: '',
            type: "error"
        };
        if (error.response) {
            const {data, status} = error.response;
            if (status === 401) {
                dispatch(logout());
                errorMsg.message = "Пожалуйста, авторизуйтесь";
            }
            if (status === 400)
                errorMsg.message = data;
        } else {
            errorMsg.message = error.toString();
            if (errorMsg.message?.includes('Network Error'))
                errorMsg.message = 'Нет связи с сервером.';
        }
        dispatch(openBar(errorMsg));
        setTimeout(() => {
            dispatch(closeBar());
        }, 3000);
    } catch (e) {
        console.log(e);
    }
};

/**
 * Обработка успешных действий
 * @param message
 * @param dispatch
 */
const dispatchHandleSuccess = (message: string, dispatch: RootDispatch) => {
    const msg: SnackbarMessage = {
        type: "info",
        message
    }
    dispatch(openBar(msg));
    setTimeout(() => {
        dispatch(closeBar());
    }, 3000);
};

const getBaseUrl = () => process.env.NODE_ENV === 'production'
    ? '/'
    : 'http://localhost:5000/';

const buildSignalRConnection = (hub: 'hubs/chats') => {
    const url = getBaseUrl();
    const httpTransportType = process.env.NODE_ENV === 'production'
        ? HttpTransportType.ServerSentEvents
        : HttpTransportType.WebSockets;
    return new HubConnectionBuilder()
        .withUrl(`${url}${hub}`, {
            accessTokenFactory: () => getAuthToken() || '',
            skipNegotiation: httpTransportType === HttpTransportType.WebSockets,
            transport: httpTransportType,
        })
        .withAutomaticReconnect()        
        .build();
};

export type {
    DecodedToken
}

export {
    getDecodedToken,
    getAuthToken,
    getUserData,
    getUserRole,
    getUserId,
    getUserName,
    dispatchHandleSuccess,
    dispatchHandleError,
    getBaseUrl,
    buildSignalRConnection
}