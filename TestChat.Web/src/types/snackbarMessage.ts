export type SnackbarMessage = {
    type: 'info' | 'error',
    message?: string
};