/**
 * пользователь системы для роли "Пользователь"
 */
import {PagedList, PagingParams, BaseFilter} from "./general";

/**
 * Модель, используемая в профиле пользователя
 */
type User = {
    id: number,
    login: string,
    name: string,
    password?: string
};

/**
 * модель, используемая при редактировании пользователя администратором
 */
export type EditUser = {
    id?: number,
    login: string,
    name: string,
    password?: string,
    roleName: string,
    roleId: number
};

/**
 * Роль пользователя
 */
export type UserRole = {
    id: number,
    name: string
};

export type IEditUserRouterProps = {
    id: number;
};

export type PagedUserList = PagedList<User>;

export type UserPagingParams = PagingParams<BaseFilter>;

export default User;