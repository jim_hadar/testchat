/**
 * Модель для авторизации пользователя
 */
export type AuthCredentials = {
    userName : string,
    password: string
};

/**
 * Модель успешной авторизации
 */
export type AuthSuccessResponse = {
    id: string,
    authToken: string,
    expires_in: number, 
    username: string
};