import {ChildrenComponentProps} from "./helperForProps";
import React from "react";
import {RouteComponentProps} from "react-router";
/**
 * Описание ссылки
 */
export type LayoutLink = {
    /**
     * ссылка
     */
    ref: string,
    /**
     * уникальное название ссылки
     */
    name: string,
    /**
     * Тескт ссылки
     */
    text: string,
    /**
     * компонент, который связан с линком
     */
    component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>,
    /**
     * нужно ли отрисовывать компонент в верхнем меню 
     */
    visibleInMenu?: boolean,
    /**
     * 
     */
    exact?: boolean
};