export default interface Enumerate<T> {
    [controlName: string] : T
}