import {ReactNode} from "react";

/**
 * Свойство children для компонентов
 */
export type ChildrenComponentProps = {
    children?: ReactNode
}