/**
 * Данные с постраничной разбивкой
 */
interface PagedList<T> {
    pageCount: number;
    totalItemCount: number;
    pageNumber: number;
    pageSize: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
    isFirstPage: boolean;
    isLastPage: boolean;
    data: T[];
}

/**
 * Параметры пагинации
 * @param TFilter - фильтр - содержит параметры сортировки + параметры фильтрации
 */
interface PagingParams<TFilter extends BaseFilter> {
    pageNumber: number;
    pageSize: number;
    filter: TFilter
}

/**
 * базовый фильтр
 */
export interface BaseFilter {
    search?: string;
    sort?: string;
    order?: 'asc' | 'desc';
}

type SelectOption = {
    value: number,
    text: string
};

export {
    // @ts-ignore
    PagedList,
    // @ts-ignore
    PagingParams,
    // @ts-ignore
    SelectOption
}