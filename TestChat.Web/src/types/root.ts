import {RouterAction, RouterState} from "connected-react-router";
import {FormStateMap} from 'redux-form';
import {ThunkAction, ThunkDispatch} from "redux-thunk";
import { Reducer as ReduxReducer } from 'redux';
import { Actions as AuthActions } from "redux/actions/auth";
import {AuthState} from "redux/reducers/auth";
import {State as LoaderState} from "redux/reducers/loader";
import {UserActions, UserAsyncActions} from "redux/actions/user";
import {SnackbarActions} from "redux/actions/snackbar";
import {SnackbarState} from "redux/reducers/snackbar";
import {UserState} from "redux/reducers/user";
import {ChatActions, ChatAsyncActions} from "redux/actions/chat";
import {ChatState} from "redux/reducers/chat";
import {MessageActions, MessageAsyncActions} from "redux/actions/message";
import {MessageState} from "redux/reducers/message";

export type RootState = {
    form: FormStateMap,
    router: RouterState,
    auth: AuthState,
    loader: LoaderState
    snackbar: SnackbarState,
    user: UserState,
    chat: ChatState,
    message: MessageState
};

export type RootAction = 
    RouterAction |
    AuthActions |
    UserActions |
    UserAsyncActions |
    SnackbarActions |
    ChatActions |
    ChatAsyncActions |
    MessageAsyncActions |
    MessageActions;

/* Submit error for redux forms. */
export type SubmitError<T> = Partial<{
    [k in keyof T]: string;
}>

export type RootDispatch = ThunkDispatch<RootState, undefined, RootAction>;

export type RootThunkAction<R> = ThunkAction<R, RootState, undefined, RootAction>;

export type Reducer<S> = ReduxReducer<S, RootAction>;