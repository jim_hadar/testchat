import ProfileForm from "types/constants/forms/ProfileForm";
import store from "redux/store";
import {push} from "connected-react-router";
import Path from "types/constants/Path";
import {deleteAdminUser} from "redux/actions/user";
import {ICellRendererParams} from "ag-grid-community";
import {getUserRole} from "../../../../helpers/general";
import AccountRole from "../../AccountRole";

const dispatch = store.dispatch;

const userColumns = [
    {headerName: 'Логин', field: ProfileForm.Login, sortable: true, maxWidth: 200},
    {headerName: 'Имя пользователя', field: ProfileForm.Name, sortable: true},
    {
        headerName: 'Действия', 
        cellRenderer: 'actionsCellRenderer',
        colId: 'actions',
        width: 200,
        cellRendererParams: {
            agreeDialogBodyText: 'Вы уверены, что хотите удалить выбранного пользователя?',
            onEdit: (data: any) => {
                dispatch(push(`${Path.Users}/${data.id}`));
            },
            onDelete: (params: ICellRendererParams) => {
                // @ts-ignore
                dispatch(deleteAdminUser(params.data.id))
                    .then(() => {
                        params.api.purgeServerSideCache();
                    });                
            },
            deleteIsVisible: (data: any) => {
                const curUserRole : AccountRole | null = getUserRole();
                return curUserRole && curUserRole === AccountRole.Administrator;
            },
            editIsVisible: () => {
                const curUserRole : AccountRole | null = getUserRole();
                return curUserRole && curUserRole === AccountRole.Administrator;
            }
        },
        hide: !(getUserRole() && getUserRole() === AccountRole.Administrator)
    }
];

export default userColumns;