import {ICellRendererParams, ValueFormatterParams} from "ag-grid-community";
import moment from 'moment';
import store from "redux/store";
import {deleteChat} from "redux/actions/chat";
import Path from "types/constants/Path";
import {push} from "connected-react-router";
import {getUserId} from "../../../../helpers/general";

const dispatch = store.dispatch;

const chatColumns = [
    {
        headerName: 'Название',
        field: 'name',
        sortable: true
    },
    {
        headerName: 'Дата создания', 
        field: 'date', 
        sortable: true,
        valueFormatter: function(params: ValueFormatterParams) {
            const date = moment(params.value);
            return date.format("DD-MM-YYYY");
        }
    },
    {
        headerName: 'Кто создал', 
        field: 'createUser', 
        sortable: false,
        valueFormatter: (params: ValueFormatterParams) => params.value.name
    },
    {
        headerName: 'Участники',
        field: 'members',
        sortable: false,
        valueFormatter: function (params: ValueFormatterParams) {
            // @ts-ignore
            const members = params.value.map(m => m.name);
            return members.join(', ');
        }
    },
    {
        headerName: 'Действия',
        cellRenderer: 'actionsCellRenderer',
        colId: 'actions',
        width: 200,
        cellRendererParams: {
            agreeDialogBodyText: 'Вы уверены, что хотите удалить выбранный чат?',
            onEdit: (data: any) => {
                console.log(data);
                dispatch(push(`${Path.Chats}/${data.id}`));
            },
            onDelete: (params: ICellRendererParams) => {
                // @ts-ignore
                dispatch(deleteChat(params.data.id))
                    .then(() => {
                        params.api.purgeServerSideCache();
                        params.api.purgeServerSideCache();
                    });
            },
            deleteIsVisible: (data: any) => {
                const curUserId : number = Number(getUserId());
                return data.createUserId === curUserId;
            },
        }
    }
];

export default chatColumns;