const chatForMessagesColumn = [
    {
        headerName: 'Выберите чат для просмотра сообщений',
        field: 'name',
        sortable: false
    },
];

export default chatForMessagesColumn;