import userColumns from "./userColumns";
import chatColumns from "./chatColumns";
import chatForMessagesColumn from "./chatForMessagesColumn";

export default {
    userColumns,
    chatColumns,
    chatForMessagesColumn
};