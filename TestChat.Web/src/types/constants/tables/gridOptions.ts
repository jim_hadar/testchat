import {ColumnResizedEvent, GridOptions} from "ag-grid-community";

/**
 * Настройки ag-rgid по умолчанию
 */
const defaultGridOptions : GridOptions = {
    defaultColDef: {
        resizable: true,
        suppressMenu: true
    },
    onColumnResized(event: ColumnResizedEvent): void {
        
    },
}

export default defaultGridOptions;