/**
 * названия форм для redux-form
 */
enum FormName {
    Profile = 'profile',
    AddUser = 'addUser',
    AddChat = 'addChat',
    AddMessage = 'addMessage'
}

export default FormName;