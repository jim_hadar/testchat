enum AddChatForm {
    ChatId = 'chatId',
    Name = 'name',
    Members = 'members'
}

export default AddChatForm;