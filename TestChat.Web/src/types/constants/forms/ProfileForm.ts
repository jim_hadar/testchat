/**
 * описание полей формы профиля
 */
enum ProfileForm {
    Id = 'id',
    Login = 'login',
    Name = 'name',
    Password = 'password'
}

export default ProfileForm;