enum AddMessageForm {
    ChatId = 'chatId',
    Content = 'content'
}

export default AddMessageForm;