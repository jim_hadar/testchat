enum AddUserForm {
    Id = 'id',
    Login = 'login',
    Name = 'name',
    Role = 'roleId',
    Password = 'password'
}
    
export default AddUserForm;