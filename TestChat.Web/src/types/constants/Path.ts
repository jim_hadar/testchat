/**
 * Пути в навигации клиента
 */
enum Path {
    Users = '/users',
    AddUser = '/users/add',
    Login = '/login' ,
    Profile = '/profile',
    Chats = '/chats',
    AddChat = '/chats/add',
    MessageChats = '/chat-messages'
}

export default Path;