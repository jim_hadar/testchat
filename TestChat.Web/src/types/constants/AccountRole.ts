/**
 * ррли пользователя системы
 */
enum AccountRole{
    Administrator = 'administrator',
    User = 'user'
};

export default AccountRole;