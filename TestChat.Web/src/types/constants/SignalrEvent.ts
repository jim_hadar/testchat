/**
 * Событие signalr
 */
enum SignalrEvent{
    CreateChatMessage = 'CreateChatMessage'
}

export default SignalrEvent;