/**
 * Пути до апи
 */
enum Api {
    Auth = 'api/v1/auth/login',
    User = 'api/v1/user/',
    AdminUser = 'api/v1/admin/user',
    UserRoles = 'api/v1/admin/UserRole',
    Chats = 'api/v1/chat',
    Messages = 'api/v1/message'
}

export default Api;