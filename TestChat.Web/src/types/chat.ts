import User from "types/user";
import {BaseFilter, PagedList, PagingParams} from "./general";

/**
 * модель чата
 */
type Chat = {
    id: number,
    name: string,
    createUserId: number,
    date: Date,
    createUser: User,
    members: User[],
};

/**
 * Модель создания чата
 */
type CreateChatModel = {
    name: string,
    members: number[]
}

/**
 * Модель обновления чата
 */
type UpdateChatModel = CreateChatModel & {
    chatId: number
}

/**
 * Модель списка чатов с постраничной разбивкой
 */
type PagedChatList = PagedList<Chat>;

/**
 * Модель фильтра для чатов с постраничной разбивкой
 */
type ChatPagingParams = PagingParams<BaseFilter>


export {
    // @ts-ignore
    Chat, CreateChatModel, UpdateChatModel, PagedChatList, ChatPagingParams 
}