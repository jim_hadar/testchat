import User from "./user";
import {BaseFilter, PagedList, PagingParams} from "./general";

/**
 * Модель сообщения
 */
type MessageModel = {
    id: number,
    userId: number,
    user: User,
    dateCreate: Date,
    content: string,
    isRead: boolean,
    chatId: number
};

type CreateMessageModel = {
    chatId: number,
    content: string
};

type MessagePagingFilter = BaseFilter & {
    chatId: number,
};

type MessagePagingParams = PagingParams<MessagePagingFilter>; 

type PagedMessageList = PagedList<MessageModel>;

export type {
    MessageModel,
    CreateMessageModel,
    MessagePagingParams,
    PagedMessageList
}