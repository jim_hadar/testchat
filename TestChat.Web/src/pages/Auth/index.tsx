import React, {FC} from 'react';
import Layout from "components/common/Layout";
import { getUnAuthLInks } from "helpers/manageLinks";
import Auth from "../../components/Auth";

const AuthPage : FC<{}> = (props) => {
    const { mainLink, links } = getUnAuthLInks();    
    return (
        // @ts-ignore
        <Layout 
            mainLink={mainLink}
            links={links}
        >
            <Auth/>
        </Layout>
    );
};

export default AuthPage;