import {Reducer} from "types/root";
import {SnackbarMessage} from "types/snackbarMessage";
import {getType} from "typesafe-actions";
import {snackbarActions} from "redux/actions/snackbar";

const initialState: State = {
    message: "dasdasdasdasd",
    isOpen: false,
    type: "error"
};

const snackbarReducer: Reducer<State> = (state = initialState, action) => {
    switch (action.type) {
        case getType(snackbarActions.openBar):
            const {message, type} = action.payload;
            return {
                ...state,
                message,
                type,
                isOpen: true
            };
        case getType(snackbarActions.closeBar):
            return {
                ...state,
                isOpen: false
            };
        default:
            return state;
    }
};

type State = SnackbarMessage & {
    isOpen: boolean
}

export type SnackbarState = State;

export default snackbarReducer;