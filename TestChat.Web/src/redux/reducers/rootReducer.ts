import { combineReducers } from "redux";
import { History } from "history";
import { connectRouter } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';
import authReducer from './auth';
import loaderReducer from './loader';
import snackbarReducer from "./snackbar";
import {userReducer} from "./user";
import chatReducer from './chat';
import messageReducer from "./message";

const rootReducer = (history: History) => combineReducers({
   router: connectRouter(history) ,
   form: formReducer,
   auth: authReducer,
   loader: loaderReducer,
   snackbar: snackbarReducer,
   user: userReducer,
   chat: chatReducer,
   message: messageReducer
});

export default rootReducer;