import { Reducer } from 'types/root';
import { DeepReadonly } from 'utility-types';

const initialState: State = { };

/**
 * Reducer.
 */
const reducer: Reducer<State> = (state = initialState, action) => {
    const { type } = action;
    const [prefix, act, stage] = type.split('.');

    const isAsync = typeof stage === 'string';

    if (isAsync) {
        return {
            ...state,
            [`${prefix}.${act}`]: stage === 'REQUEST',
        };
    }

    return state;
};

/**
 * Types
 */
export type State = DeepReadonly<{
    /* Loading state of an async action. */
    [k: string]: boolean;
}>

export default reducer;
