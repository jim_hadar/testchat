import { Reducer } from "types/root";
import AccountRole from "types/constants/AccountRole";
import { DeepReadonly } from "utility-types";
import { getType } from "typesafe-actions";
import { authActions } from "redux/actions/auth";
import appLocalStorage from "../../helpers/appLocalStorage";
import {getUserId, getUserName, getUserRole} from "../../helpers/general";

const initialState: AuthState = {
    isAuth: Boolean(appLocalStorage.getUser()),
    isLoading: false,
    userRole: getUserRole(),
    userId: getUserId(),
    userName: getUserName()
};

export const reducer: Reducer<AuthState> = (state = initialState, action) => {
    switch (action.type) {
        case getType(authActions.login.request):
            return {
                ...state,
                isLoading: true
            };       
        case getType(authActions.login.success):
            return {
                ...state,
                isLoading: false,
                ...action.payload,
                isAuth: true
            };       
        case getType(authActions.login.failure): 
            return {
                ...state,
                isLoading: false
            };       
        case getType(authActions.logout.request): 
            return {
                ...state,
                isLoading: true
            };        
        case getType(authActions.logout.success): 
            return {
                ...state,
                isLoading: false,
                isAuth: false,
                userName: null,
                userId: null,
                userRole: null
            };       
        case getType(authActions.logout.failure): 
            return {
                ...state,
                isLoading: false
            };
        default: 
            return state;
    }
};

export type AuthState = DeepReadonly<{
    /** True if an user is already logged in. */
    isAuth: boolean;

    /** True if a request is submitting. */
    isLoading: boolean;

    /** A user role. */
    userRole: AccountRole | null;

    /** A user identifier. */
    userId: string | null;

    /** A user name. */
    userName: string | null;
}>

export default reducer;