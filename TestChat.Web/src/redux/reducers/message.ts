import {Chat} from "types/chat";
import {Reducer} from "types/root";
import {getType} from "typesafe-actions";
import {messageActions, messageAsyncActions} from "../actions/message";
import {MessageModel} from "types/message";

const initialState: State = {
    currentChat: undefined,
    messages: []
}

const messageReducer: Reducer<State> = (state = initialState, action) => {
    switch (action.type) {
        case getType(messageActions.setCurrentChat): {
            const {payload} = action;
            return {
                ...state,
                currentChat: payload,
                messages: []
            }
        }
        case getType(messageAsyncActions.getPagedMessages.success): {
            const {payload} = action;
            return {
                ...state,
                messages: payload
            }
        }
        case getType(messageActions.pushNewMessage): {
            const {payload} = action;
            const curMessages = [...state.messages];
            const selectedChat = state.currentChat;
            if(selectedChat &&
                selectedChat.id === payload.chatId &&
                !curMessages.find(m => m.id === payload.id)
            ) {
                curMessages.push(payload);
                return {
                    ...state,
                    messages: curMessages
                }
            }
            else{
                return state;
            }
        }
        default:
            return state;
    }
}

type State = {
    currentChat?: Chat,
    messages: MessageModel[]
}

export type{
    State as MessageState
}

export default messageReducer;