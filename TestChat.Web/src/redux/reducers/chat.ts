import {getType} from "typesafe-actions";
import {Reducer} from "types/root";
import {chatActions, chatAsyncActions} from "redux/actions/chat";
import {PagedChatList, ChatPagingParams} from "types/chat";
import User from "types/user";

const initialState : State = {
    paging: {
        pageNumber: 1,
        pageSize: 20,
        filter: {
            search: '',
            sort: 'id',
            order: 'asc'
        }
    },
    pagingData: undefined,
    allAvailableUsers: []
};
    
const chatReducer : Reducer<State> = (state = initialState, action) => {    
    switch (action.type) {
        case getType(chatActions.setChatFilter): {
            const {payload} = action;
            return {
                ...state,
                paging: payload
            }
        }
        case getType(chatAsyncActions.getPagedChats.success): {
            const {payload} = action;
            return {
                ...state,
                pagingData: payload
            }
        }
        case getType(chatAsyncActions.allAvailableChatUsers.success): {
            const {payload} = action;
            return {
                ...state,
                allAvailableUsers: payload
            }
        }
        default: 
            return state;
    }
}
    
type State = {
    paging: ChatPagingParams,
    pagingData?: PagedChatList,
    allAvailableUsers: User[]
};

export type {
    State as ChatState
}

export default chatReducer;