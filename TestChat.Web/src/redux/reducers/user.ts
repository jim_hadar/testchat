import {getType} from "typesafe-actions";
import {PagedUserList, UserPagingParams, UserRole} from "types/user";
import {Reducer} from "types/root";
import {userActions, userAsyncActions} from "redux/actions/user";

const initialState: State = {
    paging: {
        pageNumber: 1,
        pageSize: 20,
        filter: {
            search: '',
            sort: 'id',
            order: 'asc'
        }
    },
    pagingData: undefined,
    userRoles: []
}

const userReducer: Reducer<State> = (state = initialState, action) => {
    switch (action.type) {
        case getType(userActions.setUserFilter): {
            const {payload} = action;
            return {
                ...state,
                paging: payload
            }
        }
        case getType(userAsyncActions.pagedUsers.success): {
            const {payload} = action;
            return {
                ...state,
                pagingData: payload
            }
        }
        case getType(userAsyncActions.userRoles.success): {
            const {payload} = action;
            return {
                ...state,
                userRoles: payload
            }
        }
        default:
            return state;
    }
};

type State = {
    paging: UserPagingParams,
    pagingData?: PagedUserList,
    userRoles: UserRole[]
};

export type UserState = State;

export {
    userReducer,
};