import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from "./reducers/rootReducer";
import {routerMiddleware} from "connected-react-router";
import {history} from "helpers/history";

let enhancer;
const middlewares = [
  thunk,
  routerMiddleware(history)  
];

/** If a development environment. */
if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    /** Use a redux devtools enhancer, if browser has the installed extension. */
    const composeEnhancers = ((window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ as typeof compose)
        || compose;
    enhancer = composeEnhancers(applyMiddleware(...middlewares));
} else {
    enhancer = compose(applyMiddleware(...middlewares));
}

const store = createStore(rootReducer(history), enhancer);

// console.log(store);

export default store;