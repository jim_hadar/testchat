import {userActions, userAsyncActions} from './actions';
import initializeProfileForm from './initializeProfileForm';
import {pagedUsers} from './getPagedUsers';
import {ActionType} from "typesafe-actions";
import initializeUserRoles from "./initializeUserRoles";
import createAdminUser from "./createAdminUser";
import updateAdminUser from "./updateAdminUser";
import initializeEditUserAdminForm from "./initializeEditUserAdminForm";
import deleteAdminUser from "./deleteAdminUser";

export type UserAsyncActions = ActionType<typeof userAsyncActions>;
export type UserActions = ActionType<typeof userActions>;

export {
    userActions,
    userAsyncActions,
    initializeProfileForm,
    initializeUserRoles,
    pagedUsers,
    createAdminUser,
    updateAdminUser,
    initializeEditUserAdminForm,
    deleteAdminUser
}