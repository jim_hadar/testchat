import { getUser } from 'services/user';
import { getUserId } from "helpers/general";
import {RootDispatch} from "types/root";
import { userAsyncActions } from './index';
import { initialize } from 'redux-form';
import FormName from "types/constants/FormName";

/**
 * Инициализация формы профиля
 */
const initializeProfileForm = () => {
    return async (dispatch: RootDispatch) => {
        const userId : number = getUserId() || 0;
        dispatch(userAsyncActions.getUser.request());
        try {
            const { data } = await getUser(userId);
            
            dispatch(initialize(FormName.Profile, {...data}));
            
            dispatch(userAsyncActions.getUser.success());
        }
        catch (e) {
            dispatch(userAsyncActions.getUser.failure());
        }
    };
};

export default initializeProfileForm;