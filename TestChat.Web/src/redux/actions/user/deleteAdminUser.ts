import {deleteUser} from 'services/user';
import {RootDispatch} from "types/root";
import {userAsyncActions} from "./index";
import * as statuses from 'http-status-codes';
import {dispatchHandleError, dispatchHandleSuccess} from "helpers/general";

const deleteAdminUser = (id: number) => {
    return (dispatch: RootDispatch) => {
        dispatch(userAsyncActions.deleteUser.request());
        return deleteUser(id)
            .then(r => {
                if(r.status === statuses.NO_CONTENT){
                    dispatchHandleSuccess('Пользователь успешно удален', dispatch);
                    dispatch(userAsyncActions.deleteUser.success());
                }
                else{
                    dispatchHandleError(r.data.toString(), dispatch);
                }
            })
            .catch(e => {
                dispatch(userAsyncActions.deleteUser.failure());
                dispatchHandleError(e, dispatch);
            });
    }
};

export default deleteAdminUser;