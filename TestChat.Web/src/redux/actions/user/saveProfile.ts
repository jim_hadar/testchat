import {RootDispatch} from "types/root";
import { userAsyncActions } from "./index";
import {updateUser} from 'services/user';
import User from "types/user";
import appLocalStorage from "helpers/appLocalStorage";

const saveProfile = (user: User) => {
    return async (dispatch: RootDispatch) => {
        try {
            dispatch(userAsyncActions.updateUser.request());
            user.id = Number(user.id);
            const response = await updateUser(user);
            
            const { data } = response;
            
            let oldUser = appLocalStorage.getUser();
            oldUser.username = data.login;
            appLocalStorage.setUser(oldUser);
            
            dispatch(userAsyncActions.updateUser.success());
            window.location.reload();
        }
        catch (e) {
            dispatch(userAsyncActions.updateUser.failure());
        }
    }
};

export default saveProfile;