import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {getUserRoles} from "services/user";
import {userAsyncActions} from "./actions";

const initializeUserRoles = () => {
    return async (dispatch: RootDispatch, getState: Function) => {
        try{
            const curRoles = getState().user.userRoles;
            if(curRoles && curRoles.length > 0)
                return;
            
            dispatch(userAsyncActions.userRoles.request());
            
            const response = await getUserRoles();            
            const {data} = response;
            
            dispatch(userAsyncActions.userRoles.success(data));
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(userAsyncActions.userRoles.failure());
        }
    }
}

export default initializeUserRoles;