import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {userAsyncActions} from "./index";
import {getAdminUserById} from "services/user";
import {initialize} from "redux-form";
import FormName from "types/constants/FormName";

const initializeEditUserAdminForm = (id: number) => {
    return async (dispatch: RootDispatch) => {
        try{
            dispatch(userAsyncActions.getAdminUser.request());

            const response = await getAdminUserById(id);            
            const {data} = response;            
            dispatch(initialize(FormName.AddUser, data));
            
            dispatch(userAsyncActions.getAdminUser.success());
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(userAsyncActions.getAdminUser.failure());
        }
    }
}

export default initializeEditUserAdminForm;