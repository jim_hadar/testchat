import {createAction, createAsyncAction} from "typesafe-actions";
import {PagedUserList, UserPagingParams, UserRole} from "types/user";

const getUser = createAsyncAction(
    'USER.GET_BY_YD.REQUEST',
    'USER.GET_BY_ID.SUCCESS',
    'USER.GET_BY_ID.FAILURE'
)<undefined, undefined, undefined>();

const updateUser = createAsyncAction(
    'USER.UPDATE_BY_ID.REQUEST',
    'USER.UPDATE_BY_ID.SUCCESS',
    'USER.UPDATE_BY_ID.FAILURE'
)<undefined, undefined, undefined>();

const pagedUsers = createAsyncAction(
    'USER.GET_PAGED_USERS.REQUEST',
    'USER.GET_PAGED_USERS.SUCCESS',
    'USER.GET_PAGED_USERS.FAILURE'
)<undefined, PagedUserList, undefined>();

const userRoles = createAsyncAction(
    'ROLES.GET_ALL.REQUEST',
    'ROLES.GET_ALL.SUCCESS',
    'ROLES.GET_ALL.FAILURE'
)<undefined, UserRole[], undefined>();

const createUser = createAsyncAction(
    'USER.CREATE.REQUEST',
    'USER.CREATE.SUCCESS',
    'USER.CREATE.FAILURE'
)<undefined, undefined, undefined>();

const updateAdminUser = createAsyncAction(
    'USER.UPDATE_BY_ADMIN.REQUEST',
    'USER.UPDATE_BY_ADMIN.SUCCESS',
    'USER.UPDATE_BY_ADMIN.FAILURE'
)<undefined, undefined, undefined>();

const getAdminUser = createAsyncAction(
    'USER.GET_ADMIN_USER_BY_ID.REQUEST',
    'USER.GET_ADMIN_USER_BY_ID.SUCCESS',
    'USER.GET_ADMIN_USER_BY_ID.FAILURE'
)<undefined, undefined, undefined>();

const deleteUser = createAsyncAction(
    'USER.DELETE.REQUEST',
    'USER.DELETE.SUCCESS',
    'USER.DELETE.FAILURE'
)<undefined, undefined, undefined>();

const setUserFilter = createAction('USER.SET_USERS_FILTER')<UserPagingParams>();

export const userAsyncActions = {
    getUser,
    updateUser,
    pagedUsers,
    userRoles,
    createUser,
    updateAdminUser,
    getAdminUser,
    deleteUser
};

export const userActions = {
    setUserFilter
};