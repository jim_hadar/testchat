import {UserPagingParams} from "types/user";
import {RootDispatch} from "types/root";
import {userActions, userAsyncActions} from "./index";
import {getPagedUsers} from "services/user";

const pagedUsers = (params?: UserPagingParams) => {
    return async (dispatch: RootDispatch, getState: Function) => {
        try{
            if(!params)
                params = getState().user.paging;
            // @ts-ignore
            dispatch(userActions.setUserFilter(params));
            dispatch(userAsyncActions.pagedUsers.request());            
            // @ts-ignore
            const response = await getPagedUsers(params);            
            const {data} = response;            
            dispatch(userAsyncActions.pagedUsers.success(data));
            return data;
        }
        catch (e) {
            dispatch(userAsyncActions.pagedUsers.failure());
            throw new Error('нет связи с сервером');
        }
    };
};

export {pagedUsers};