import {EditUser} from "types/user";
import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {userAsyncActions} from "./actions";
import {createUser} from "services/user";
import {SubmissionError} from "redux-form";
import {goBack} from "connected-react-router";

/**
 * создание пользователя/
 * @param model
 */
const createAdminUser = (model: EditUser) => {
    return async (dispatch: RootDispatch) => {
        try{
            dispatch(userAsyncActions.createUser.request());

            await createUser(model);
            
            dispatch(userAsyncActions.createUser.success());
            dispatch(goBack());
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(userAsyncActions.createUser.failure());
            throw new SubmissionError(e);
        }
    };
};

export default createAdminUser; 