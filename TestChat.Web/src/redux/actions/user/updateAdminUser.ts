import {EditUser} from "types/user";
import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {SubmissionError} from "redux-form";
import {userAsyncActions} from "./index";
import {push} from "connected-react-router";
import Path from "types/constants/Path";
import {updateUserByAdmin} from "services/user";

/**
 * Обновление пользователя
 * @param model
 */
const updateAdminUser = (model: EditUser) => {
    return async (dispatch: RootDispatch) => {
        try {
            dispatch(userAsyncActions.updateAdminUser.request());

            await updateUserByAdmin(model);

            dispatch(userAsyncActions.updateAdminUser.success());
            dispatch(push(Path.Users));
        } catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(userAsyncActions.updateAdminUser.failure());
            throw new SubmissionError({_error: e});
        }
    };
};

export default updateAdminUser;