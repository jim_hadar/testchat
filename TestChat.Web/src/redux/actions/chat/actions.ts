import {createAction, createAsyncAction} from "typesafe-actions";
import {ChatPagingParams, PagedChatList} from "types/chat";
import User from "types/user";

const getChatById = createAsyncAction(
    'CHAT.GET_BY_ID.REQUEST',
    'CHAT.GET_BY_ID.SUCCESS',
    'CHAT.GET_BY_ID.FAILURE'
)<undefined, undefined, undefined>();

const getPagedChats = createAsyncAction(
    'CHAT.GET_PAGED.REQUEST',
    'CHAT.GET_PAGED.SUCCESS',
    'CHAT.GET_PAGED.FAILURE'
)<undefined, PagedChatList, undefined>();

const createChat = createAsyncAction(
    'CHAT.CREATE.REQUEST',
    'CHAT.CREATE.SUCCESS',
    'CHAT.CREATE.FAILURE'
)<undefined, undefined, undefined>();

const updateChat = createAsyncAction(
    'CHAT.UPDATE.REQUEST',
    'CHAT.UPDATE.SUCCESS',
    'CHAT.UPDATE.FAILURE'
)<undefined, undefined, undefined>();

const deleteChat = createAsyncAction(
    'CHAT.DELETE.REQUEST',
    'CHAT.DELETE.SUCCESS',
    'CHAT.UPDATE.FAILURE'
)<undefined, undefined, undefined>();

const allAvailableChatUsers = createAsyncAction(
    'CHAT.ALL_AVAILABLE_USERS.REQUEST',
    'CHAT.ALL_AVAILABLE_USERS.SUCCESS',
    'CHAT.ALL_AVAILABLE_USERS.FAILURE'
)<undefined, User[], undefined>();

const setChatFilter = createAction('CHAT.SET_PAGING_FILTER.SUCCESS')<ChatPagingParams>(); 

const chatAsyncActions = {
    getChatById,
    getPagedChats,
    createChat,
    updateChat,
    deleteChat,
    allAvailableChatUsers
};

const chatActions = {
    setChatFilter
};

export {
    chatActions,
    chatAsyncActions
}