import {ChatPagingParams} from "types/chat";
import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {chatAsyncActions} from "./index";
import {chatService} from "services";


const getPagedChats = (params: ChatPagingParams) => {
    return async (dispatch: RootDispatch) => {
        try {
            dispatch(chatAsyncActions.getPagedChats.request());
            const response = await chatService.getPagedChats(params);            
            const {data} = response;            
            dispatch(chatAsyncActions.getPagedChats.success(data));
            return data;
        }
        catch (e) {
            dispatch(chatAsyncActions.getPagedChats.failure());
            dispatchHandleError(e, dispatch);
            throw new Error(e.toString());
        }
    }
}

export default getPagedChats;