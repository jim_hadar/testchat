import {chatActions, chatAsyncActions} from "./actions";
import {ActionType} from "typesafe-actions";
import getPagedChats from "./getPagedChats";
import initializeUsersForChatForm from './initializeUsersForChatForm';
import createChat from "./createChat";
import deleteChat from "./deleteChat";
import editChat from './editChat';
import initializeEditChatForm from "./initializeEditChatForm";

type ChatAsyncActions = ActionType<typeof chatAsyncActions>;
type ChatActions = ActionType<typeof chatActions>;

export {
    chatActions, 
    chatAsyncActions,
    getPagedChats,
    initializeUsersForChatForm,
    createChat,
    deleteChat,
    editChat,
    initializeEditChatForm
};

export type { 
    ChatActions,
    ChatAsyncActions
};

