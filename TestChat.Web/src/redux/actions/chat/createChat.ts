import {chatService} from 'services';
import {CreateChatModel} from "types/chat";
import {RootDispatch} from "types/root";
import {dispatchHandleError, dispatchHandleSuccess} from "helpers/general";
import {chatAsyncActions} from "./index";

const createChat = (model: CreateChatModel) => {
    return async (dispatch: RootDispatch) => {
        try{
            dispatch(chatAsyncActions.createChat.request());

            await chatService.createChat(model);
            
            dispatch(chatAsyncActions.createChat.success());
            dispatchHandleSuccess('Чат успешно создан', dispatch);
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(chatAsyncActions.createChat.failure());
            throw new Error(e);
        }
    };
};

export default createChat;