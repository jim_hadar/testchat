import {RootDispatch} from "types/root";
import {dispatchHandleError} from "../../../helpers/general";
import {chatAsyncActions} from "./index";
import {goBack} from "connected-react-router";
import {chatService} from "services";
import {initialize} from "redux-form";
import FormName from "types/constants/FormName";

const getChatById = (id: number) => {
    return async (dispatch: RootDispatch) => {
        try {
            dispatch(chatAsyncActions.getChatById.request());            
            const response = await chatService.getChatById(id);           
            const {data} = response;            
            dispatch(chatAsyncActions.getChatById.success());     
            const formValues = {
                chatId: data.id,
                name: data.name,
                members: data.members.map(m => ({ label: m.name, value: m.id }))
            };
            dispatch(initialize(FormName.AddChat, formValues));
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(chatAsyncActions.getChatById.failure());
            dispatch(goBack());
        }
    }
}

export default getChatById;