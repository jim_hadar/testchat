import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {chatAsyncActions} from "./index";
import {userService} from "services";

const initializeUsersForChatForm = () => {
    return async (dispatch : RootDispatch) => {
        try{
            dispatch(chatAsyncActions.allAvailableChatUsers.request());

            const {data} = await userService.getPagedUsers({
                pageNumber: 1,
                pageSize: 1000000,
                filter: {
                    search: '',
                    order: "desc",
                    sort: "name"
                }
            });
            
            dispatch(chatAsyncActions.allAvailableChatUsers.success(data.data));
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(chatAsyncActions.allAvailableChatUsers.failure());
        }
    };
};

export default initializeUsersForChatForm;