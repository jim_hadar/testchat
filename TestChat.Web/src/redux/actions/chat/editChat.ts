import {UpdateChatModel} from "types/chat";
import {RootDispatch} from "types/root";
import {dispatchHandleError, dispatchHandleSuccess} from "helpers/general";
import {chatAsyncActions} from "./index";
import {chatService} from "services";
import {push} from "connected-react-router";
import Path from "types/constants/Path";

const editChat = (model: UpdateChatModel) => {
    return async (dispatch: RootDispatch) => {
        try{
            dispatch(chatAsyncActions.updateChat.request());
            await chatService.updateChat(model);
            dispatch(push(Path.Chats));
            dispatch(chatAsyncActions.updateChat.success());
            dispatchHandleSuccess('Чат успешно изменен', dispatch);
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(chatAsyncActions.updateChat.failure());
        }
    }
}

export default editChat;