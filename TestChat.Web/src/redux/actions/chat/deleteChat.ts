import {RootDispatch} from "types/root";
import {chatAsyncActions} from "./index";
import {chatService} from "services";
import * as statuses from 'http-status-codes';
import {dispatchHandleError, dispatchHandleSuccess} from "helpers/general";

const deleteChat = (id:number) => {
    return (dispatch: RootDispatch) => {
        dispatch(chatAsyncActions.deleteChat.request());
        return chatService.deleteChat(id)
            .then(r => {
                if(r.status === statuses.NO_CONTENT){
                    dispatchHandleSuccess('Чат успешно удален', dispatch);
                    return dispatch(chatAsyncActions.deleteChat.success());
                }
                else{
                    return dispatchHandleError(r.data.toString(), dispatch);
                }
            })
            .catch(e => {                
                dispatchHandleError(e, dispatch);
                return dispatch(chatAsyncActions.deleteChat.failure()); 
            });
    }
}

export default deleteChat;