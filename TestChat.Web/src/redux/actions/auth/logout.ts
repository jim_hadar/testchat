import {authActions} from "./index";
import {RootDispatch} from "types/root";
import appLocalStorage from "helpers/appLocalStorage";

const logout = () => {
    return async (dispatch: RootDispatch) => {
        dispatch(authActions.logout.success());
        appLocalStorage.setUser(null);
    };
};

export default logout;