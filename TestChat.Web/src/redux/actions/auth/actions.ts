import { createAsyncAction } from 'typesafe-actions';
// @ts-ignore

const login = createAsyncAction(
    'AUTH.LOGIN.REQUEST',
    'AUTH.LOGIN.SUCCESS',
    'AUTH.LOGIN.FAILURE'
)<undefined, any, undefined>();

const logout = createAsyncAction(
    'AUTH.LOGOUT.REQUEST',
    'AUTH.LOGOUT.SUCCESS',
    'AUTH.LOGOUT.FAILURE'
)<undefined, undefined, string>();

export{
    login,
    logout
}