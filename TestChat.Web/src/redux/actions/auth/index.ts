import { ActionType } from 'typesafe-actions';
import * as actions from './actions';
import login from './login';
import logout from "./logout";

export type Actions = ActionType<typeof actions>;

export {
    actions as authActions,
}

export {
    login,
    logout
}