import { authService } from 'services';
import {RootDispatch} from "types/root";
import {AuthCredentials} from "types/auth";
import HttpStatus from 'http-status-codes';
import { authActions } from "./index";
import appLocalStorage from "helpers/appLocalStorage";
import {getUserRole} from "helpers/general";

/**
 * взод пользователя и получени token 
 * @param data - данные авторизации
 */
const login = (data: AuthCredentials) => {
    return async (dispatch: RootDispatch) => {
        try {
            dispatch(authActions.login.request());
            const response = await authService.login(data);
            if(response.status === HttpStatus.OK){
                const { data } = response;
                appLocalStorage.setUser(data);
                const payload = {
                    userName: data.username,
                    userRole: getUserRole(),
                    userId: data.id
                };
                dispatch(authActions.login.success(payload));
                // TODO: сделать переход на главную страницу
            }
            else {
                throw new Error('При авторизации произошла ошибка');
            }
        }
        catch (e) {
            dispatch(authActions.login.failure());
        }
    };
};

export default login;