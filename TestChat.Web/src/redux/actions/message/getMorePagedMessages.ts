import {RootDispatch} from "types/root";
import { messageService } from 'services';
import {dispatchHandleError} from "helpers/general";
import {messageAsyncActions} from "./index";
import {MessagePagingParams} from "types/message";

const getMorePagedMessages = (isMore: boolean) => {
    return async (dispatch: RootDispatch, getState: Function) => {
        try{
            const currentChat = getState().message.currentChat;            
            if(!currentChat)
                return;
            
            dispatch(messageAsyncActions.getPagedMessages.request());
            
            if(isMore){
                
            }
            
            const query : MessagePagingParams = {
                pageNumber: 1,
                pageSize: 20,
                filter: {
                    search: '',
                    sort: 'dateCreate',
                    order: 'asc',
                    chatId: currentChat.id 
                }
            };
            
            const {data} = await messageService.getPagedMessages(query);
            
            dispatch(messageAsyncActions.getPagedMessages.success(data.data.reverse()));
        }
        catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(messageAsyncActions.getPagedMessages.failure());
        }
    }
}

export default getMorePagedMessages;