import {
    messageAsyncActions,
    messageActions
} from './actions';
import {ActionType} from "typesafe-actions";
import createMessage from './createMessage';
import getMorePagedMessages from "./getMorePagedMessages";

type MessageAsyncActions = ActionType<typeof messageAsyncActions>;
type MessageActions = ActionType<typeof messageActions>; 

export type {
    MessageAsyncActions,
    MessageActions
}

export {
    messageAsyncActions,
    messageActions,
    createMessage,
    getMorePagedMessages
}
