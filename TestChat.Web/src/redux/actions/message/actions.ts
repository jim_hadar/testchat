import {createAction, createAsyncAction} from "typesafe-actions";
import {Chat} from "types/chat";
import {MessageModel} from "types/message";

const createMessage = createAsyncAction(
    'MESSAGE.CREATE.REQUEST',
    'MESSAGE.CREATE.SUCCESS',
    'MESSAGE.CREATE.FAILURE'
)<undefined, undefined, undefined>();

const getPagedMessages = createAsyncAction(
    'MESSAGE.GET_PAGED.REQUEST',
    'MESSAGE.GET_PAGED.SUCCESS',
    'MESSAGE.GET_PAGED.FAILURE'
)<undefined, MessageModel[], undefined>()

const setCurrentChat = createAction('MESSAGE.SET_CURRENT.SUCCESS')<Chat | undefined>();

const pushNewMessage = createAction('MESSAGE.PUSH_NEW.SUCCESS')<MessageModel>();

const messageAsyncActions = {
    createMessage,
    getPagedMessages
}

const messageActions = {
    setCurrentChat,
    pushNewMessage
}

export {
    messageAsyncActions,
    messageActions
}