import {messageService} from 'services';
import {CreateMessageModel} from "types/message";
import {RootDispatch} from "types/root";
import {dispatchHandleError} from "helpers/general";
import {messageAsyncActions} from "./index";
import {initialize} from "redux-form";
import FormName from "types/constants/FormName";

const createMessage = (content: string) => {
    return async (dispatch: RootDispatch, getState: Function) => {
        try {
            dispatch(messageAsyncActions.createMessage.request());            
            
            const model: CreateMessageModel = {
                chatId: getState().message.currentChat.id,
                content
            };            
            const {data} = await messageService.createMessage(model);
            
            dispatch(messageAsyncActions.createMessage.success());
            dispatch(initialize(FormName.AddMessage, {
                content: ''
            }));
            
            return data;
        } catch (e) {
            dispatchHandleError(e, dispatch);
            dispatch(messageAsyncActions.createMessage.failure());
        }
    }
}

export default createMessage;