import {createAction} from "typesafe-actions";
import {SnackbarMessage} from "types/snackbarMessage";

/**
 * Action открытия snackbar
 */
const openBar = createAction("SNACKBAR.OPEN")<SnackbarMessage>();

/**
 * action закрытия snackbar
 */
const closeBar = createAction("SNACKBAR.CLOSE")<undefined>();

export{
    openBar,
    closeBar
}
