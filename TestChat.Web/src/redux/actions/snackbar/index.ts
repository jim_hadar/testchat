import * as actions from './actions';
import {ActionType} from "typesafe-actions";

export type SnackbarActions = ActionType<typeof actions>;

export {
    actions as snackbarActions
}