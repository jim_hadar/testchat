import {RootState} from "types/root";
import FormName from "types/constants/FormName";

/**
 * возвращает объект из формы redux-form
 * @param state - текущий state приложения
 * @param formName - название формы, для которой необходимо получить текущие значения
 * @returns значения формы или null
 */
function getReduxFormValues<T>(state: RootState, formName: FormName) : T | null {
    const form = state.form[formName];    
    if(form && form.values) {
        return form.values as T;
    }
    return null;
}

export default getReduxFormValues;