import { RootAction, RootState } from 'types/root';
import { getType } from 'typesafe-actions';

/**
 * Selectors
 */
const getIsLoading = (
    state: RootState,
    actionCreator: { request: () => RootAction },
) => {
    const type = getType(actionCreator.request);
    const [prefix, action] = type.split('.');

    return state.loader[`${prefix}.${action}`];
};

/**
 * Export
 */
export default {
    getIsLoading,
};
