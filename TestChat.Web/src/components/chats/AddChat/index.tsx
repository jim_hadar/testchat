import React, {FC, useEffect} from "react";
import classes from "./style.module.scss";
import {connect} from "react-redux";
import {UpdateChatModel} from 'types/chat';
import {reduxForm, InjectedFormProps} from "redux-form";
import {RootDispatch, RootState} from "types/root";
import AddChatForm from "types/constants/forms/AddChatForm";
import FormName from "types/constants/FormName";
import FormWrapper from "components/common/FormWrapper";
import loader from "redux/redux-form-selectors/loader";
import {
    chatAsyncActions,
    createChat,
    editChat,
    initializeEditChatForm,
    initializeUsersForChatForm
} from "redux/actions/chat";
import {required} from "helpers/validation";
import RFInputTextField from "components/common/Input/RFInputTextField";
import RFMultiSelectField from "components/common/Input/RFMultiselectField";
import {Option} from "react-multi-select-component/dist/lib/interfaces";
import {push} from "connected-react-router";
import Path from "types/constants/Path";
import {RouteComponentProps} from "react-router-dom";
import {IEditUserRouterProps} from "types/user";

const AddChat: FC<AddChatProps> = (
    {
        id,
        handleSubmit,
        submitting,
        pristine,
        valid,
        createChatLoading,
        updateChatLoading,
        users,
        initUsers,
        initForm
    }
) => {

    const isEditChat = id !== undefined;

    useEffect(() => {
        initUsers();
        if (isEditChat)
            initForm(id);
    }, [isEditChat, initUsers, id, initForm]);

    return (
        <FormWrapper
            onSubmit={handleSubmit}
            submitText={!isEditChat ? 'Создать чат' : 'Сохранить'}
            headerText={!isEditChat ? 'Создание чата' : 'Редактирование чата'}
            headerTextClassName='text-center'
            noSubmit={false}
            noCancel={true}
            disabledSubmit={submitting || (pristine && !isEditChat) || !valid || createChatLoading || updateChatLoading}
            submitHasProgress={createChatLoading || updateChatLoading}
        >
            <RFInputTextField
                label="Название чата"
                name={AddChatForm.Name}
                className={classes.text__field__max__width}
                validate={[required]}
            />
            <RFMultiSelectField
                label="Выберите участников чата"
                options={users}
                name={AddChatForm.Members}
                className={classes.text__field__max__width}
                disableSearch={false}
            />
        </FormWrapper>
    );
};

const AddChatReduxForm = reduxForm<ReduxFormProps, ReduxFormProps>({
    form: FormName.AddChat,
    initialValues: {
        chatId: 0,
        name: '',
        members: []
    },
    touchOnChange: true,
    onSubmit: (values, dispatch: RootDispatch) => {
        const {chatId, name, members} = values;
        const chatModel: UpdateChatModel = {
            chatId,
            name,
            members: members.map(m => m.value)
        };
        if (chatId > 0) {
            dispatch(editChat(chatModel))
                .then(() => dispatch(push(Path.Chats)))
                .catch(e => {
                });
        } else {
            dispatch(createChat(chatModel))
                .then(() => {
                    dispatch(push(Path.Chats))
                })
                .catch(e => {

                });
        }
    }
// @ts-ignore
})(AddChat);

// @ts-ignore
const mapStateToProps = (state: RootState, ownProps: RouteComponentProps<IEditUserRouterProps>) => {
    return {
        createChatLoading: loader.getIsLoading(state, chatAsyncActions.createChat),
        users: state.chat.allAvailableUsers.map(v => ({value: v.id, label: v.name})),
        id: ownProps.match.params.id,
        updateChatLoading: loader.getIsLoading(state, chatAsyncActions.updateChat)
    };
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        initUsers: () => dispatch(initializeUsersForChatForm()),
        initForm: (id: number) => dispatch(initializeEditChatForm(id)),
    };
};

type ReduxFormProps = {
    chatId: number,
    name: string,
    members: Option[],
};

type AddChatProps =
    InjectedFormProps<ReduxFormProps, {}> &
    ReturnType<typeof mapStateToProps> &
    ReturnType<typeof mapDispatchToProps>;

const ConnectedAddChatForm = connect(mapStateToProps, mapDispatchToProps)(AddChatReduxForm);

export default ConnectedAddChatForm;