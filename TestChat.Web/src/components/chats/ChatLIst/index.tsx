import React, {FC, useEffect, useState} from "react";
import {getCommonServerSideGetRows, initServerGridOptions} from "helpers/agGridHelper";
import defaultGridOptions from "types/constants/tables/gridOptions";
import {GridOptions, GridReadyEvent} from "ag-grid-community";
import FormHeader from "components/common/FormWrapper/FormHeader";
import {Col, Row} from "react-bootstrap";
import SearchInput from "components/common/Input/SearchInput";
import {AgGridReact} from "ag-grid-react";
import tableColumns from "types/constants/tables/tableColumns";
import ActionsCellRenderer from "components/common/Table/CustomCellRenders/ActionsCellRenderer";
import {RootDispatch, RootState} from "types/root";
import loader from "redux/redux-form-selectors/loader";
import {chatAsyncActions, getPagedChats} from "redux/actions/chat";
import {ChatPagingParams} from "types/chat";
import {connect} from "react-redux";
import ButtonWithSpinner from "components/common/ButtonWithSpinner";
import {push} from "connected-react-router";
import Path from "types/constants/Path";

type ChatListLocalState = {
    gridOptions: GridOptions,
    search?: string
};

const initialState: ChatListLocalState = {
    gridOptions: initServerGridOptions(defaultGridOptions)(),
    search: ''
};

const ChatList: FC<ConnectedProps> = (
    {
        load,
        isLoading,
        paging,
        addChat
    }
) => {
    const [{gridOptions, search}, setState] = useState(initialState);

    useEffect(() => {
        setState((s) => ({
            ...s,
            search: paging.filter?.search
        }));
    }, [paging.filter]);
    
    const refreshGrid = (searchValue: string) => {
        setState(s => ({
            ...s,
            search: searchValue
        }));
        gridOptions.api?.setServerSideDatasource(getCommonServerSideGetRows(load, {search: searchValue}));
    };

    const onGridReady = (event: GridReadyEvent) => {
        refreshGrid(search ?? '');
    };
    
    const onSearchChangeHandler = (value: string) => {
        refreshGrid(value);
    };
    
    return (
        <>
            <FormHeader
                headerText='Чаты пользователя'
                headerTextClassName='text-center'
            />
            <Row className="mb-4">
                <Col lg={8}>
                    <SearchInput
                        placeholder="Введите название чата для начала поиска"
                        value={search}
                        onSearch={onSearchChangeHandler}
                    />
                </Col>
                <Col lg={4}>
                    <ButtonWithSpinner
                        loading={false}
                        className="float-right"
                        text='Создать чат'
                        onClick={addChat}
                    />
                </Col>
            </Row>
            <div className="ag-theme-balham"
                 style={{height: '80%'}}
            >
                <AgGridReact
                    columnDefs={tableColumns.chatColumns}
                    gridOptions={gridOptions}
                    onGridReady={onGridReady}
                    frameworkComponents={{'actionsCellRenderer': ActionsCellRenderer}}
                >
                </AgGridReact>
            </div>
        </>
    );
}

const mapStateToProps = (state: RootState) => {
    return {
        rowsData: state.chat.pagingData,
        paging: state.chat.paging,
        isLoading: loader.getIsLoading(state, chatAsyncActions.getPagedChats)
    }
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        load: (params: ChatPagingParams) => dispatch(getPagedChats(params)),
        addChat: () => dispatch(push(Path.AddChat))
    };
};

type ConnectedProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

export default connect(mapStateToProps, mapDispatchToProps)(ChatList);