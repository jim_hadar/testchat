import React, {FC, PropsWithChildren} from "react";
import Message from './Message';
import {MessageModel} from "types/message";
import classes from 'components/messages/MessageChatList/style.module.scss';

const MessageList: FC<MessageListProps> = (
    {
        style,
        messages,
        children
    }
) => {
    
    const renderMessages = () => (
        messages.map((message) => {
            return (
                <Message
                    key={message.id}
                    {...message}
                    className={classes.message}
                />
            )
        })
    );
    
    return (
        <div
            style={style}
            className={classes.messageList}
        >
            <ul
                
            >
                {renderMessages()}
            </ul>
            {children}
        </div>
    );
}

type OwnProps = {
    messages: MessageModel[],
    style: React.CSSProperties
};

type MessageListProps = PropsWithChildren<OwnProps>;

export default MessageList;