import React, {FC} from "react";
import {MessageModel} from "types/message";
import moment from "moment";

const Message : FC<Props> = (
    {
        user,
        content,
        className,
        dateCreate
    }
) => {
    
    const date = moment(dateCreate);
    
    return (
        <li className={className}>
            <div>{date.format("DD-MM-YYYY hh:mm")} {user.name}</div>
            <div>{content}</div>
        </li>
    );
};

type Props = MessageModel & { 
    className?: string,    
};

export default Message;