import React, {FC, useState} from 'react';
import {RootDispatch, RootState} from "types/root";
import loader from "redux/redux-form-selectors/loader";
import {chatAsyncActions, getPagedChats} from "redux/actions/chat";
import {Chat, ChatPagingParams} from "types/chat";
import {connect} from "react-redux";
import {getCommonServerSideGetRows, initServerGridOptions} from "helpers/agGridHelper";
import {GridOptions, GridReadyEvent, RowSelectedEvent} from "ag-grid-community";
import defaultGridOptions from "types/constants/tables/gridOptions";
import {AgGridReact} from "ag-grid-react";
import tableColumns from "types/constants/tables/tableColumns";

const initialState: LocalState = {
    gridOptions: initServerGridOptions(defaultGridOptions)({
        rowSelection: 'single'
    }),
    search: ''
};

const LeftChatList: FC<ConnectedProps> = (
    {
        load,
        onChangeChat
    }
) => {
    const [{gridOptions, search}, setState] = useState(initialState);
    const refreshGrid = (searchValue: string) => {
        setState(s => ({...s, search: searchValue}));
        gridOptions.api?.setServerSideDatasource(getCommonServerSideGetRows(load, {search: searchValue}));
        onChangeChat(undefined);
        gridOptions.api?.sizeColumnsToFit();
    };
    const onGridReady = (event: GridReadyEvent) => {
        refreshGrid(search ?? '');
    };
    const onRowSelected = (event: RowSelectedEvent) => {
        if (event.api.getSelectedRows().length > 0) {
            onChangeChat(event.api.getSelectedRows()[0]);
        } else {
            onChangeChat(undefined);
        }
        event.api.sizeColumnsToFit();
    };
    return (
        <div className="ag-theme-balham"
             style={{height: '100%'}}
        >
            <AgGridReact
                columnDefs={tableColumns.chatForMessagesColumn}
                gridOptions={gridOptions}
                onGridReady={onGridReady}
                onRowSelected={onRowSelected}
            >
            </AgGridReact>
        </div>
    );
};

type LocalState = {
    gridOptions: GridOptions,
    search: string,
};

type LeftChatListProps = {
    onChangeChat: (chat?: Chat) => void,
};

const mapStateToProps = (state: RootState) => {
    return {
        rowsData: state.chat.pagingData,
        paging: state.chat.paging,
        isLoading: loader.getIsLoading(state, chatAsyncActions.getPagedChats)
    }
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        load: (params: ChatPagingParams) => dispatch(getPagedChats(params)),
    };
};

type ConnectedProps =
    LeftChatListProps &
    ReturnType<typeof mapStateToProps> &
    ReturnType<typeof mapDispatchToProps>;

export default connect(mapStateToProps, mapDispatchToProps)(LeftChatList);