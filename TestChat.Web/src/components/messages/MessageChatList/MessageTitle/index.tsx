import React, {FC} from "react";
import classes from 'components/messages/MessageChatList/style.module.scss';

const MessageTitle : FC<TitleProps> = (
    {
        title
    }
) => {
    
    const cls = [classes.title, 'bg-primary', 'rounded', 'text-white'];
    
    return (
        <div>
            <h4 
                className={cls.join(' ')}
            >
                {title}
            </h4>
        </div>        
    );
};

type TitleProps = {
    title: string
};

export default MessageTitle;