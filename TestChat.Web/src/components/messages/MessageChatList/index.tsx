import React, {FC, useEffect, useRef, useState} from 'react';
import {connect} from "react-redux";
import {RootDispatch, RootState} from "types/root";
import FormHeader from "components/common/FormWrapper/FormHeader";
import {Col, Row} from "react-bootstrap";
import {Chat} from "types/chat";
import LeftChatList from "./LeftChatList";
import MessageTitle from "./MessageTitle";
import MessageList from "./MessageList";
import RFInputTextField from "components/common/Input/RFInputTextField";
import classes from "./style.module.scss";
import AddMessageForm from "types/constants/forms/AddMessageForm";
import {Form, InjectedFormProps, reduxForm} from "redux-form";
import FormName from "types/constants/FormName";
import {createMessage, getMorePagedMessages, messageActions, messageAsyncActions} from "redux/actions/message";
import {required} from "helpers/validation";
import loader from "redux/redux-form-selectors/loader";
import {buildSignalRConnection} from "helpers/general";
import SignalrEvent from "types/constants/SignalrEvent";
import {HubConnection} from "@microsoft/signalr";
import {MessageModel} from "types/message";

const initialState: LocalState = {
    inputValue: '',
    hubConnection: buildSignalRConnection('hubs/chats')
};

const MessageChatList: FC<MessageChatListProps> = (
    {
        handleSubmit,
        selectedChat,
        setCurrentChat,
        messageIsSending,
        messages,
        getMessages,
        addMessageToChat
    }) => {
    const [{maxMessageListHeight, hubConnection}, setState] = useState(initialState);
    const chatIsSelect: boolean = selectedChat !== undefined;
    
    const chatsRef = useRef(null);    
    const messagesRef = useRef(null);    

    const scrollToBottom = (behavior: ("smooth" | "auto") = 'auto') => {
        if(messagesRef.current){
            // @ts-ignore
            messagesRef.current.scrollIntoView({ behavior: behavior});
        }
    };

    useEffect(() => {
        hubConnection.start().catch(err => {});
        hubConnection.on(SignalrEvent.CreateChatMessage, (data) => {
            addMessageToChat(data);
            scrollToBottom();
        });
    }, [hubConnection]);  

    const onChangeChat: ((chat?: Chat) => void) = (chat?: Chat) => {
        setCurrentChat(chat);
        if (chatsRef.current) {
            setState(s => ({
                ...s, maxMessageListHeight:
                // @ts-ignore    
                    chatsRef.current?.clientHeight
                        // @ts-ignore    
                        ? (chatsRef.current.clientHeight - 80)
                        : 0
            }));
        }
        
        getMessages().then(() => {
            scrollToBottom("auto");
        })
    };
    
    return (
        <>
            <FormHeader
                headerText='Сообщения'
                headerTextClassName='text-center'
            />
            <Row style={{height: '80%'}}>
                <Col xs={chatIsSelect ? 4 : 12} ref={chatsRef}>
                    <LeftChatList
                        onChangeChat={onChangeChat}
                    />
                </Col>
                {chatIsSelect && selectedChat &&
                <Col xs={8}>
                    <MessageTitle
                        title={selectedChat.name}
                    />
                    <MessageList
                        messages={messages}
                        style={{
                            height: maxMessageListHeight,
                            maxHeight: maxMessageListHeight
                        }}                     
                    >
                        <div ref={messagesRef}/>
                    </MessageList>
                    <Form
                        onSubmit={handleSubmit}
                    >
                        <RFInputTextField
                            label=''
                            name={AddMessageForm.Content}
                            placeHolder={'Начните вводить сообщение'}
                            className={classes.sendMessageForm}
                            validate={[required]}
                            disabled={messageIsSending}
                        />
                    </Form>
                </Col>
                }
            </Row>
        </>
    );
};

const ReduxForm = reduxForm<ReduxFormProps, {}>({
    form: FormName.AddMessage,
    initialValues: {
        content: ''
    },
    touchOnChange: true,
    forceUnregisterOnUnmount: true,
    onSubmit: (values, dispatch: RootDispatch) => {
        dispatch(createMessage(values.content));
    }
// @ts-ignore
})(MessageChatList);

type ReduxFormProps = {
    content: string
};

type LocalState = {
    inputValue: string,
    maxMessageListHeight?: number,
    hubConnection: HubConnection
};

const mapStateToProps = (state: RootState) => {
    return {
        selectedChat: state.message.currentChat,
        messageIsSending: loader.getIsLoading(state, messageAsyncActions.createMessage),
        messages: state.message.messages
    };
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        setCurrentChat: (chat?: Chat) => dispatch(messageActions.setCurrentChat(chat)),
        getMessages: () => dispatch(getMorePagedMessages(false)),
        addMessageToChat: (message: MessageModel) => dispatch(messageActions.pushNewMessage(message)),
    };
};

type MessageChatListProps =
    ReturnType<typeof mapStateToProps>
    & ReturnType<typeof mapDispatchToProps>
    & InjectedFormProps<ReduxFormProps, {}>;

export default connect(mapStateToProps, mapDispatchToProps)(ReduxForm);