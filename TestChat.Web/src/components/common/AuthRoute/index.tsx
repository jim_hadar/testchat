import React, {FC} from "react";
import {RootState} from "types/root";
import {connect} from 'react-redux';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import Layout, {LayoutProps} from "components/common/Layout";

const AuthRoute : FC<PropType> = ({
    isAuth,
    component: Component = () => null,
    mainLink ,
    links= [],
    ...rest
}) => {
    
    const renderComponent: RouteProps['render'] = (renderProps) => {
        if(isAuth){            
            return (
                // @ts-ignore
                <Layout
                    mainLink={mainLink}
                    links={links}
                    isAuthSt={isAuth} 
                >
                    <Component {...renderProps} {...rest}/>
                </Layout>
            );
        }  
        return (
            <Redirect to={{ pathname: '/login' }} />
        );
    };
    
    return (
        <Route 
            render={renderComponent}
            { ...rest }
        />
    );  
};

const mapStateToProps = (state: RootState) => {
    return {
        isAuth: state.auth.isAuth,
    }
};

type PropType = ReturnType<typeof mapStateToProps> & RouteProps & LayoutProps;

const connectedAuthRoute = connect(mapStateToProps)(AuthRoute);

export type AuthRoutePropType = PropType;

export default connectedAuthRoute;