import React, {FC} from 'react';
import {connect} from "react-redux";
import classes from "./snackbar.module.scss";
import {RootState} from "types/root";

const Snackbar: FC<ConnectedProps> = (
    {
        message,
        isOpen,
        type
    }
) => {
    const cls = [classes.snackbar];
    if(isOpen)
        cls.push(classes.show);
    else
        cls.push(classes.hidden);
    
    if(type === 'info')
        cls.push(classes.info);
    else
        cls.push(classes.error);
    
    return (
        isOpen ?
        <div className={cls.join(' ')}>{message}</div>
        : null    
    );
};

const mapStateToProps = (state: RootState) => {
    return {
        message: state.snackbar.message,
        isOpen: state.snackbar.isOpen,
        type: state.snackbar.type
    }
};

type ConnectedProps = ReturnType<typeof mapStateToProps>;

export default connect(mapStateToProps)(Snackbar);