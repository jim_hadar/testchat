import React, {FC, PropsWithChildren} from "react";
import {FormControlProps} from "react-bootstrap";
import {Form} from "react-bootstrap";

const SearchInput: FC<SearchInputProps> = (
    {
        label,
        onSearch,
        placeholder,
        ...rest
    }
) => {
    const controlId = `searchControl${Math.random()}`;
    return (
        <Form.Group controlId={controlId}>
            {label &&
                <Form.Label>{label}</Form.Label>
            }
            <Form.Control
                type="text"
                placeholder={placeholder}
                // @ts-ignore
                onChange={(e) => onSearch(e.target.value)}
                {...rest}
            />
        </Form.Group>
    );
};

type Props = {
    label?: string,
    onSearch: (value: string) => void,
    placeholder: string
};

type SearchInputProps = PropsWithChildren<FormControlProps & Props>;

export default SearchInput;