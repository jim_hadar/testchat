import React, {FC} from "react";
import classes from "./style.module.scss";

const RFErrorLabel: FC<LabelProps> = (
    {
        touched,
        error,
        warning
    }
) => {
    return (
        <>
            {
                touched &&
                ((error && <span className={classes.error}>{error}</span>)
                    ||
                    (warning && <span className={classes.error}>{warning}</span>))
            }
        </>
    );
};

type LabelProps = {
    touched: boolean,
    error: string,
    warning: string
}

export default RFErrorLabel;