import React, {FC} from "react";
import {Field, BaseFieldProps, WrappedFieldProps} from "redux-form";
import {Col, Form, FormControlProps} from "react-bootstrap";
import {SelectOption} from "types/general";
import RFErrorLabel from 'components/common/Input/RFErrorLabel';

const SelectField: FC<SelectFieldProps & WrappedFieldProps> = (
    {
        id,
        label,
        input,
        className,
        plaintext,
        disabled,
        visible = true,
        meta: {touched, error, warning},
        options,
        ...rest
    }
) => {
    const isValid = !touched ? undefined : touched && !error && !warning;
    const renderOptions = () => (
        options.map(option => (
            <option
                key={option.value}
                value={option.value}
            >
                {option.text}
            </option>
        ))
    );

    return (
        <>
            {visible
                ?
                <Form.Row>
                    <Form.Label column lg={3} className={className}>
                        {label}
                    </Form.Label>
                    <Col>
                        <Form.Control
                            size='sm'
                            as="select"
                            custom
                            id={id}
                            name={id}
                            className={className}
                            plaintext={plaintext}
                            disabled={disabled}
                            isInvalid={!isValid && isValid !== undefined}
                            {...input}
                        >
                            {renderOptions()}
                        </Form.Control>
                        <RFErrorLabel
                            touched={touched}
                            error={error}
                            warning={warning}
                        />
                    </Col>
                </Form.Row>
                : null}
        </>
    );
}

const RFSelectField: FC<RFSelectFieldProps> = (
    {
        name,
        ...rest
    }) => {
        return (
            <Field
                id={name}
                name={name}
                component={SelectField}
                {...rest}
            />
        );
    };

type OwnProps = {
    label: string,
    className?: string,
    visible?: boolean,
    options: SelectOption[]
};

type SelectFieldProps = FormControlProps & OwnProps;

type RFSelectFieldProps = SelectFieldProps & BaseFieldProps;

export default RFSelectField;