import React, {FC} from "react";
import {Field, BaseFieldProps, WrappedFieldProps} from "redux-form";
import {Col, Form, FormControlProps} from "react-bootstrap";
import MultiSelect from "react-multi-select-component";
import RFErrorLabel from "components/common/Input/RFErrorLabel";
import {Option} from "react-multi-select-component/dist/lib/interfaces";

const MultiSelectField: FC<MultiSelectFieldProps & WrappedFieldProps> = (
    {
        label,
        className,
        disabled,
        visible = true,
        meta: {touched, error, warning},
        options,
        disableSearch,
        isLoading,
        input,
    }
) => {    
    
    const filterOptions = (options: Option[], filter: string) => {
        filter = filter.toLowerCase();
        return options.filter(f => f.label.toLowerCase().includes(filter));
    };
    
    return (
        <>
            {visible
                ?
                <Form.Row>
                    <Form.Label column lg={3} >
                        {label}
                    </Form.Label>
                    <Col>
                        <MultiSelect
                            disabled={disabled}
                            className={className}
                            disableSearch={disableSearch}
                            isLoading={isLoading}
                            focusSearchOnOpen={true}
                            onChange={input.onChange}
                            options={options} 
                            value={input.value}
                            labelledBy={"Выберите"}
                            hasSelectAll={false}
                            overrideStrings={{
                                selectSomeItems: "Выберите",
                                selectAll: "Выбрать все",
                                search: "Поиск",
                                allItemsAreSelected: "Выбраны все значения"
                            }}
                            filterOptions={filterOptions}
                        />
                        <RFErrorLabel
                            touched={touched}
                            error={error}
                            warning={warning}
                        />
                    </Col>
                </Form.Row>
                : null}
        </>
    );
}

const RFMultiSelectField: FC<RFMultiSelectFieldProps> = (
    {
        name,
        ...rest
    }) => {
    return (
        <Field
            id={name}
            name={name}
            component={MultiSelectField}
            {...rest}
        />
    );
};

type OwnProps = {
    label: string,
    className?: string,
    visible?: boolean,
    options: Option[],
    disableSearch?: boolean,
    isLoading?: boolean,
};

type MultiSelectFieldProps = FormControlProps & OwnProps;

type RFMultiSelectFieldProps = MultiSelectFieldProps & BaseFieldProps;

export default RFMultiSelectField;