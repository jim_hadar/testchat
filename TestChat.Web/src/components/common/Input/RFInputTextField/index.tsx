import React, {FC} from "react";
import {Field, BaseFieldProps, WrappedFieldProps} from "redux-form";
import {Col, Form, FormControlProps} from "react-bootstrap";
import RFErrorLabel from 'components/common/Input/RFErrorLabel';

const InputTextField: FC<TextFieldProps & WrappedFieldProps> = (
    {
        id,
        label,
        input,
        className,
        plaintext,
        disabled,
        visible = true,
        meta: {touched, error, warning},
        type = 'text',
        placeHolder,
        ...rest
    }
) => {
    
    const isValid = !touched ? undefined : touched && !error && !warning;
    
    return (
        <>
            {visible
                ?
                <Form.Row>
                    {label &&
                        <Form.Label column lg={3} className={className}>
                            {label}
                        </Form.Label>
                    }
                    <Col>
                        <Form.Control size='sm'
                                      type={type}
                                      id={id}
                                      name={id}
                                      className={className}
                                      plaintext={plaintext}
                                      disabled={disabled}
                                      placeholder={placeHolder}
                                      isInvalid={!isValid && isValid !== undefined}
                                      {...input}
                        />
                        <RFErrorLabel 
                            touched={touched}
                            error={error}
                            warning={warning}
                        />
                    </Col>
                </Form.Row>
                : null}
        </>
    );
};

const RFInputTextField: FC<RFInputTextFieldProps> = (
    {
        name,
        ...rest
    }) => {

    return (
        <Field
            component={InputTextField}
            id={name}
            name={name}
            {...rest}
        >

        </Field>
    );
};

type OwnProps = {
    label: string,
    className?: string,
    visible?: boolean,
    placeHolder?: string
};

type TextFieldProps = FormControlProps & OwnProps;

export type RFInputTextFieldProps = TextFieldProps & BaseFieldProps;

export default RFInputTextField;