import React, {FC} from "react";
import {Col, Row} from "react-bootstrap";

const FormHeader: FC<FormHeaderProps> = (
    {
        headerText,
        headerTextClassName
    }) => {

    return (
        <Row>
            <Col lg={12}>
                <h2 className={headerTextClassName}>{headerText}</h2>
            </Col>
        </Row>
    );
};

type OwnProps = {
    headerText: string,
    headerTextClassName?: string
};

export type FormHeaderProps = OwnProps;

export default FormHeader;