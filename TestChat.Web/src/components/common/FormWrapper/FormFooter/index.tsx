import React, {FC, MouseEventHandler} from "react";
import {ButtonProps, Form} from "react-bootstrap";
import ButtonWithSpinner from "components/common/ButtonWithSpinner";
import {connect} from "react-redux";

const FormFooter: FC<FormFooterProps> = (
    {
        onReset,
        onCancel,
        onSubmit,
        onView,
        noReset = true,
        noCancel = true,
        noSubmit = true,
        noView = false,
        disabledReset = false,
        disabledCancel = false,
        disabledSubmit = false,
        resetText = '',
        cancelText = 'Отменить',
        submitText = 'Сохранить',
        viewText = 'Редактировать',
        submitHasProgress,
        submitType = 'button',
    }) => {
    return (
        <Form.Row className="float-left mt-4">
            {!noReset && resetText && onReset &&
            <ButtonWithSpinner loading={false}
                               text={resetText}
                               onClick={onReset}
                               type={'button'}
                               disabled={disabledReset}
                               className="mr-3"
            />
            }
            {!noSubmit && onSubmit &&
            <ButtonWithSpinner loading={submitHasProgress || false}
                               text={submitText}
                               onClick={onSubmit}
                               type={submitType}
                               disabled={disabledSubmit}
                               className="mr-3"
            />
            }
            {!noCancel && onCancel &&
            <ButtonWithSpinner loading={false}
                               text={cancelText}
                               onClick={onCancel}
                               type={'button'}
                               disabled={disabledCancel}
                               className="mr-3"
            />
            }
            {!noView && onView && noSubmit && noCancel &&
            <ButtonWithSpinner loading={false}
                               text={viewText}
                               onClick={onView}
                               type={'button'}
                               disabled={false}
            />
            }
        </Form.Row>
    );
};

export type FormFooterProps = {
    /** Root class name. */
    className?: string;

    /** A class name for the cancel button. */
    cancelClassName?: string;

    /** On reset button click handler. */
    onReset?: MouseEventHandler<HTMLButtonElement>;

    /** On cancel button click handler. */
    onCancel?: MouseEventHandler<HTMLButtonElement>;

    /** On submit button click handler. */
    onSubmit?: MouseEventHandler<HTMLButtonElement>;

    /** On view button click handler */
    onView?: MouseEventHandler<HTMLButtonElement>;

    /** If true, renders without a reset button. */
    noReset?: boolean;

    /** If true, renders without a cancel button. */
    noCancel?: boolean;

    /** If true, renders without a submit button. */
    noSubmit?: boolean;

    /** If true, renders without view button */
    noView?: boolean;

    /** If true, a reset button is disabled. */
    disabledReset?: boolean;

    /** If true, a cancel button is disabled. */
    disabledCancel?: boolean;

    /** If true, a submit button is disabled. */
    disabledSubmit?: boolean;

    /** Reset button text. */
    resetText?: string;

    /** Cancel button text. */
    cancelText?: string;

    /** Submit button text. */
    submitText?: string;

    /** View button text. */
    viewText?: string;

    /** If true, shows circular progress in the submit button. */
    submitHasProgress?: boolean;

    /** A submit button type. */
    submitType?: 'button' | 'submit';

    /** A variant of the cancel button. */
    cancelVariant?: ButtonProps['variant'];
};

const mapStateToProps = null;

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(FormFooter);