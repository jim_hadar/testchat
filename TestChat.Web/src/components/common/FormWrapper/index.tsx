import React, {FC, PropsWithChildren} from "react";
import {Form} from "react-bootstrap";
import FormFooter, {FormFooterProps} from "./FormFooter";
import FormHeader, {FormHeaderProps} from "./FormHeader";

const FormWrapper: FC<FormProps> = (
    {
        children,
        headerText,
        ...rest
    }) => {
    return (
        <Form>
            <FormHeader 
                headerText={headerText}
                {...rest}
            />
            {children}
            <FormFooter
                {...rest}
            />
        </Form>
    );
};

type OwnProps = {
    
};

type FormProps = PropsWithChildren<OwnProps> & FormFooterProps & FormHeaderProps;

export default FormWrapper;