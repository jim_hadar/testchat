import React, {FC, MouseEventHandler} from "react";
import {Button, Spinner} from "react-bootstrap";

const ButtonWithSpinner: FC<ButtonProps> = (
    {
        loading = false,
        type = 'button',
        color = 'primary',
        text,
        disabled = false,
        onClick,
        size,
        className
    }
) => {

    const renderSpinner = () => (
        loading
            ? <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
                className='mr-2'
            />
            : null
    );

    return (
        <>
            <Button variant={color}
                    disabled={disabled}
                    type={type}
                    className={className}
                    onClick={onClick || (() => console.log('unsupport'))}
                    size={size}
            >
                {renderSpinner()}
                {text}
            </Button>
        </>
    );
};

type ButtonProps = {
    loading: boolean,
    type?: 'button' | 'submit',
    color?: 'primary' | 'secondary' | 'success' | 'danger' | 'warning' | 'info',
    text: string,
    disabled?: boolean,
    onClick?: (() => void) | (MouseEventHandler<HTMLButtonElement>) | undefined,
    size?: 'sm' | 'lg',
    className?: string
};

export default ButtonWithSpinner;