import React, {FC} from "react";
import {Button, Modal, ModalProps} from "react-bootstrap";

const AgreeDialog: FC<OwnModalProps> = (
    {
        headerText,
        bodyText,
        onAgree,
        onCancel,
        ...rest
    }
) => {
    return (
        <Modal
            {...rest}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered 
            show={true}
            onHide={onCancel}
            animation={true}
        >
            {
                headerText &&
                <Modal.Header
                    closeButton
                >
                    <Modal.Title id="contained-modal-title-vcenter">
                        {headerText}
                    </Modal.Title>
                </Modal.Header>
            }
            <Modal.Body>
                <p>
                    {bodyText}
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button
                    variant="secondary"
                    onClick={onCancel}
                >
                    Нет
                </Button>
                <Button
                    variant="primary"
                    onClick={onAgree}
                >
                    Да
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

type OwnModalProps = ModalProps & {
    onAgree: (() => void),
    onCancel?: (() => void),
    bodyText: string,
    headerText?: string,    
};

export default AgreeDialog; 