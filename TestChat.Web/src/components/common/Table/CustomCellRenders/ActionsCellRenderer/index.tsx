import React, {FC, useState} from "react";
import {ICellRendererParams} from "ag-grid-community";
import {Button} from "react-bootstrap";
import AgreeDialog from "components/common/Dialogs/AgreeDialog";
import {ColDef} from "ag-grid-community/dist/lib/entities/colDef";

const initialState = {
    deleteDialogIsOpened: false
};

const ActionsCellRenderer: FC<IActionsCellRendererProps> = (
    {
        colDef,
        data,
        ...rest
    }
) => {

    const [{deleteDialogIsOpened}, setState] = useState(initialState);

    const {cellRendererParams} = colDef;

    const deleteVisible = cellRendererParams.deleteIsVisible === undefined ||
        cellRendererParams.deleteIsVisible(data);

    const editIsVisible = cellRendererParams.editIsVisible === undefined ||
        cellRendererParams.editIsVisible() === true;

    const onAgreeDeleteHandle = () => {
        setState((s) => ({...s, deleteDialogIsOpened: false}));
        cellRendererParams.onDelete?.call(null, {
            data,
            ...rest
        });
    }

    const onCancelDeleteHandler = () => {
        setState((s) => ({...s, deleteDialogIsOpened: false}));
    };

    return (
        <>
            <div className="float-right">
                {deleteDialogIsOpened &&
                <AgreeDialog
                    onAgree={onAgreeDeleteHandle}
                    onCancel={onCancelDeleteHandler}
                    bodyText={cellRendererParams.agreeDialogBodyText ?? ''}
                    headerText="Подтвердите удаление"
                />
                }
                {editIsVisible &&
                <Button
                    size='sm'
                    style={{height: 20, paddingTop: 0, paddingBottom: 0}}
                    onClick={() => cellRendererParams.onEdit?.call(null, data)}
                >
                    Редактировать
                </Button>
                }
                {deleteVisible &&
                <Button
                    size='sm'
                    style={{height: 20, paddingTop: 0, paddingBottom: 0}}
                    onClick={() => setState((s) => ({...s, deleteDialogIsOpened: true}))}
                    className="ml-2"
                >
                    Удалить
                </Button>
                }
            </div>
        </>
    );
};

export type ActionCellRendererParams = {
    onEdit?: ((params: any) => void),
    onDelete?: ((params: any) => void),
    agreeDialogBodyText?: string
};

export type ActionColDef = ColDef & {
    cellRendererParams: ActionCellRendererParams
};

export type IActionsCellRendererProps = ICellRendererParams & {
    colDef: ActionColDef
}

export default ActionsCellRenderer;