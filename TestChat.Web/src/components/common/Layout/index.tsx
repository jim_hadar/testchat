import React, {FC} from "react";
import {Container, Navbar, Nav, Button} from "react-bootstrap";
import {LayoutLink} from "types/layout";
import {NavLink} from "react-router-dom";
import {ChildrenComponentProps} from "types/helperForProps";
import {logout} from "redux/actions/auth";
import {RootDispatch} from "types/root";
import {connect} from "react-redux";

const Layout: FC<ConnectedProps> =
    ({
         mainLink,
         links,
         children,
         isAuthSt,
         logout,
         ...rest
     }) => {

        const linkComponents = links.map(link => (
            link.visibleInMenu
                ?
                <Nav.Item
                    key={link.name}
                >
                    <NavLink
                        to={link.ref}
                        className="nav-link"
                        {...rest}>
                        {link.text}
                    </NavLink>
                </Nav.Item>
                : null
        ));

        const handleLogout = () => {
            logout();
        };

        return (
            <>                
                <Container className="layout">
                    <Navbar
                        className="container layout-header navbar navbar-primary navbar-expand bg-primary"
                    >
                        <NavLink className="navbar-brand" activeClassName="activeL"
                                 to={mainLink.ref}>{mainLink.text}</NavLink>
                        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                {linkComponents}
                            </Nav>
                        </Navbar.Collapse>
                        {isAuthSt &&
                        <div className="nav navbar-nav navbar-right">
                            <Button onClick={handleLogout}>Выйти</Button>
                        </div>
                        }
                    </Navbar>
                    {children}
                    <div className="container layout-footer bg-primary">

                    </div>
                </Container>                
            </>
        );
    };

/**
 * Свойства для компонента layout:
 * Children + список линков
 */
export type LayoutProps = ChildrenComponentProps & {
    links: LayoutLink[],
    mainLink: LayoutLink,
    redirectLink?: LayoutLink,
    isAuthSt?: boolean,
}

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        logout: () => dispatch(logout())
    }
};

type ConnectedProps = LayoutProps & ReturnType<typeof mapDispatchToProps>;

export default connect(null, mapDispatchToProps)(Layout);