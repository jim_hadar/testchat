import React, {FC, useEffect, useState} from 'react';
import {connect} from "react-redux";
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import FormHeader from "components/common/FormWrapper/FormHeader";
import {RootDispatch, RootState} from "types/root";
import {UserPagingParams} from "types/user";
import {pagedUsers, userAsyncActions} from "redux/actions/user";
import loader from "redux/redux-form-selectors/loader";
import tableColumns from 'types/constants/tables/tableColumns';
import defaultGridOptions from "types/constants/tables/gridOptions";
import {getCommonServerSideGetRows, initServerGridOptions} from "helpers/agGridHelper";
import 'ag-grid-enterprise';
import {GridOptions, GridReadyEvent} from "ag-grid-community";
import SearchInput from "components/common/Input/SearchInput";
import ButtonWithSpinner from "components/common/ButtonWithSpinner";
import {Col, Row} from "react-bootstrap";
import {push} from "connected-react-router";
import Path from "types/constants/Path";
import ActionsCellRenderer from "components/common/Table/CustomCellRenders/ActionsCellRenderer";

type LocalState = {
    gridOptions: GridOptions,
    search?: string
};

const initialState: LocalState = {
    gridOptions: initServerGridOptions(defaultGridOptions)(),
    search: ''
};

const UserList: FC<ConnectedProps> = (
    {
        load,
        paging,
        addUser,
    }
) => {
    const [{gridOptions, search}, setState] = useState(initialState);

    const refreshGrid = (searchValue: string) => {
        setState(s => ({
            ...s,
            search: searchValue
        }));
        gridOptions.api?.setServerSideDatasource(getCommonServerSideGetRows(load, {search: searchValue}));
    };

    useEffect(() => {
        setState((s) => ({
            ...s,
            search: paging.filter?.search
        }));
    }, [paging.filter]);

    const onGridReady = (event: GridReadyEvent) => {
        refreshGrid(search ?? '');
    };

    const onSearchChangeHandler = (value: string) => {        
        refreshGrid(value);
    };

    return (
        <>            
            <FormHeader
                headerText='Пользователи'
                headerTextClassName='text-center'
            />
            <Row className="mb-4">
                <Col lg={8}>
                    <SearchInput
                        placeholder="Введите имя пользователя для начала поиска"
                        value={search}
                        onSearch={onSearchChangeHandler}
                    />
                </Col>
                <Col lg={4}>
                    <ButtonWithSpinner
                        loading={false}
                        className="float-right"
                        text='Создать пользователя'
                        onClick={addUser}
                    />
                </Col>
            </Row>
            <div className="ag-theme-balham"
                 style={{height: '80%'}}
            >
                <AgGridReact
                    columnDefs={tableColumns.userColumns}
                    gridOptions={gridOptions}
                    onGridReady={onGridReady}
                    frameworkComponents={{'actionsCellRenderer': ActionsCellRenderer}}
                >
                </AgGridReact>
            </div>
        </>
    );
};

const mapStateToProps = (state: RootState) => {
    return {
        rowsData: state.user.pagingData,
        paging: state.user.paging,
        isLoading: loader.getIsLoading(state, userAsyncActions.pagedUsers)
    }
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        load: (params: UserPagingParams) => dispatch(pagedUsers(params)),
        addUser: () => dispatch(push(Path.AddUser)),
    };
};

type ConnectedProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>;

export default connect(mapStateToProps, mapDispatchToProps)(UserList);