import React, {FC, useEffect} from "react";
import FormWrapper from "components/common/FormWrapper";
import {connect} from "react-redux";
import {RootDispatch, RootState} from "types/root";
import {getLocation, push} from "connected-react-router";
import {InjectedFormProps, reduxForm} from "redux-form";
import {EditUser, IEditUserRouterProps} from "types/user";
import FormName from "types/constants/FormName";
import RFInputTextField from "components/common/Input/RFInputTextField";
import AddUserForm from "types/constants/forms/AddUserForm";
import classes from "./style.module.scss";
import RFSelectField from "components/common/Input/RFSelectField";
import {onlyLatin, required} from "helpers/validation";
import {
    createAdminUser,
    initializeEditUserAdminForm,
    initializeUserRoles,
    updateAdminUser,
    userAsyncActions
} from "redux/actions/user";
import loader from "redux/redux-form-selectors/loader";
import {RouteComponentProps} from "react-router-dom";
import {getUserRole} from "helpers/general";
import Path from "types/constants/Path";

const AddUser: FC<AddUserProps> = (
    {
        handleSubmit,
        submitting,
        pristine,
        valid,
        loadRoles,
        mappedUserRoles,
        createUserLoading,
        id,
        initializeForm,
        toUsers
    }
) => {

    const isEditUser = id !== undefined;
    const requiredForPassword = !isEditUser ? required : ((str: string) => undefined);

    useEffect(() => {
        loadRoles();
        if (isEditUser)
            initializeForm(id);
    }, [loadRoles, isEditUser, initializeForm, id]);

    return (
        <FormWrapper
            onSubmit={handleSubmit}
            submitText={!isEditUser ? 'Создать пользователя' : 'Сохранить'}
            headerText={!isEditUser ? 'Создание пользователя' : 'Редактирование пользователя'}
            headerTextClassName='text-center'
            noSubmit={false}
            noCancel={false}
            onCancel={toUsers}
            disabledSubmit={submitting || (pristine && !isEditUser) || !valid || createUserLoading}
            submitHasProgress={createUserLoading}
        >
            <RFInputTextField
                label="Имя пользователя"
                name={AddUserForm.Name}
                className={classes.text__field__max__width}
                validate={[required]}
            />
            <RFInputTextField
                label="Логин"
                name={AddUserForm.Login}
                className={classes.text__field__max__width}
                validate={[required, onlyLatin]}
            />
            <RFInputTextField
                label="Пароль"
                type="password"
                name={AddUserForm.Password}
                className={classes.text__field__max__width}
                validate={[requiredForPassword]}
            />
            <RFSelectField
                name={AddUserForm.Role}
                label="Роль"
                options={mappedUserRoles}
                className={classes.text__field__max__width}
            />
        </FormWrapper>
    )
}

const ReduxAddUserForm = reduxForm<ReduxFormProps, {}>({
    form: FormName.AddUser,
    initialValues: {
        id: undefined,
        login: '',
        name: '',
        password: undefined,
        roleName: '',
        roleId: 1
    },
    touchOnChange: true,
    onSubmit: (values, dispatch: RootDispatch) => {
        values.roleId = Number(values.roleId)
        if (values.id === undefined) {
            dispatch(createAdminUser(values))
                .catch(e => {

                });
        } else {            
            dispatch(updateAdminUser(values))
                .catch(e => {

                });
        }
    }
// @ts-ignore
})(AddUser);

// @ts-ignore
const mapStateToProps = (state: RootState, ownProps: RouteComponentProps<IEditUserRouterProps>) => {
    return {
        location: getLocation(state),
        mappedUserRoles: state.user.userRoles.map(v => {
            return {value: v.id, text: v.name}
        }),
        createUserLoading: loader.getIsLoading(state, userAsyncActions.createUser),
        id: ownProps.match.params.id,
        userRole: getUserRole()
    }
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        loadRoles: () => dispatch(initializeUserRoles()),
        initializeForm: (id: number) => dispatch(initializeEditUserAdminForm(id)),
        toUsers: () => dispatch(push(Path.Users))
    }
};

type ReduxFormProps = EditUser;

type AddUserProps = ReturnType<typeof mapStateToProps>
    & ReturnType<typeof mapDispatchToProps>
    & InjectedFormProps<ReduxFormProps, {}>;

const ConnectedAddUserForm = connect(mapStateToProps, mapDispatchToProps)(ReduxAddUserForm);

// @ts-ignore
export default ConnectedAddUserForm;