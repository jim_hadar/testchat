import React, {FC, useEffect, useState} from "react";
import {RootDispatch, RootState} from "types/root";
import loader from "redux/redux-form-selectors/loader";
import {initializeProfileForm} from 'redux/actions/user';
import {reduxForm, InjectedFormProps} from "redux-form";
import FormName from "types/constants/FormName";
import {connect} from "react-redux";
import User from "types/user";
import {getUserId, getUserName} from "helpers/general";
import ProfileForm from "types/constants/forms/ProfileForm";
import getReduxFormValues from "redux/redux-form-selectors/generalFormSelector";
import RFInputTextField from "components/common/Input/RFInputTextField";
import FormWrapper from "components/common/FormWrapper";
import classes from "./style.module.scss";
import { required, onlyLatin, profilePassword } from "helpers/validation";
import saveProfile from "redux/actions/user/saveProfile";
import {userAsyncActions} from "redux/actions/user/actions";

const initialState = {
    view: true,
};

const Profile: FC<ProfileProps> = (
    {
        isLoading,
        isSaveLoading,
        children,
        load,
        formValues,
        handleSubmit,
        pristine,
        submitting,
        valid,
        ...rest
    }) => {

    const [{view}, setState] = useState(initialState);

    useEffect(() => {
        if (!isLoading) {
            load();
        }
    }, [isLoading, load]);

    const onViewHandler = () => {
        setState((prevState) => ({
            ...prevState,
            view: false
        }));
    };

    const onCancelHandler = () => {
        setState(prevState => ({
            ...prevState,
            view: true
        }));
    };

    return (
        <FormWrapper
            onSubmit={handleSubmit}
            headerText='Профиль пользователя'
            headerTextClassName='text-center'
            noView={!view}
            onView={onViewHandler}
            noCancel={view}
            onCancel={onCancelHandler}
            noSubmit={view}
            disabledSubmit={submitting || !valid || isSaveLoading}
            submitHasProgress={isSaveLoading}            
        >
            <RFInputTextField
                name={ProfileForm.Name}
                label={'Имя пользователя'}
                className={classes.text__field__max__width}
                disabled={view || isSaveLoading}
                validate={[required]}
            />
            <RFInputTextField
                name={ProfileForm.Login}
                label={'Логин'}
                className={classes.text__field__max__width}
                disabled={true}
                validate={[required , onlyLatin]}
            />
            <RFInputTextField
                name={ProfileForm.Password}
                label={'Пароль'}
                type="password"
                className={classes.text__field__max__width}
                disabled={view || isSaveLoading}
                visible={!view}
                validate={[profilePassword]}
            />
        </FormWrapper>
    );
};


const ReduxProfileForm = reduxForm<ReduxFormProps, {}>({
    form: FormName.Profile,
    initialValues: {
        id: getUserId(),
        login: getUserName(),
        name: getUserName(),
        password: '',
    },
    touchOnChange: true,
    onSubmit(values, dispatch) {
        dispatch(saveProfile(values));
    },
// @ts-ignore
})(Profile);

type ReduxFormProps = User;

const mapStateToProps = (state: RootState) => {
    return {
        isLoading: loader.getIsLoading(state, userAsyncActions.getUser),
        isSaveLoading: loader.getIsLoading(state, userAsyncActions.updateUser),
        formValues: getReduxFormValues<User>(state, FormName.Profile),        
    };
};

const mapDispatchToProps = (dispatch: RootDispatch) => {
    return {
        load: () => dispatch(initializeProfileForm()),
    }
};

type OwnProps = {};

export type ProfileProps =
    ReturnType<typeof mapStateToProps>
    & ReturnType<typeof mapDispatchToProps>
    & InjectedFormProps<ReduxFormProps, {}>
    & OwnProps;

const connectedForm = connect(mapStateToProps, mapDispatchToProps)(ReduxProfileForm);

export default connectedForm;