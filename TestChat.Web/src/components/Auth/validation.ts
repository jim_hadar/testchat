/**
 * Validation control description
 */
export type ControlDescription = {
    /**
     * label for control 
     */
    label: string,
    /**
     * error message for control
     */
    errorMessage: string,
    type: 'text' | 'password'
};

/**
 * Validation control definition
 */
export interface IValidationControl extends ControlDescription {
    valid: boolean,
    touched: boolean,
    value: string,
    validation: any,    
}

/**
 * Создание объекта, описывающего правила валидации контрола
 * @param {IValidationControl} controlDescription - описание контрола
 * @param {Object} validation - параметры валидации
 * @return {Object} - описание контрола валидации
 */
export function createControl(controlDescription: ControlDescription, validation: any = null) : IValidationControl {
    return {
        ...controlDescription,
        valid: !validation,
        touched: false,
        value: '',
        validation
    };
}

/**
 * Control для валидации
 * @param {IValidationControl} control - контрол для валидации
 * @return {Boolean} результат валидации
 */
export function validateControl(control: IValidationControl ) : Boolean{
    if(!control.validation)
        return true;
    const { validation, value } = control; 
    let isValid: Boolean = true;
    if(validation.required){
        isValid = !!value && value.trim() !== '' && isValid;
    }    
    return isValid;
}

/**
 * Валидация контролов формы
 * @param controls - объект, содержащий контролыдля валидации
 * @return {Boolean} результат валидации
 */
export function validateForm(controls: any): boolean{
    let isFormValid = true;
    
    for(let controlKey of Object.keys(controls)){
        if(controls.hasOwnProperty(controlKey)){            
            let control = controls[controlKey] as IValidationControl;
            if(!control)
                continue;
            // control.valid = validateControl(control) as boolean;            
            isFormValid = isFormValid && control.valid;
        }        
    }
    
    return isFormValid;
}