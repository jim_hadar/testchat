import React, {FC, useState} from "react";
import AuthInput from "./AuthInput";
import {createControl, IValidationControl, validateControl, validateForm} from "./validation";
import Enumerate from "types/enumerate";
import {Col, Row} from "react-bootstrap";
import classes from './auth.module.scss';
import {RootDispatch, RootState} from "types/root";
import {AuthCredentials} from "types/auth";
import * as authActions from "redux/actions/auth";
import {connect} from "react-redux";
import ButtonWithSpinner from "components/common/ButtonWithSpinner";

const authControls = (): Enumerate<IValidationControl> => ({
    login: createControl({
        label: 'Логин',
        errorMessage: 'Поле обязательно к заполнению',
        type: 'text'
    }, {
        required: true
    }),
    password: createControl({
        label: 'Пароль',
        errorMessage: 'Поле обязательно к заполнению',
        type: 'password'
    }, {
        required: true
    })
});

const Auth: FC<FormProps> = ({ login, loading }) => {
    
    const [controls, setControls] = useState(authControls());

    const changeControlHandle = (value: string, inputName: string): void => {
        setControls(prevState => {
            const control = {...prevState[inputName]};
            control.value = value;
            control.touched = true;
            control.valid = validateControl(control) as boolean;
            const stateControls = {...prevState};
            stateControls[inputName] = control;
            return stateControls;
        });
    };

    const clickHandler = () => {
        login({ userName: controls.login.value, password: controls.password.value });
    };

    let formValid = validateForm(controls);

    return (
        <>
            <Row>
                <Col
                    xs={{span: 12}}
                    sm={{span: 8, offset: 2}}
                    md={{span: 6, offset: 3}}
                    lg={{span: 4, offset: 4}}
                    xl={{span: 4, offset: 4}}
                    className={"align-items-center"}
                >
                    <div className={classes.Auth}>
                        <h4>Вход</h4>
                        <hr/>
                        {Object.keys(controls).map((controlName) => {
                            const control: IValidationControl = controls[controlName];
                            return (<AuthInput
                                key={controlName}
                                label={control.label}
                                onChange={(val) => changeControlHandle(val, controlName)}
                                value={control.value}
                                valid={control.valid}
                                touched={control.touched}
                                inputType={control.type}
                            />)
                        })}
                        <ButtonWithSpinner 
                            loading={loading}
                            text='Войти в систему'
                            disabled={!formValid || loading}
                            color='primary'
                            onClick={clickHandler}
                        />
                    </div>
                </Col>
            </Row>
        </>
    );
};

const mstp = (state : RootState) => ({
    loading: state.auth.isLoading
});

const mdtp = (dispatch: RootDispatch) => {
    return {
        login: (data: AuthCredentials) => dispatch(authActions.login(data))
    }
};

type FormProps = ReturnType<typeof mstp> & ReturnType<typeof mdtp>

export default connect(mstp, mdtp)(Auth);