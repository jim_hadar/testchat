import * as React  from "react";
import {Form} from "react-bootstrap";
import classes from './AuthInput.module.scss';
import {ChangeEvent} from "react";

export type AuthInputPropTypes = {
    shouldValidate?: boolean,
    valid?: boolean,
    touched?: boolean,
    value?: string,
    inputType?: 'text' | 'password',
    label: string,
    errorMessage?: string,
    onChange: (value: string) => void
};

const isInValid = (valid: boolean, touched: boolean, shouldValidate: boolean): boolean => {
    return !valid && shouldValidate && touched;
};

export const AuthInput: React.FC<AuthInputPropTypes> = (
    {
        valid= false,
        touched= false,
        value,
        inputType= "text",
        label,
        shouldValidate = true,
        errorMessage = 'Поле обязательно к заполнению',
        onChange
    }) => {
    
    const id = `${inputType}--${Math.random()}`;
    
    const inValid: boolean = isInValid(valid, touched, shouldValidate);
    
    let cls = [];
    
    if (inValid)
        cls.push('is-invalid');
    
    return (
        <Form.Group>
            <Form.Label htmlFor={id}>
                {label}
            </Form.Label>
            <Form.Control
                type={inputType}
                value={value}
                id={id}                
                size="sm"
                className={cls.join(' ')}
                onChange={(e: ChangeEvent<HTMLInputElement>) => onChange(e.target.value)} 
            />
            {
                inValid
                    ? <span className={classes.AuthInput}>{errorMessage}</span>
                    : null
            }
        </Form.Group>
    )
};

export default AuthInput;