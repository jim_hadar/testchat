import React, { FC } from 'react';
import {history} from 'helpers/history';
import { Router, Switch, Redirect, Route } from 'react-router-dom';
import {connect} from "react-redux";
import Path from "types/constants/Path";
import AuthPage from "pages/Auth";

const UnAuthRouter : FC<{}> = (props) => {
    return (
        <Router history={history}>
            <Switch>
                <Route 
                    path={Path.Login}
                    component={AuthPage}
                    exact
                />
                <Redirect to={Path.Login} />
            </Switch>
        </Router>
    );
};

export default connect(null, null)(UnAuthRouter);