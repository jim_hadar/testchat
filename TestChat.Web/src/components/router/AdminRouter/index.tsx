import React, { FC } from 'react';
import AuthRoute from 'components/common/AuthRoute';
import {getAuthAdminLinks} from "helpers/manageLinks";
import {Router, Switch, Redirect} from 'react-router-dom';
import {history} from 'helpers/history';
import {connect} from "react-redux"; 

const AdminRouter : FC<{}> = (props) => {
    const {mainLink, links, redirectLink} = getAuthAdminLinks();
    
    const renderAuthRoutes = links.map((link) => {
        return (
            <AuthRoute
                key={link.name}
                mainLink={mainLink}
                links={links}
                component={link.component}
                path={link.ref}
                exact={link.exact}
            />
        );
    });
    
    return (
        <Router history={history}>
            <Switch>
                {renderAuthRoutes}
                {
                    redirectLink 
                        ? <Redirect to={redirectLink.ref}/>
                        : null
                }
            </Switch>
        </Router>
    );
};

export default connect()(AdminRouter);