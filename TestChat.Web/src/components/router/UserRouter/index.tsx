import React, { FC } from 'react';
import {history} from 'helpers/history';
import { Router, Switch, Redirect } from 'react-router-dom';
import {connect} from "react-redux";
import {getAuthUserLinks} from "helpers/manageLinks";
import AuthRoute from "components/common/AuthRoute";

const UnAuthRouter : FC<{}> = (props) => {
    const {mainLink, links, redirectLink} = getAuthUserLinks();

    const renderAuthRoutes = links.map((link) => {
        return (
            <AuthRoute
                key={link.name}
                mainLink={mainLink}
                links={links}
                component={link.component}
                path={link.ref}
                exact={link.exact}
            />
        );
    });

    return (
        <Router history={history}>
            <Switch>
                {renderAuthRoutes}
                {
                    redirectLink
                        ? <Redirect to={redirectLink.ref}/>
                        : null
                }
            </Switch>
        </Router>
    );
};

export default connect(null, null)(UnAuthRouter);