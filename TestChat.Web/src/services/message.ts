import axios from "helpers/axiosInstance";
import Api from "types/constants/Api";
import {CreateMessageModel, MessageModel, MessagePagingParams, PagedMessageList} from "types/message";

const createMessage = (model: CreateMessageModel) => {
    return axios.post<MessageModel>(Api.Messages, model);
}

const getPagedMessages = (filter: MessagePagingParams) => {
    return axios.get<PagedMessageList>(Api.Messages, {
        params: filter
    });
};

export {
    createMessage,
    getPagedMessages
}
