import User, {EditUser, UserPagingParams, UserRole} from "types/user";
import axios from "helpers/axiosInstance";
import Api from "types/constants/Api";

/**
 * Получение информации о пользователе (только для роли обычного пользователя)
 * @param id идентификатор пользователя
 * @returns Promise, который содержит информацию о пользователе с указанным идентификатором или ошибку
 */
const getUser = (id: number) => {
    const url = `${Api.User}${id}`;
    return axios.get<User>(url);
};

/**
 * Обновление информации профиля текущего пользователя
 * @param model
 */
const updateUser = (model : User) => {
    const url = Api.User;
    return axios.put(url, model);
};

/**
 * Получение пользователей с постаничной разбивкой
 * @param data
 */
const getPagedUsers = (data: UserPagingParams) => {
    const url = Api.User;
    return axios.get(url, {params: data});
};

/**
 * Получение ролей пользователей
 */
const getUserRoles = () => {
    return axios.get<UserRole[]>(Api.UserRoles);
}

/**
 * Создание пользователя
 * @param model - модель пользователя 
 */
const createUser = (model: EditUser) => {
    return axios.post<EditUser>(Api.AdminUser, model);
}

/**
 * Обновление пользователя администратором
 * @param model
 */
const updateUserByAdmin = (model: EditUser) => {
    return axios.put<EditUser>(Api.AdminUser, model);
}

/**
 * Получение информации о пользователе по его идентификатору
 * @param id
 */
const getAdminUserById = (id: number) => {
    const url = `${Api.AdminUser}/${id}`;
    return axios.get<EditUser>(url);
}

/**
 * Удаление пользователя
 * @param id
 */
const deleteUser = (id: number) => {
    const url = `${Api.AdminUser}/${id}`;
    return axios.delete<''>(url);
};

export {
    getUser,
    updateUser,
    getPagedUsers,
    getUserRoles,
    createUser,
    updateUserByAdmin,
    getAdminUserById,
    deleteUser
};