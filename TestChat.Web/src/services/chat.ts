import { 
    Chat, 
    CreateChatModel,
    UpdateChatModel,
    PagedChatList,
    ChatPagingParams
} from 'types/chat';

import axios from 'helpers/axiosInstance';
import Api from "types/constants/Api";

/**
 * Получение чата по его идентификатору
 * @param chatId - идентификатор чата
 */
const getChatById = (chatId: number) => {
    return axios.get<Chat>(`${Api.Chats}/${chatId}`); 
};

/**
 * Создание чата
 * @param model - модель чата для создания
 */
const createChat = (model: CreateChatModel) => {
    return axios.post<Chat>(Api.Chats, model);
};

/**
 * Обновление чата
 * @param model - модель чата для обновления
 */
const updateChat = (model: UpdateChatModel) => {
    return axios.put<Chat>(Api.Chats, model);
};

/**
 * Удаление чата по его идентификатору
 * @param chatId - идентификатор чата для удаления
 */
const deleteChat = (chatId: number) => {
    return axios.delete<''>(`${Api.Chats}/${chatId}`);
}

/**
 * Получение списка чатов с постраничной разбивкой и фильтрацией.
 * @param pagingParams - параметры постраничной разбивки и фильтрации
 */
const getPagedChats = (pagingParams: ChatPagingParams) => {
    return axios.get<PagedChatList>(Api.Chats, {
        params: pagingParams
    });
}

export {
    getChatById,
    createChat,
    updateChat,
    deleteChat,
    getPagedChats
}