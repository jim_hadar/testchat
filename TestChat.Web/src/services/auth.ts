import {AuthCredentials, AuthSuccessResponse} from "types/auth";

import axios from "helpers/axiosInstance";
import Api from "types/constants/Api";

/**
 * отправка запроса на сервер на авторизацию
 * @param credentials - параметры авторизации
 * @returns Promise - промис с результатом запроса
 */
const login = (credentials: AuthCredentials) => {
    return axios.post<AuthSuccessResponse>(Api.Auth, credentials);
};

export {
    login
}