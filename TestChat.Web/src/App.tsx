import React, {FC} from 'react';
import {RootState} from "types/root";
import {connect} from 'react-redux';
import UnAuthRouter from "components/router/UnAuthRouter";
import AccountRole from "types/constants/AccountRole";
import AuthAdminRouter from 'components/router/AdminRouter';
import UserRouter from 'components/router/UserRouter';

const App: FC<AppProps> = (props) => {
    const { userRole } = props;
    
    switch (userRole) {
        case AccountRole.Administrator:
            return (
                <AuthAdminRouter />
            );
        case AccountRole.User:
            return (
                <UserRouter />
            );
        default:
            return (
                <UnAuthRouter/>
            );
    }
};

/**
 * Connected props.
 */
const mstp = (state: RootState) => ({
    isAuth: state.auth.isAuth,
    userRole: state.auth.userRole
});
const mdtp = {};

type AppProps = ReturnType<typeof mstp & typeof mdtp>;

export default connect(mstp, mdtp)(App);
