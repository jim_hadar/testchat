﻿const { craco } = require('@craco/craco');

module.exports = {
    webpack: {
        alias: {
            // Add the aliases for all the top-level folders in the `src/` folder.
            assets: `${paths.appSrc}/assets/`,
            components: `${paths.appSrc}/components/`,
            interfaces: `${paths.appSrc}/interfaces/`,
            modules: `${paths.appSrc}/modules/`,
            utils: `${paths.appSrc}/utils/`,
            // Another example for using a wildcard character
            '~': `${paths.appSrc}/`
        }
    },
    jest: {
        configure: {
            moduleNameMapper: {
                // Jest module mapper which will detect our absolute imports.
                '^assets(.*)$': '<rootDir>/src/assets$1',
                '^components(.*)$': '<rootDir>/src/components$1',
                '^interfaces(.*)$': '<rootDir>/src/interfaces$1',
                '^modules(.*)$': '<rootDir>/src/modules$1',
                '^utils(.*)$': '<rootDir>/src/utils$1',

                // Another example for using a wildcard character
                '^~(.*)$': '<rootDir>/src$1'
            }
        }
    }
};