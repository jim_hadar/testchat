﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using TestChat.Business.Models.Messages;
using TestChat.Business.SignalrHubs;

namespace TestChat.SignalrConsoleClient
{
    class Program
    {
        private static HubConnection connection;
        static void Main(string[] args)
        {
            string token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhZG1pbmlzdHJhdG9yIiwianRpIjoiYTU5YTI2NjctNzdlZi00NWFmLTkyMDctY2FmNmM3ZDIxYzA4IiwiaWF0IjoxNTg3ODU4NzAwLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJhZG1pbmlzdHJhdG9yIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6ImFkbWluaXN0cmF0b3IiLCJuYmYiOjE1ODc4NTg3MDAsImV4cCI6MTU4Nzg2NTkwMCwiaXNzIjoid2ViQXBpIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAwIn0.tstnPd4yTrRvT9QVKiTQv8SlF2vuXFlsPLFxkGQVL90";
            connection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/hubs/chats", opt =>
                    {
                        opt.AccessTokenProvider = () => Task.FromResult(token);
                    })
                .WithAutomaticReconnect()
                .Build();
            
            
            connection.On<MessageModel>(ChatHub.CreateChatMessage, (obj) =>
            {
                
            });
            
            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0,5) * 1000);
                await connection.StartAsync();
            };
            
            try
            {
                connection.StartAsync().Wait();
            }
            catch (Exception ex)
            {
                
            }
            
            Console.ReadKey();
        }
        
        
    }
}