namespace TestChat.Data.Common
{
    public static class UserType
    {
        public const string Administrator = "administrator";
        public const string User = "user";
    }
}