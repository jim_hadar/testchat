using Framework.DAL.EF.Concrete;
using Framework.Wrappers;
using Microsoft.EntityFrameworkCore;
using TestChat.Data.Contexts;

namespace TestChat.Data.Common
{
    public class ChatUnitOfWork : UnitOfWorkEf
    {
        public ChatUnitOfWork(ChatContext context, ICacheWrapper cache) 
            : base(context, cache)
        {
        }
    }
}