using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Framework.DAL.Attributes;
using Framework.DAL.Models;
using TestChat.Data.Common;

#nullable disable
namespace TestChat.Data.Models
{
    /// <summary>
    /// Пользователь
    /// </summary>
    [Table("users")]
    public class User : BaseObject
    {
        /// <summary>
        /// Имя пользователя.
        /// </summary>
        [FullTextSearchProperty]
        public string Name { get; set; }

        /// <summary>
        /// Логин пользователя.
        /// </summary>
        [FullTextSearchProperty]
        public string Login { get; set; }

        /// <summary>
        /// Пароль пользователя. 
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Роль пользователя. 
        /// </summary>
        public long RoleId { get; set; }

        /// <summary>
        /// Является ли пользователь администратором
        /// </summary>
        [NotMapped]
        public bool IsAdmin => Role != null && Role.Name == UserType.Administrator;

        #region [ Navigation ]

        /// <summary>
        /// Роль пользователя.
        /// </summary>
        [ForeignKey(nameof(RoleId))]
        public virtual UserRole Role { get; set; }
        
        /// <summary>
        /// Чаты пользователя
        /// </summary>
        public virtual ICollection<ChatUser> Chats { get; set; }
            = new List<ChatUser>();
        
        /// <summary>
        /// Все сообщения пользователя
        /// </summary>
        public virtual ICollection<ChatMessage> Messages { get; set; }
            = new List<ChatMessage>();

        #endregion
    }
}