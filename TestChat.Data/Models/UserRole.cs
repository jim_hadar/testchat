using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Framework.DAL.Models;

#nullable disable
namespace TestChat.Data.Models
{
    /// <summary>
    /// Роли пользователя
    /// </summary>
    [Table("user_roles")]
    public class UserRole : BaseObject
    {
        /// <summary>
        /// Название роли
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отрбражаемое название роли
        /// </summary>
        public string DisplayName { get; set; }

        #region [ Navigation ]
        
        /// <summary>
        /// Пользователи, связанные с ролью
        /// </summary>
        public virtual ICollection<User> Users { get; set; }
            = new List<User>();

        #endregion
    }
}