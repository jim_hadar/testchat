using System;
using System.ComponentModel.DataAnnotations.Schema;
using Framework.DAL.Models;

namespace TestChat.Data.Models
{
    /// <summary>
    /// Логи
    /// </summary>
    [Table("log_entries")]
    public class LogEntry : BaseObject
    {
        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// Уровень логов
        /// </summary>
        public string Level { get; set; }
        
        /// <summary>
        /// Источник лога
        /// </summary>
        public string Source { get; set; }
        
        /// <summary>
        /// Сообщение исключения
        /// </summary>
        public string Exception { get; set; }
        
        /// <summary>
        /// Stacktrace 
        /// </summary>
        public string Stacktrace { get; set; }
        
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime Date { get; set; }
    }
}