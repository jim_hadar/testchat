using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable
namespace TestChat.Data.Models
{
    /// <summary>
    /// Связь чатов и привязанных к чатам пользователей
    /// </summary>
    [Table("chat_users")]
    public class ChatUser
    {
        /// <summary>
        /// Идентификатор чата
        /// </summary>
        public long ChatId { get; set; }
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        #region [ Navigation ]

        /// <summary>
        /// Чат
        /// </summary>
        [ForeignKey(nameof(ChatId))]
        public virtual Chat Chat { get; set; }
        
        /// <summary>
        /// Пользователь
        /// </summary>
        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }

        #endregion
        
    }
}