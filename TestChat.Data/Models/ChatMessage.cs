using System;
using System.ComponentModel.DataAnnotations.Schema;
using Framework.DAL.Attributes;
using Framework.DAL.Models;

#nullable disable
namespace TestChat.Data.Models
{
    /// <summary>
    /// Сообщения чата. 
    /// </summary>
    [Table("chat_messages")]
    public class ChatMessage : BaseObject
    {
        /// <summary>
        /// Идентификато чата, к которому привязано сообщение
        /// </summary>
        public long ChatId { get; set; }
        
        /// <summary>
        /// Идентификатор пользователя, отправившего сообщение
        /// </summary>
        public long UserId { get; set; }
        
        /// <summary>
        /// Дата создания сообщения
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Содержимое сообщения
        /// </summary>
        [FullTextSearchProperty]
        public string Content { get; set; }
        
        /// <summary>
        /// Прочитано ли сообщение
        /// </summary>
        public bool IsRead { get; set; }

        #region [ Navigation ]

        /// <summary>
        /// Чат, в котором было отправлено сообщение
        /// </summary>
        [ForeignKey(nameof(ChatId))]
        public virtual Chat Chat { get; set; }

        /// <summary>
        /// Пользователь, отправивший сообщение
        /// </summary>
        [ForeignKey(nameof(UserId))]
        public virtual User User { get; set; }
        
        #endregion
    }
}