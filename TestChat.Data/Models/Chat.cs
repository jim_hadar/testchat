using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Framework.DAL.Models;

#nullable disable
namespace TestChat.Data.Models
{
    /// <summary>
    /// Чаты.
    /// </summary>
    [Table("chats")]
    public class Chat : BaseObject
    {
        /// <summary>
        /// Название чата
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Создатель чата
        /// </summary>
        public long CreateUserId { get; set; }
        
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime DateCreate { get; set; }

        #region [ Navigation ]

        /// <summary>
        /// Создатель чата
        /// </summary>
        [ForeignKey(nameof(CreateUserId))]
        public virtual User CreateUser { get; set; }
        
        /// <summary>
        /// Входящие в состав чата пользователи
        /// </summary>
        public virtual ICollection<ChatUser> Users { get; set; }
            = new List<ChatUser>();
        
        /// <summary>
        /// Сообщения чата
        /// </summary>
        public virtual ICollection<ChatMessage> Messages { get; set; }
            = new List<ChatMessage>();

        #endregion
    }
}