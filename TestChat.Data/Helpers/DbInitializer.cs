using System;
using System.Linq;
using System.Threading.Tasks;
using Framework;
using Framework.DAL.EF.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestChat.Data.Common;
using TestChat.Data.Contexts;
using TestChat.Data.Models;

namespace TestChat.Data.Helpers
{
    public static class DbInitializer
    {
        public static async Task InitializeDb(this IServiceProvider serviceProvider)
        {
            await serviceProvider.MigrateAsync();
            await serviceProvider.InitializeRoles();
            await serviceProvider.InitializeDefaultUser();
        }

        private static async Task InitializeRoles(this IServiceProvider serviceProvider)
        {
            try
            {
                using var scope = serviceProvider.CreateScope();
                var scopedServiceProvider = scope.ServiceProvider;
                var ufw = scopedServiceProvider.GetRequiredService<IUnitOfWork>();
                var roleRepo = ufw.Repository<UserRole>();
                if (!roleRepo.Query.Any())
                {
                    var adminRole = new UserRole
                    {
                        DisplayName = "Администратор",
                        Name = UserType.Administrator,
                    };
                    await roleRepo.CreateAsync(adminRole);
                    var userRole = new UserRole
                    {
                        DisplayName = "Пользователь",
                        Name = UserType.User
                    };
                    await roleRepo.CreateAsync(userRole);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static async Task InitializeDefaultUser(this IServiceProvider serviceProvider)
        {
            try
            {
                using var scope = serviceProvider.CreateScope();
                var scopedServiceProvider = scope.ServiceProvider;
                var ufw = scopedServiceProvider.GetRequiredService<IUnitOfWork>();
                var userRepo = ufw.Repository<User>();
                var passwordHasher = scopedServiceProvider.GetRequiredService<PasswordCryptographer>();
                if (!userRepo.Query.Any())
                {
                    var adminRole = await ufw.Repository<UserRole>().FirstOrDefaultAsync(_ => _.Name == UserType.Administrator);
                    var defaultUser = new User
                    {
                        Name = UserType.Administrator,
                        Login = UserType.Administrator,
                        Password = passwordHasher.GenerateSaltedPassword("Qwerty7"),
                        RoleId = adminRole.Id,
                        Role = adminRole,
                    };
                    await userRepo.CreateAsync(defaultUser);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        private static async Task MigrateAsync(this IServiceProvider serviceProvider)
        {
            try
            {
                using var scope = serviceProvider.CreateScope();
                var scopedServiceProvider = scope.ServiceProvider;
                var context = scopedServiceProvider.GetRequiredService<ChatContext>();
                var pendingMigrations = await context.Database.GetPendingMigrationsAsync();
                if (pendingMigrations.Any())
                {
                    await context.Database.MigrateAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}