using Framework.DAL.EF.Abstract;
using Framework.Wrappers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using TestChat.Data.Common;

namespace TestChat.Data.Extensions
{
    public static class DataServicesInitializer
    {
        public static void InitializeDataServices(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.TryAddScoped<ICacheWrapper, CacheWrapper>();
            services.TryAddScoped<IUnitOfWork, ChatUnitOfWork>();
        }
    }
}