using System.Diagnostics.Eventing.Reader;
using Framework.DAL.EF.Extensions;
using Microsoft.EntityFrameworkCore;
using TestChat.Data.Models;

namespace TestChat.Data.Contexts
{
    public class ChatContext : DbContext
    {
        public ChatContext(DbContextOptions options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ChatUser>(e =>
            {
                e.HasKey(_ => new {_.ChatId, _.UserId});
            });
            
            builder.TableToSnakeCase();
        }

        #region [ DbSets ]

        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<User> Users { get; set; }
        
        /// <summary>
        /// Роли пользователя
        /// </summary>
        public DbSet<UserRole> UserRoles { get; set; }
        
        /// <summary>
        /// Чаты
        /// </summary>
        public DbSet<Chat> Chats { get; set; }
        
        /// <summary>
        /// Участники чата
        /// </summary>
        public DbSet<ChatUser> ChatUsers { get; set; }

        /// <summary>
        /// Сообщения
        /// </summary>
        public DbSet<ChatMessage> Messages { get; set; }
        
        /// <summary>
        /// Логи
        /// </summary>
        public DbSet<LogEntry> Logs { get; set; }
        
        #endregion
    }
}